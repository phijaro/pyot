import gen, OT, multiprocessing

class Generator:
    def __init__(self, ur, ranking, **kwargs):
        self.ur = ur
        self.ranking = ranking
        self.lang = kwargs.get('lang', 'fallback')
        self.level = kwargs.get('level', OT.ProsodicPhrase)
        self.filter = kwargs.get('filter', ())
        self.baton = kwargs.get('baton', None)
        self.pool = kwargs.get('pool', None)
        self.jobs = []
        self.pending = []
        self.seg_cands = []
        self.kwargs = kwargs
        self.cc_coindex = (OT.constraints.CC_Corr in [c[0] for c in ranking])

        script = gen.segVarScript(OT.flatten(ur), ranking, lang=self.lang)
        self.seg_cands = gen.generateFromScript(script)

    def __enter__(self):
        if self.pool is None:
            try:
                self.pool = multiprocessing.Pool(None, None, None, 4)
            except TypeError:
                self.pool = multiprocessing.Pool()
            self.close_pool = True
        else:
            self.close_pool = False
        return self
        
    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.close_pool:
            if exc_type:
                self.pool.terminate()
            else:
                self.pool.close()
        return False

    def next_job(self):
        for slot in range(multiprocessing.cpu_count() - len(self.pending)):
            if self.jobs:
                job = self.jobs.pop(0)
                if job[-1] in (OT.ProsodicWord, OT.ProsodicPhrase):
                    level = job[-1]
                elif job[0] == gen.syllabify:
                    level = OT.Syllable
                elif job[0] == gen.footGeneral:
                    level = OT.Foot
                elif job[0] == gen.Max_CC:
                    level = 'CC_done'
                else:
                    level = OT.Segment
                self.pending.append((self.pool.apply_async(job[0], job[1:]), level))

    def build_structure(self):
        if self.level is OT.Segment:
            for cand in self.seg_cands:
                self.jobs.append((gen.applyFilter, self.ur, cand, self.filter))
        else:
            for cand in self.seg_cands:
                self.jobs.append((gen.syllabify, cand))        
        for idx in range(len(self.jobs)):
            self.next_job()

    def main_loop(self):
        while self.pending:
            for job in self.pending[:]:
                if job[0].ready():
                    result = job[0].get()
                    currentLevel = job[1]
                    if type(result) == OT.Utterance:
                        if (currentLevel == self.level and self.cc_coindex):
                            self.jobs.insert(0, (gen.Max_CC, result))
                        elif (currentLevel == self.level or currentLevel == 'CC_done'):
                            yield result
                        elif currentLevel == OT.Segment:
                            self.jobs.insert(0, (gen.syllabify, result))
                        elif currentLevel == OT.Syllable:
                            self.jobs.insert(0, (gen.footGeneral, result))
                        elif currentLevel == OT.Foot:
                            self.jobs.insert(0, (gen.hiLevelParse,
                                                 result, OT.ProsodicWord))
                        elif currentLevel == OT.ProsodicWord:
                            self.jobs.insert(0, (gen.hiLevelParse,
                                                 result, OT.ProsodicPhrase))
                    elif result:
                        for cand in result:
                            self.pending.append((self.pool.apply_async(gen.applyFilter,
                                                                       (self.ur, cand, self.filter)),
                                currentLevel))
                    self.next_job()
                    self.pending.remove(job)

def Gen(ur, ranking, **kwargs):
    level = kwargs.get('level', OT.ProsodicPhrase)
    baton = kwargs.get('baton', None)
    filt = kwargs.get('filter', ())

    with Generator(ur, ranking, **kwargs) as machine:
        machine.build_structure()
        for cand in machine.main_loop():
            yield cand
