import re, config, sys, locale
from types import *

locale.setlocale(locale.LC_ALL)
code = locale.getpreferredencoding()

features = {'consonantal': 'cons',
            'vocalic': 'voc',
            'sonorant': 'son',
            'obstruent': 'obs',
            'voice': 'voi',
            'spread glottis': 'sprgl',
            'nasal': 'nas',
            'lateral': 'lat',
            'strident': 'strid',
            'rhotic': 'rho',
            'plosive': 'plos',
            'continuant': 'cont',
            'labial': 'lab',
            'coronal': 'cor',
            'dorsal': 'dor',
            'radical': 'rad',
            'high': 'hi',
            'low': 'lo',
            'atr': 'atr',
            'rtr': 'rtr'}

binary = (('cons', 'voc'), ('obs', 'son'))
mutex = (('plos', 'cont'), ('hi', 'lo'), ('atr', 'rtr'))

diacritics = config.get_diacritics(scheme='ipa')
TIPAdiacritics = config.get_diacritics(scheme='tipa')
XSAMPAdiacritics = config.get_diacritics(scheme='xsampa')

class Reduplicant(list):
    def __init__(self, parent=None, lang=None):
        if lang is None:
            self.lang = getattr(parent, 'lang', 'fallback')
        else:
            self.lang = lang
        self.ipa = u'\u0280\u1d07\u1d05' # Small capital RED
        self.xsampa = 'RED'
        self.tipa = r'\*{\textsc{red}}'

        if parent is not None:
            self.populate(parent)

    def copy(self):
        out = Reduplicant()
        for unit in self:
            out.append(unit.copy())
        return out

    def populate(self, parent):
        for unit in self[:]:
            self.remove(unit)
        for unit in parent:
            if type(unit) is not Reduplicant:
                unit.indices['BR'] = 'B%d' % unit.indices['IO']
                self.append(unit.copy())
                del self[-1].indices['IO']
                self[-1].indices['BR'] = 'R%d' % unit.indices['IO']

    def prettyPrint(self, scheme='ipa'):
        return getattr(self, scheme)

class Morpheme(list):
    '''Represents morphemes with phonologically conditioned allomorphs.'''
    def __init__(self, *args):
        for unit in args:
            self.append(unit)

    def set_ipa(self, ipa):
        self.ipa = ipa
        return self.ipa

    def set_tipa(self, tipa):
        self.tipa = tipa
        return self.tipa

    def set_xsampa(self, xsampa):
        self.xsampa = xsampa
        return self.xsampa

    def prettyPrint(self, scheme='ipa'):
        return getattr(self, scheme)
        
class Segment(set):
    def __init__(self, inp=None, ind=0, **kwargs):
        self.lex = kwargs.get('lex', True)
        self.lang = kwargs.get('lang', 'fallback')
        self.indices = {}
        self.indices['IO'] = ind
        self.clear()
        ipas = config.get_ipas(self.lang, 'ipa', lex=self.lex)
        tipas = config.get_ipas(self.lang, 'tipa', lex=self.lex)
        xsampas = config.get_ipas(self.lang, 'xsampa', lex=self.lex)
        
        if type(inp) is list:
            self.update(inp)
            for seg in ipas:
                if self.difference(set(ipas[seg])) == set():
                    self.ipa = seg
        elif type(inp) is unicode or type(inp) is str:
            if inp in ipas:
                self.update(ipas[inp])
                self.ipa = inp
                self.set_xsampa()
            elif inp in xsampas:
                self.update(xsampas[inp])
                self.xsampa = inp
                self.set_ipa()
            elif inp in tipas:
                self.update(tipas[inp])
                self.tipa = inp
                self.set_ipa()
                self.set_xsampa()
            else:
                self.set_ipa()
                self.set_xsampa()
        self.set_tipa()

    def __eq__(self, other):
        if not (type(other) is Segment or type(other) is set):
            return False
        if self ^ other: return False
        if getattr(self, 'indices', None) != getattr(other, 'indices', None):
            return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def flatten(self):
        return [self]

    def copy(self):
        out = Segment(lang=self.lang, lex=self.lex)
        for feat in self:
            out.add(feat)
        out.set_ipa()
        out.set_tipa()
        out.indices = self.indices.copy()
        return out

    def set_ipa(self):
        ipas = config.get_ipas(self.lang, 'ipa', lex=self.lex)
        for ipa in ipas:
            if not (set(ipas[ipa]) ^ self):
                self.ipa = ipa
                return ipa
        self.ipa = approxIPA(self) or repr(self)
        return self.ipa

    def set_tipa(self):
        tipas = config.get_ipas(self.lang, 'tipa', lex=self.lex)
        for tipa in tipas:
            if not (set(tipas[tipa]) ^ self):
                self.tipa = tipa
                return tipa
        self.tipa = approxTIPA(self) or repr(self)
        return self.tipa

    def set_xsampa(self):
        xsampas = config.get_ipas(self.lang, 'xsampa', lex=self.lex)
        for xsampa in xsampas:
            if not (set(xsampas[xsampa]) ^ self):
                self.xsampa = xsampa
                return xsampa
        self.xsampa = approxXSAMPA(self) or repr(self)
        return self.xsampa

    def set_language(self, lang):
        self.lang = lang
        self.set_ipa()
        self.set_tipa()
        self.set_xsampa()

    def prettyPrint(self, scheme='ipa'):        
        self.set_ipa()
        self.set_tipa()
        self.set_xsampa()
        out = getattr(self, scheme, repr(self))
        if self.indices.get('CC', False):
            if scheme == 'ipa': out += u'\u1d62'
            elif scheme == 'tipa': out += r'\ensuremath{_i}'
            elif scheme == 'xsampa': out += '{i}'
        return out

    def toggleFeature(self, feat):
        if feat in self:
            func = self.discard
            notfunc = self.add
        else:
            func = self.add
            notfunc = self.discard

        func(feat)
        for pair in binary:
            if pair[0] == feat:
                notfunc(pair[1])
            elif pair[1] == feat:
                notfunc(pair[0])
        for pair in mutex:
            if pair[0] == feat and pair[1] in self and func is self.add:
                return False
            if pair[1] == feat and pair[0] in self and func is self.add:
                return False
        self.set_ipa
        return True

class Utterance(list):
    def __init__(self, inp='', **kwargs):
        self.weight_by_position = kwargs.get('weight_by_position', True)
        self.setWeightByPosition()
        self.lex = kwargs.get('lex', True)
        self.lang = kwargs.get('lang', 'fallback')
        if type(inp) is str:
            self.validateInput(inp.decode(code))
        elif type(inp) is unicode:
            self.validateInput(inp)
        elif type(inp) is list:
            for item in inp:
                self.append(item)
        return None

    def __repr__(self):
        out = 'Utterance(['
        for item in self:
            out += repr(item)
            out += ', '
        out = out.rstrip(', ')
        out += '], lex=%s, lang="%s")' % (self.lex, self.lang)
        return out

    def __unicode__(self):
        return u'Utterance("%s", lex=%s, lang="%s")' % (self.prettyPrint('ipa'),
                                                        self.lex,
                                                        self.lang)

    def __str__(self):
        return 'Utterance("%s", lex=%s, lang="%s")' % (self.prettyPrint('xsampa'),
                                                       self.lex,
                                                       self.lang)
        

    def __eq__(self, other):
        try:
            if len(self) != len(other): return False
        except TypeError:
            return False
        for idx in range(len(self)):
            if self[idx] != other[idx]: return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def set_language(self, lang='fallback'):
        self.lang = lang
        for unit in self:
            if hasattr(unit, 'set_language'):
                unit.set_language(lang)

    def get_reduplicant(self):
        '''Returns the leftmost Reduplicant object in self.'''
        for unit in self:
            if type(unit) is Reduplicant: return unit

    def expand_reduplicants(self):
        '''Replaces every Reduplicant object in self with the elements
        from the reduplicant.'''
        unit = self.get_reduplicant()
        while unit is not None:
            idx = self.index(unit)
            unit.reverse()
            for red_unit in unit:        
                self.insert(idx, red_unit)
            self.remove(unit)
            unit = self.get_reduplicant()
        for unit in flatten(self):
            if hasattr(unit, 'indices') and unit.indices.get('BR', None) is None:
                unit.indices['BR'] = 'B%d' % unit.indices['IO']

    def expand_morphemes(self):
        '''Returns a list of Utterance objects corresponding to every
        possible parse of self with allomorphs of any Morpheme objects
        in self fully specified.'''
        out = [Utterance(lang=self.lang)]
        for unit in self:
            if type(unit) is Morpheme:
                newout = []
                for allomorph in unit:
                    for ben in out:
                        newout.append(ben.copy())
                        newout[-1].extend(allomorph)
                out = newout
            else:
                for ben in out:
                    ben.append(unit)
        return out
    
    def copy(self):
        out = Utterance(lang=self.lang, lex=self.lex)
        for unit in self:
            out.append(unit.copy())
        return out

    def setWeightByPosition(self, weight_by_position=None):
        if weight_by_position is not None:
            self.weight_by_position = weight_by_position

        for syll in flatten(self, Syllable):
            syll.weight_by_position = self.weight_by_position
            syll.getMoraCount()

    def validateInput(self, inp):
        if '||' in inp:
            inp = inp.split('||')
            for pph in inp:
                if pph:
                    self.append(ProsodicPhrase(pph, lang=self.lang))
        elif '|' in inp or '"' in inp or u'\u02cc' in inp:
            inp = inp.split('|')
            for pwd in inp:
                if pwd:
                    self.append(ProsodicWord(pwd, lang=self.lang))
        elif '(' in inp:
            inp = bracketParse(inp, lang=self.lang)
            for div in inp:
                self.append(div)
        elif '.' in inp:
            inp = inp.split('.')
            for syll in inp:
                if syll:
                    self.append(Syllable(syll, lang=self.lang, lex=self.lex))
        elif inp:            
            segs = parseIntoSegments(inp, self.lang, lex=self.lex)
            if len(segs) > 1:
                print >> sys.stderr, 'Warning: input %s is ambiguous.' % inp.encode(code)
            elif not segs:
                raise ParseError
            segs = segs[0]
            self.extend(segs)

        for level in levels:
            ind = self.indexLevel(level)
            if level == Segment:
                self.topIndex = ind
        
        return None

    def indexLevel(self, level=Segment, mode='IO', **kwargs):
	obj = kwargs.get('obj', self)
        ind = kwargs.get('ind', 1)
        last = set([])
        if level is Segment:
            for seg in flatten(obj, Segment):
		if type(seg) is Segment:
		    if (type(last) is Segment
			and not (seg ^ last)):
			ind -=1
                    if mode == 'CC':
                        seg.indices[mode] = False
                    else:
                        seg.indices[mode] = ind
		    last = seg
                    ind += 1
		elif type(seg) is Morpheme:
		    newind = ind
		    for allomorph in seg:
			c = self.indexLevel(level, mode, obj=allomorph, ind=ind)
			if c > newind:
			    newind = c
                    ind = newind
            return ind
        for unit in flatten(self, level):
            if unit == last: ind -= 1
            unit.indices[mode] = ind
            ind += 1
            last = unit
        return ind
    
    def unitByIndex(self, ind, level=Segment):
        for unit in flatten(self, level):
            if getattr(unit, 'indices', {}).get('IO', None) == ind:
                return unit
        return None

    def prettyPrint(self, scheme='ipa'):
        out = u''

        if not self:
            if scheme == 'ipa':
                return u'\u2205'
            elif scheme == 'tipa':
                return r'\ensuremath{\emptyset}'
            elif scheme == 'xsampa':
                return '0'

        if scheme == 'ipa':
            lm = u'\u02d0'
        else:
            lm = ':'
            
        last = set([])
        for unit in self:
            if (type(unit) is Segment 
                and type(last) is Segment
                and (not unit ^ last)):
                out += lm
            else:
                out += unit.prettyPrint(scheme)                
            last = unit
        if out.endswith(' \\textdoublevertline{} '):
            out = out[:0 - len(' \\textdoublevertline{} ')]
        if out.endswith(' \\textvertline{} '):
            out = out[:0 - len(' \\textvertline{} ')]
        out = out.rstrip(u' |\u2016.')

        return out

class Syllable(list):
    def __init__(self, inp='', **kwargs):
        self.lang = kwargs.get('lang', 'fallback')
        self.lex = kwargs.get('lex', True)
        self.weight_by_position = kwargs.get('weight_by_position', True)
        self.indices = {}
        if inp and type(inp) in (str, unicode):
            self.validateInput(inp)
        else:
            self.ons = []
            self.nuc = []
            self.cod = []
        return None

    def __eq__(self, other):
        if type(other) is not Syllable:
            return False
        if len(self) != len(other): return False
        for idx in range(len(self)):
            if self[idx] != other[idx]: return False
        for sub in ('ons', 'nuc', 'cod'):
            if not hasattr(other, sub): return False
            if len(getattr(self, sub)) != len(getattr(other, sub)): return False
            for idx in range(len(getattr(self, sub))):
                if (getattr(self, sub)[idx] !=
                    getattr(other, sub)[idx]): return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def set_language(self, lang='fallback'):
        self.lang = lang
        for unit in self:
            if hasattr(unit, 'set_language'):
                unit.set_language(lang)

    def validateInput(self, inp):
        last = None
        (segs, cv_dict) = parseIntoSegments(inp, self.lang, mode='syllable', lex=self.lex)
        if len(segs) > 1:
            print >> sys.stderr, 'Warning, input is ambiguous'
        elif not segs:
            raise ParseError(inp)
        segs = segs[0]
        for seg in segs:
            self.append(seg)
            if last is not None and (not seg ^ last):
                seg.indices = last.indices
            last = seg

        self.hierarchize(cv_dict)

        self.getMoraCount()

    def hierarchize(self, cv_dict={'C': [], 'V': []}):
        sons = [gradeSonority(seg) for seg in self]
        nucStart = None
        if not cv_dict['V']:
            for idx in range(len(sons)):
                if sons[idx] > 5 and self[idx] not in cv_dict['C']:
                    nucStart = idx
                    break

            if nucStart is None:
                nucStart = sons.index(max(sons))
            if len(self) != nucStart + 1:
                codStart = nucStart + 1
                test = sons[codStart]
                if sons[nucStart] > 5:
                    while sons[codStart] > 5:
                        codStart += 1
                        if codStart >= len(sons):
                            codStart = False
                            break
            else:
                codStart = False
        else:
            Vs = [self.index(V) for V in cv_dict['V']]
            Vs.sort()
            nucStart = Vs[0]
            codStart = False
            for idx in range(1, len(Vs)):
                if Vs[idx] != (Vs[idx - 1] + 1):
                    codStart = nucStart + idx
            
        self.ons = self[:nucStart]
        if codStart:
            self.nuc = self[nucStart:codStart]
            self.cod = self[codStart:]
        else:
            self.nuc = self[nucStart:]
            self.cod = []

    def getMoraCount(self, weight_by_position=None):
        self.MoraCount = len(self.nuc)

        if self.weight_by_position:
            self.MoraCount += len(self.cod)

        if weight_by_position is None:
            return self.MoraCount
        if weight_by_position:
            return len(self.nuc) + len(self.cod)
        return len(self.nuc)

    def copy(self):
        out = Syllable(lang=self.lang, lex=self.lex)
        out.indices = self.indices.copy()
        for seg in self:
            cop = seg.copy()
            out.append(cop)
            if seg in self.ons:
                out.ons.append(cop)
            if seg in self.nuc:
                out.nuc.append(cop)
            if seg in self.cod:
                out.cod.append(cop)
        out.getMoraCount()
        return out

    def edgemostSegs(self, edge='R'):
        '''Returns a tuple of segments at edge ('L' or 'R') that are
        not parsed into the ons, nuc or cod.'''
        out = []
        done = False
        if edge.upper() == 'L':
            for seg in self:
                for oseg in self.ons:
                    if oseg is seg:
                        done = True
                        break
                for nseg in self.nuc:
                    if nseg is seg:
                        done = True
                        break
                if done: break
                out.append(seg)
        elif edge.upper() == 'R':
            for idx in range(len(self) -1, -1, -1):
                for seg in self.nuc:
                    if seg is self[idx]:
                        done = True
                        break
                for seg in self.cod:
                    if seg is self[idx]:
                        done = True
                        break
                if done: break
                out.insert(0, self[idx])
        else:
            raise InvalidEdge(edge)

        return tuple(out)
            

    def pepper(self, seg, su, scheme='ipa'):
        '''Returns the output of seg.prettyPrint, where seg is a
        segment, plus the appropriate diacritic for the subunit su
        (self.ons, self.nuc or self.cod) according to scheme.'''
        if su is self.nuc and 'cons' in seg:
            if scheme == 'tipa':
                return '%s{%s}' % (TIPAdiacritics['voc'],
                                  seg.prettyPrint(scheme))
            elif scheme == 'xsampa':
                return '%s_%s' % (seg.prettyPrint(scheme),
                                  XSAMPAdiacritics['voc'])
            else:
                return seg.prettyPrint(scheme) + diacritics['voc']
        elif su in (self.ons, self.cod) and 'voc' in seg:
            if scheme == 'tipa':
                return '%s{%s}' % (TIPAdiacritics['cons'],
                                   seg.prettyPrint(scheme))
            elif scheme == 'xsampa':
                return '%s_%s' % (seg.prettyPrint(scheme),
                                  XSAMPAdiacritics['cons'])
            else:
                return seg.prettyPrint(scheme) + diacritics['cons']
        return seg.prettyPrint(scheme)

    def prettyPrint(self, scheme='ipa'):
        out = ''
        last = None

        for su in (self.ons, self.nuc, self.cod):
            stretch = ''
            for seg in su:
                if last == seg and scheme == 'ipa': 
                    stretch += u'\u02d0'
                elif last == seg:
                    stretch += ':'
                else:
                    stretch += self.pepper(seg, su, scheme)
                last = seg
            out += stretch

        pre = ''
        for seg in self.edgemostSegs('L'):
            pre += seg.prettyPrint()

        post = ''
        for seg in self.edgemostSegs('R'):
            post += seg.prettyPrint()

        return pre + out + post + '.'

class Foot(list):
    def __init__(self, inp='', **kwargs):
        self.lang = kwargs.get('lang', 'fallback')
        self.indices = {}
        return None

    def __eq__(self, other):
        return _eq(self, other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def set_language(self, lang='fallback'):
        self.lang = lang
        for unit in self:
            if hasattr(unit, 'set_language'):
                unit.set_language(lang)

    def copy(self):
        out = Foot(lang=self.lang)
        out.indices = self.indices.copy()
        for unit in self:
            out.append(unit.copy())
        out.head = self.head
        return out

    def prettyPrint(self, scheme='ipa'):
        out = u'('
        for ind in range(len(self)):
            if self.head == ind:
                if scheme in ('tipa', 'xsampa'):
                    out += '"'
                else:
                    out += u'\u02c8'
            out += self[ind].prettyPrint(scheme)
        out = out.rstrip('.')
        out += ')'
        return out

class ProsodicWord(list):
    def __init__(self, inp='', **kwargs):
        self.lang = kwargs.get('lang', 'fallback')
        self.indices = {}
        self.validateInput(inp)
        return None

    def __eq__(self, other):
        return _eq(self, other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def set_language(self, lang='fallback'):
        self.lang = lang
        for unit in self:
            if hasattr(unit, 'set_language'):
                unit.set_language(lang)

    def copy(self):
        out = ProsodicWord(lang=self.lang)
        out.indices = self.indices.copy()
        for unit in self:
            out.append(unit.copy())
        if hasattr(self, 'head'):
            out.head = self.head
        return out

    def setHeadFromInp(self, inp):
        footCount = -1
        last = None
        buf = ''
        for c in inp:
            buf += c
            if len(buf) > 3:
                buf = buf[-2:]
            if c == '(':
                footCount += 1

            if buf[:2] in (u'(\u02c8', "('"):
                break
            elif buf[:2] == '("' and buf[-1] != '"':
                break
        pos = -1
        for elem in self:
            if type(elem) is Foot:
                pos += 1
                if pos == footCount:
                    self.head = self.index(elem)

    def validateInput(self, inp):
        if not inp:
            pass
        elif '(' in inp:
            p = bracketParse(inp, lang=self.lang)
            for div in p:
                self.append(div)
            self.setHeadFromInp(inp)
        elif '.' in inp:
            inp = inp.split('.')
            for syll in inp:
                if syll:
                    self.append(Syllable(syll))
        else:
            self.append(Syllable(inp))

    def prettyPrint(self, scheme='ipa'):
        out = u''
        for idx in range(len(self)):
            addend = self[idx].prettyPrint(scheme)
            if type(self[idx]) is Foot and self.head != idx:
                if scheme == 'tipa':
                    addend = addend.replace(u'"', u'""')
                elif scheme == 'xsampa':
                    addend = addend.replace('"', '%')
                else:
                    addend = addend.replace(u'\u02c8', u'\u02cc')
            out += addend
            if idx != (len(self) - 1):
                if type(self[idx + 1]) is Foot:
                    out = out.rstrip('.')
            else:
                out = out.rstrip('.')

        if scheme == 'tipa':
            return out + ' \\textvertline{} '
        return out + ' | '

class ProsodicPhrase(list):
    def __init__(self, inp='', **kwargs):
        self.lang = kwargs.get('lang', 'fallback')
        self.indices = {}
        self.validateInput(inp)
        return None

    def __eq__(self, other):
        return _eq(self, other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def set_language(self, lang='fallback'):
        self.lang = lang
        for unit in self:
            if hasattr(unit, 'set_language'):
                unit.set_language(lang)

    def validateInput(self, inp):
        for pwd in inp.split('|'):
            if pwd:
                self.append(ProsodicWord(pwd, lang=self.lang))

    def copy(self):
        out = ProsodicPhrase(lang=self.lang)
        out.indices = self.indices.copy()
        for unit in self:
            out.append(unit.copy())
        if hasattr(self, 'head'):
            out.head = self.head
        return out

    def prettyPrint(self, scheme='ipa'):
        out = ''
        for pwd in self:
            out += pwd.prettyPrint(scheme)
        if scheme == 'tipa':
            if out.endswith(' \\textvertline{} '):
                out = out[:0 - len(' \\textvertline{} ')]
            return out + u' \\textdoublevertline{} '
        out = out.rstrip(' |')
        if scheme == 'xsampa':
            return out + ' ||'
        return out + u' \u2016 '

def getLevel(unit):
    for idx in range(len(levels) - 1, 0, -1):
        if flattenable(unit, levels[idx]):
            return levels[idx]

def flattenable(unit, level=Segment):
    if not unit:
        return False
    elif issubclass(type(unit), level):
        return True
    elif issubclass(type(unit), Segment):
        return False
    else:
        for subunit in unit:
            if flattenable(subunit, level):
                return True
    return False

def flatten(unit, level=Segment):
    if not flattenable(unit, level):
        return []
    else:
        out = Utterance(lang=getattr(unit, 'lang', 'fallback'))
        for subunit in unit:
            if issubclass(type(subunit), level):
                out.append(subunit)
            elif type(subunit) == Reduplicant:
                out.append(subunit)
            elif type(subunit) == Morpheme:
                out.append(subunit)
            elif level is Foot and issubclass(type(subunit), Syllable):
                out.append(subunit)
            else:
                out.extend(flatten(subunit, level))
    return out

def prettyPrint(seq):
    for unit in seq:
        func = getattr(unit, 'prettyPrint', prettyPrint)
        print func(unit)

def approxIPA(seg, lang='fallback'):
    diffs = {}
    base = u''

    ipas = config.get_ipas(lang, 'ipa', lex=seg.lex)
    for ipa in ipas:
        diff = seg.difference(ipas[ipa])
        sdiff = seg.symmetric_difference(ipas[ipa])
        if len(sdiff) is 1 and len(diff) is 1:
            diffs[ipa] = diff.pop()

    for ipa in diffs:
        if diacritics.get(diffs[ipa], ''):
            return ipa + diacritics[diffs[ipa]]

    for ipa in ipas:
        if (set(ipas[ipa]).difference(seg) == set(['voi'])
            and set(ipas[ipa]).symmetric_difference(seg) == set(['voi'])):
            return ipa + u'\u0325'

    return u''    

def approxTIPA(seg, lang='fallback'):
    diffs = {}
    base = ''

    tipas = config.get_ipas(lang, 'tipa', lex=seg.lex)
    for tipa in tipas:
        diff = seg.difference(tipas[tipa])
        sdiff = seg.symmetric_difference(tipas[tipa])
        if len(sdiff) is 1 and len(diff) is 1:
            diffs[tipa] = diff.pop()

    for tipa in diffs:
        dia = TIPAdiacritics.get(diffs[tipa], '')
        if dia.endswith('}'):
            return tipa + dia
        if dia.startswith('\\'):
            return '%s{%s}' % (dia, tipa)
        return tipa + dia

    for tipa in tipas:
        if (set(tipas[tipa]).difference(seg) == set(['voi'])
            and set(tipas[tipa]).symmetric_difference(seg) == set(['voi'])):
            return '\\textsubring{%s}' % tipa

    return ''

def approxXSAMPA(seg, lang='fallback'):
    diffs = {}
    base = ''

    xsampas = config.get_ipas(lang, 'xsampa', lex=seg.lex)
    for xsampa in xsampas:
        diff = seg.difference(xsampas[xsampa])
        sdiff = seg.symmetric_difference(xsampas[xsampa])
        if len(sdiff) is 1 and len(diff) is 1:
            diffs[xsampa] = diff.pop()

    for xsampa in diffs:
        dia = XSAMPAdiacritics.get(diffs[xsampa], '')
        return '%s_%s' % (xsampa, dia)

    for xsampa in xsampas:
        if set(xsampas[xsampa]).difference(seg) == set(['voi']):
            return '%s_0' % xsampa

    return ''

def possibleSegment(seg):
    for pair in mutex:
        if pair[0] in seg and pair[1] in seg:
            return False
    for pair in binary:
        if pair[0] in seg and pair[1] in seg:
            return False
        if not (pair[0] in seg or pair[1] in seg):
            return False
    for feat in seg:
        if not feat in features:
            out = False
            for full in features:
                if feat == features[full]:
                    out = True
            if not out:
                return out
    return True

def gradeSonority(seg):
    out = 4
    if 'voc' in seg:
        out += 1
        if 'low' in seg:
            out += 1

    for feat in ('cons', 'obs', 'lat', 'rho', 'high'):
        if feat in seg:
            out -= 1

    if 'son' in seg:
        out += 2

    if 'plos' in seg:
        out -= 2

    return out

def bracketParse(inp, offset=0, **kwargs):
    lang = kwargs.get('lang', 'fallback')
    opened = []
    out = []
    head = False
    syll = ''
    for ch in range(len(inp)):
        if inp[ch] == '(':
            if syll:
                out.append(Syllable(syll, lang=lang))
            opened.append(ch)
            out.append(Foot(lang=lang))
            syll = ''
        elif inp[ch] == ')':
            if not opened:
                raise ParseError(')', ch + offset)
            else:
                if syll:
                    out[-1].append(Syllable(syll, lang=lang))
                syll = ''
                opened.pop(-1)
                if head:
                    out[-1].head = (len(out[-1]) - 1)
                    head = False
                elif len(out[-1]) == 1:
                    out[-1].head = 0
                else:
                    if getattr(out[-1], 'head', None) is None:
                        raise ParseError('No head specified for foot closed',
                                         ch + offset)
        elif inp[ch] in (u'\u02c8', u'\u02cc', "'", '"', '%'):
            if opened:
                if getattr(out[-1], 'head', None) is not None:
                    raise ParseError(inp[ch], ch + offset)
                else:
                    head = True
            else:
                raise ParseError(inp[ch], ch + offset)
        elif inp[ch] == '.':
            if syll:
                if opened:
                    out[-1].append(Syllable(syll, lang=lang))
                else:
                    out.append(Syllable(syll, lang=lang))
            syll = ''
            if head:
                out[-1].head = (len(out[-1]) - 1)
                head = False
        else:
            syll += inp[ch]
    if syll:
        out.append(Syllable(syll, lang=lang))
    if opened:
        raise ParseError('(', opened[0] + offset)
    return out
        
class FlattenError(Exception):
    def __init__(self, obj):
        self.obj = obj

    def __str__(self):
        return ('Cannot flatten object %s' % self.obj.__name__)

class InvalidEdge(Exception):
    def __init__(self, st):
        self.st = st

    def __str__(self):
        return ('Invalid edge specification "%s".  Should be "L" or "R"' 
                % self.st)

class ParseError(Exception):
    def __init__(self, brac='', pos=None):
        self.brac = brac
        if pos is not None:
            self.pos = pos + 1
        else:
            self.pos = None

    def __str__(self):
        if self.brac == ')':
            return "Superfluous `)' at position %d" % self.pos
        elif self.brac == '(':
            return ("`(' at position %d has no corresponding `)'" % self.pos)
        elif self.pos is not None:
            return "%s at position %d" % (self.brac, self.pos)
        else:
            return "Could not parse input '%s' into segments." % self.brac.encode(code)

def upconvert(utt, **kwargs):
    lang = kwargs.get('lang', 'fallback')
    OT.Utterance.set_language(utt, lang)
    for level in levels:
        for unit in flatten(utt, level):
            if not hasattr(unit, 'indices'):
                unit.indices = {}
                if not type(getattr(unit, 'index', None)) is BuiltinMethodType:
                    unit.indices['IO'] = unit.index
                    del unit.index
    return utt

def hardwaysUpconvert(obj, **kwargs):
    '''Takes an object created by any previous version of this module
    and returns one from this version that is equivalent. Works by
    creating a new object and populating it from the old one.'''
    lang = kwargs.get('lang', 'fallback')
    obj_type = type(obj)
    out = obj_type()
    if hasattr(obj, 'indices'):
        out.indices = obj.indices.copy()
    elif type(getattr(out, 'index', None)) is not BuiltinMethodType:
        out.indices['IO'] = obj.index

    if hasattr(obj, 'head'):
        out.head = obj.head

    if obj_type is Segment:
        for feat in obj:
            out.add(feat)
        return out
    elif obj_type is Reduplicant:
        return out
    elif obj_type is Morpheme:
        for attr in ('ipa', 'tipa', 'xsampa'):
            if hasattr(obj, attr):
                eval('out.set_%s(obj.%s)' % (attr, attr))
        for unit in obj:
            out.append(hardwaysUpconvert(unit))
        return out
    else:
        for unit in obj:
            out.append(hardwaysUpconvert(unit))
        if obj_type is Utterance:
            out.set_language(lang)
        elif obj_type is Syllable:
            for idx in range(len(obj)):
                if obj[idx] in obj.ons:
                    out.ons.append(out[idx])
                if obj[idx] in obj.nuc:
                    out.nuc.append(out[idx])
                if obj[idx] in obj.cod:
                    out.cod.append(out[idx])

        return out

def _eq(thing, other):
    if type(other) is not type(thing):
        return False
    if len(other) != len(thing):
        return False
    for idx in range(len(thing)):
        if thing[idx] != other[idx]:
            return False
    if getattr(thing, 'head', None) != getattr(other, 'head', None):
        return False
    if not (type(getattr(thing, 'indices', None)) is BuiltinMethodType):
        if getattr(thing, 'indices', None) != getattr(other, 'indices', None):
            return False
    return True

def parseIntoSegments(text, language='fallback', **kwargs):
    '''Returns a list of lists of Segment-level objects, each of which
    is a possible parse of the transcription text.'''
    sofar = kwargs.get('sofar', [])
    out = kwargs.get('out', [])
    segments = kwargs.get('segments', {})
    lex = kwargs.get('lex', True)
    lengths = kwargs.get('lengths', set())
    mode = kwargs.get('mode', 'segment')
    sylldict = kwargs.get('sylldict', {'C': [], 'V': []})
    
    if not segments:
        for lang in ('fallback', language):
            for scheme in ('tipa', 'xsampa', 'ipa'):
                segments.update(config.get_ipas(lang, scheme, lex=lex))
        for key in segments:
            lengths.add(len(key))

        lengths = list(lengths)
        lengths.sort(None, None, True)

    specials = {u'\u02d0': 'length',
                ':': 'length',
                u'\u1d62': 'cc_coindex',
                r'\ensuremath{_i}': 'cc_coindex',
                '{i}': 'cc_coindex'
                }
    r = Reduplicant()
    for scheme in ('tipa', 'xsampa', 'ipa'):
        specials[getattr(r, scheme)] = r

    if not text:
        out.append(sofar)
        return sofar
    
    possible = False

    for special in specials:
        cop = sofar[:]
        if text.startswith(special):
            possible = True
            if specials[special] == 'length':
                cop.append(cop[-1].copy())
            elif specials[special] == 'cc_coindex':
                cop[-1].indices['CC'] = True
            elif type(specials[special]) == Reduplicant:
                cop.append(Reduplicant())
            parseIntoSegments(text[len(special):], language, sofar=cop, out=out,
                              segments=segments, lengths=lengths, mode=mode, sylldict=sylldict, lex=lex)

    for feat in diacritics:
        cop = sofar[:]
        if diacritics[feat] and text.startswith(diacritics[feat]):
            if feat not in ('cons', 'voc') or mode == 'segment':
                if cop[-1].toggleFeature(feat):
                    possible = True
                    parseIntoSegments(text[1:], language, sofar=cop, out=out,
                                      segments=segments, lengths=lengths, mode=mode, sylldict=sylldict, lex=lex)
            else:
                possible = True
                if feat == 'voc':
                    sylldict['V'].append(cop[-1])
                elif feat == 'cons':
                    sylldict['C'].append(cop[-1])
                parseIntoSegments(text[1:], language, sofar=cop, out=out,
                                  segments=segments, lengths=lengths, mode=mode, sylldict=sylldict, lex=lex)
                
    for length in lengths:
        cop = sofar[:]
        if len(text) >= length:
            if text[:length] in segments:
                possible = True
                cop.append(Segment(segments[text[:length]], lang=language, lex=lex))
                parseIntoSegments(text[length:], language, sofar=cop, out=out,
                                  segments=segments, lengths=lengths, mode=mode, sylldict=sylldict, lex=lex)

    if not possible:
        return False
    if mode == 'syllable':
        return out, sylldict
    else:
        return out

levels = (Segment, Syllable, Foot, ProsodicWord, ProsodicPhrase)

