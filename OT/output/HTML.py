#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import with_statement

import sys, OT, re
from xml.parsers import expat
from os import path
from OT import config
from OT import constraints
from OT.representations import *
from OT.gen import Gen
from ui_util import Baton, batonify
from types import FunctionType

def AlignFootNamer(constr):
    return u'Align<span class="param">-[Σ, %s; ω, %s]</span>' % (constr[1], constr[1])

def AlignHeadFootInWordNamer(constr):
    return u'Align<span class="param">-[Σ<sub>s</sub>, %s; ω, %s]</span>' % (constr[1], constr[1])

def MaxFeatNamer(constr):
    return 'Max-[%s]' % constr[1]

def DepFeatNamer(constr):
    return 'Dep-[%s]' % constr[1]

def CondAgreeNamer(constr):
    return '[%s]-Agree-[%s]' % constr[1:]

def GOCPNamer(constr):
    feat = constr[1]
    morae = constr[2]
    return (u'*%s-%s-%s' %
           (feat, u'µ' * morae, feat))

def StarNamer(constr):
    return nameConstraint(constr, name=('*' + constr[0].__name__[4:]))

def StarFeatsNamer(constr):
    if len(constr[1:]) == 1:
        out = '*[%s]' % constr[1]
    else:
        out = '*Seg[%s ' % constr[1]
        for feat in constr[2:]:
            out += u'∧ %s ' % feat
        out = out.rstrip() + ']'
    return out

def EntailsFeatsNamer(constr):
    if type(constr[1]) in (str, unicode):
        out = u'[%s' % constr[1]
    else:
        out = u'[%s ' % constr[1][0]
        for feat in constr[1][1:]:
            out += u'∧ %s ' % feat
    out = out.rstrip() + u'] ⊃ '
    if type(constr[2]) in (str, unicode):
        out += u'[%s' % constr[2]
    else:
        out += u'[%s ' % constr[2][0]
        for feat in constr[2][1:]:
            out += u'∧ %s ' % feat
    return out.rstrip() + u']'

constraintNamers = {
    '^Star.+$': StarNamer,
    'StarFeats': StarFeatsNamer,
    'StarFeat': StarFeatsNamer,
    'StressMaxMora': lambda x: u'Max-<span class="param">µ-σ<sub>s</sub></span>',
    'StarLongFeat': lambda constr: u'*Seg[%s]ː' % constr[1],
    'VOP': lambda x: u'VOP',
    'StarComplexOnset': lambda x: u'*ComplexOnset',
    'PVL': lambda x: u'<span class="param">*V<sub>µ</sub>D…<sub>σ</sub>]</span>',
    'OnsetSonLow': lambda x: u'OnsSonLow',
    '^Onset$': lambda x: u'Onset',
    'DepSegment': lambda x: u'Dep<span class="param">-seg</span>',
    'NoCoda': lambda x: u'NoCoda',
    'MaxMora': lambda x: u'Max-<span class="param">µ</span>',
    'MaxSegment': lambda x: u'Max-<span class="param">seg</span>',
    'StarComplexCoda': lambda x: u'*ComplexCoda',
    'StarTriMora': lambda x: u'<span class="param">*σ<sub>µµµ</sub></span>',
    'StarSemiVowel': lambda x: u'*SemiVowel',
    'AgreePlaceNode': lambda x: u'NodeAgree-place',
    'DepMora': lambda x: u'Dep-<span class="param">µ</span>',
    'StarVoicedCoda': lambda x: u'*VoicedCoda',
    'SabSonority': lambda x: u'Sonority',
    'ParseSyll': lambda x: u'Parse-<span class="param">σ</span>',
    'Max_LongV': lambda x: u'Max-<span class="param">V\u0304</span>',
    'FtTypeT': lambda x: u'FtType(T)',
    'WeightToStress': lambda x: u'Weight-To-Stress',
    'FtBin': lambda x: u'FtBin',
    'StarSyllC': lambda x: u'*C̩',
    'StarVoicelessV': lambda x: u'*V̥',               
    'StarTriphthong': lambda x: u'*Triphthong',
    'StarLongCoda': lambda x: u'*LongCoda',
    'MaxPriStressSeg': lambda x: u'Max-<span class="param">σ́-seg</span>',
    'MaxDepWord': lambda x: u'Dep-<span class="param">ω</span>',
    'StarNCsubring': lambda x: u'*NC̥',
    'StarGeminate': lambda x: u'*Geminate'
}

symbols = {ProsodicPhrase: u'Φ',
           ProsodicWord: u'ω',
           Foot: u'Σ',
           Syllable: u'σ'}

from common import sortedFeatures

def LenLimitNamer(constr):
    if constr[2] == 0 and constr[1] == 'cod':
        out = u'NoCoda'
    elif constr[2] == 1:
        if constr[1] == 'cod':
            out = u'*ComplexCoda'
        elif constr[1] == 'ons':
            out = u'*ComplexOnset'
        elif constr[1] == 'nuc':
            out = u'*V\u0304'
    else:
        nodes = {'ons': 'Onset',
                 'nuc': 'Nuc',
                 'cod': 'Coda'}
        out = u'LenLimit-%s-%d' % (nodes[constr[1]], constr[2])
    out = extendParms(constr, out, parms=constr[3:])
    return out

def IdentCCNamer(constr):
    out = u'Ident-CC'
    out = extendParms(constr, out)
    return out    

def notIdentCCNamer(constr):
    out = u'¬Ident-CC'
    out = extendParms(constr, out)
    return out

def fallbackNamer(constr, name=None):
    if name is None:
        name = constr[0].__name__.replace('_', '-')
    out = name
    out = extendParms(constr, out)
    return out

def extendParms(constr, out, parms=None):
    if parms is None:
        parms = constr[1:]
    if set(parms) == set(['lab', 'cor', 'dor', 'rad']):
        out += r'-art'
    else:
        for param in parms:
            if param in features.values() or param in features.keys():
                out += '-' + '[%s]' % param
            elif param in symbols:
                out += '-<span class="param">%s</span>' % symbols[param]
            else:
                out += '-<span class="param">%s</span>' % str(param)
    return out

def nameConstraint(constr, name=None):
    if name is None:
        name = constr[0].__name__
    namer = None
    if constraintNamers.has_key(name):
        namer = constraintNamers[name]
    else:
        for key in constraintNamers.keys():
            if re.search(key, name):
                namer = constraintNamers[key]
    if namer is None:
        try:
            namer = eval('%sNamer' % name)
        except:
            namer = fallbackNamer
    return namer(constr)

class TableauReader():
    def __init__(self, fl, grammar=None, **kwargs):
        if grammar is None:
            self.grammar = OT.Grammar()
        else: 
            self.grammar = grammar
        self.lang = kwargs.get('lang', 'fallback')
        self.cnum = 0
        self.grammar.ranking = []
        self.constraintName = ''
        self.cindex = 0
        self.viol = None
        self.violCell = False
        self.winComing = False
        self.inParm = False
        self.subViol = False
        self.cellClass = None
        self.parms = []
        self.cand = ''
        p = expat.ParserCreate('UTF-8')
        p.StartElementHandler = self.start_element
        p.EndElementHandler = self.end_element
        p.CharacterDataHandler = self.char_data
        p.ParseFile(fl)
        self.grammar.numCands = len(self.grammar.tabCands)
        self.grammar.makeTabCands = False
        return None

    def start_element(self, name, attrs):
        if name == 'td':
            self.cellClass = attrs.get('class', 'viol')
        elif name == 'sub':
            self.subViol = True
        elif name == 'span':
            self.inParm = True

    def end_element(self, name):
        if name == 'tr':
            self.cindex = 0
        if name == 'td' and self.cellClass == 'constraint':
            self.addConstraint()
            self.parms = []
        if name == 'td' and self.cellClass == 'cand':
            self.grammar.tabCands.append(self.cand)
            if self.winComing:
                self.grammar.winners.append((OT.Utterance(self.cand, lang=self.lang), 
                                             self.grammar.tabCands.index(self.cand)))
                self.winComing = False
            self.cand = ''
        if name == 'td':
            self.cellClass = None
        if name == 'td' and self.cellClass == 'viol':
            self.viol = None
        if name == 'sub':
            self.subViol = False
        if name == 'span':
            self.inParm = False

    def char_data(self, data):
        if self.inParm:
            self.parms.append(data)
        elif self.subViol:
            self.viol = int(data)
        elif self.cellClass == 'cand':
            self.cand += data
        elif self.cellClass == 'win':
            if data == u'\u261e':
                self.winComing = True
        elif self.cellClass == 'input':
            self.grammar.inp = OT.Utterance(data[1:-1], lang=self.lang)
        elif self.cellClass == 'viol':
            if set(data) <= set([u'*', u'!']):
                self.viol = data.count(u'*')
            elif self.viol is None:
                self.viol = int(data)
            self.grammar.violations[self.grammar.ranking[self.cindex]].append(self.viol)
            self.cindex += 1
            self.viol = None
        elif self.cellClass == 'constraint':
            self.constraintName += data

    def addConstraint(self):
        constr = None
        for (fname, uname) in names.iteritems():
            if uname == self.plusParms():
                try:
                    constr = (eval('constraints.%s' % fname),)
                except:
                    pass
        if constr is None:
            constr = self.revNameConstraint(self.constraintName, 
                                            self.parms)
        self.grammar.ranking.append(constr)
        self.grammar.violations[constr] = []
        self.constraintName = ''
        self.parms = []        

    def revNameConstraint(self, data, parms):
        mat = re.search(r'([A-Za-z]+)-\[([a-z]+)\]', data)
        if mat:
            constr = (eval('constraints.%s' % mat.group(1)), mat.group(2))
            return constr
        try:
            constr = [eval('constraints.%s' % data.strip('\n\t -'))]
        except:
            constr = [data]
        for parm in parms:
            try:
                constr.append(int(parm))
            except ValueError:
                constr.append(parm)
        constr = tuple(constr)
        return constr

    def plusParms(self):
        out = self.constraintName.strip('\n\t -')
        for parm in self.parms:
            out += '-<span class="param">%s</span>' % parm
        return out

def formatUtterance(utt, paren='', ucs=True):
    out = paren
    for unit in utt:
        if type(unit) == Syllable:
            if unit.MoraCount > len(unit.nuc) + len(unit.cod):
                out += formatUtterance(unit.ons, '', ucs)
                out += formatUtterance(unit.nuc, '', ucs)
                out += ucs and u'\u02D0' or ':'
                out += formatUtterance(unit.cod)
            else:
                for seg in flatten(unit):
                    out += seg.ipa
                out += '.'
        elif type(unit) == Segment:
            out += unit.ipa
    return out.strip('.') + paren

def namedViols(viols):
    violations = {}
    for viol in viols:
        violations[nameConstraint(viol)] = viols[viol]    
    return violations

def assembleComparativeTableau(g, rows):
    ranking = [nameConstraint(constr) for constr in g.ranking]
    ur = '/' + g.inp.prettyPrint('ipa') + '/'

    yield '\n\n'

    yield '<table class="tableau">\n<tr class="head">\n\t<td colspan="3" class="input">%s</td>\n' % ur

    for constr in ranking:
        yield '\t<td class="constraint">%s</td>\n' % constr

    yield '''</tr>
'''

    if not g.dopts:
        for win in g.winners[:-1]:
            yield u'<tr>\n\t<td class="num"></td><td class="win">\u261E</td><td class="cand">%s</td>\n' % (win['tc'])
            for vio in win['vio']:
                yield u'\t<td>%d</td>\n' % vio
            yield u'</tr>\n'
        win = g.winners[-1]
        yield u'<tr class="head">\n\t<td class="num"></td><td class="win">\u261E</td><td class="cand">%s</td>\n' % win['tc']
        for vio in win['vio']:
            yield u'\t<td>%d</td>\n' % vio
        yield u'</tr>\n'
    else:
        for dopt in g.dopts[:-1]:
            yield u'<tr>\n\t<td class="num"></td>'
            if dopt in g.winners:
                yield u'<td class="win">\u261e</td>'
            else:
                yield u'<td class="win">\u263A</td>'
            yield '<td class="cand">%s</td>\n' % dopt['tc']
            for vio in dopt['vio']:
                yield u'\t<td>%d</td>\n' % dopt
            yield u'</tr>\n'

        dopt = g.dopts[-1]
        yield u'<tr class="head">\n\t<td class="num"></td>'
        if dopt in g.winners:
            yield u'<td class="win">\u261e</td>'
        else:
            yield u'<td class="win">\u263A</td>'
        yield '<td class="cand">%s</td>\n' % dopt['tc']
        for vio in dopt['vio']:
            yield u'\t<td>%d</td>\n' % vio
        yield u'</tr>\n'

    cnum = 0    
    for cand in rows:
        if (cand not in g.winners and
            cand not in g.dopts):
            cnum += 1
            yield u'<tr>\n\t<td class="num">%d.</td><td class="win"></td>' % cnum
            yield u'<td class="cand">%s</td>\n' % cand['tc']
            for idx in range(len(ranking)):
                wldash = cand['comprow'][idx]
                if wldash == '-': wldash = u'—'
                yield (u'\t<td><sub>%d</sub>%s</td>\n' % 
                       (cand['vio'][idx], wldash))
            yield u'</tr>\n'
        
    yield '</table>'
 
def assembleTableau(g, rows, verbose=True):
    if g.dopts:
        dopts = [dopt.prettyPrint('ipa') for dopt in dopts]
    else:
        dopts = []

    ranking = [nameConstraint(constr) for constr in g.ranking]
    ur = '/' + g.inp.prettyPrint('ipa') + '/'

    yield '\n\n'

    yield '<table class="tableau">\n<tr class="head">\n\t<td colspan="3" class="input">%s</td>\n' % ur

    for constr in ranking:
        yield '\t<td class="constraint">%s</td>\n' % constr

    yield '''</tr>
'''

    cnum = 0
    for cand in rows:
        bang = False
        cnum += 1
        yield u'<tr>\n\t<td class="num">%d.</td><td class="win">' % cnum
        if cand in g.winners:
            if cand in dopts or not dopts:
                yield u'\u261e'
            else:
                yield u'\u263a'
        elif cand in g.dopts:
            yield u'\u2639'

        yield u'</td><td class="cand">%s</td>\n' % cand['tc']
        for idx in range(len(cand['vio'])):
            if not bang:
                vdiff = cand['crucials'][idx]
                if vdiff > 0:
                    starbang = '%s!%s' % ('*' * (cand['vio'][idx] - 
                                                 vdiff + 1),
                                          '*' * (vdiff - 1))
                    yield u'\t<td>%s</td>\n' % starbang
                    bang = True
                else:
                    yield (u'\t<td>%s</td>\n' % 
                           ('*' * cand['vio'][idx]))
            else:
                yield (u'\t<td class="elim">%s</td>\n' % 
                       ('*' * cand['vio'][idx]))
        yield u'</tr>\n'
    yield '</table>'

@batonify
def writeTableau(g, rows, verbose=True, **kwargs):
    fn = kwargs.get('filename', 'tableau.html')

    try:
        with config.get_file('template.html') as f:
            template = f.read()

            topntail = template.split('<!--Tableau goes here!-->')
            assert len(topntail) == 2

            with open(fn, 'wb') as f:
                f.write(topntail[0].encode('UTF-8'))
                if kwargs.get('tableau', 'comparative') == 'comparative':
                    for chunk in assembleComparativeTableau(g, rows):
                        f.write(chunk.encode('UTF-8'))
                else:
                    for chunk in assembleTableau(g, rows):
                        f.write(chunk.encode('UTF-8'))
                f.write(topntail[1].encode('UTF-8'))
    except:
        raise
    else:
        kwargs['baton'].message('Tableau written to %s' % path.normpath(fn))

    return 0
