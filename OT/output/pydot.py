from __future__ import with_statement

import yapgvb

from OT.representations import *
from common import sortedFeatures

class TreeDrawer():
    def __init__(self, **kwargs):
        self.symbols = {ProsodicPhrase: lambda x: 'P', #u'\u03A6', # Phi
                        ProsodicWord: lambda x: 'w', #u'\u03c9',   # omega
                        Foot: lambda x: 'F', #u'\u03a3',           # Sigma
                        Syllable: lambda x: 's', #u'\u03c3',       # sigma
                        Segment: self.labelSegment}


        self.tree = yapgvb.Graph('Tree')
        self.sonority = kwargs.get('sonority', None)

        self.nodes = {}
        self.links = ''
        self.strata = {ProsodicPhrase: 0, 
                       ProsodicWord: 0,   
                       Foot: 0,           
                       Syllable: 0, 
                       Segment: 0,
                       'Onset': 0,
                       'Rhyme': 0,
                       'Nucleus': 0,
                       'Coda': 0,
                       'Mora': 0}

    def labelSegment(self, seg):
        lines = []
        lines.append(seg.prettyPrint('xsampa'))
        for feat in sortedFeatures:
            if feat in seg:
                lines.append(feat.upper())
        if self.sonority: lines.append('Sonority: %d' % gradeSonority(seg))
        return '{%s}' % ' | '.join(lines)

    def drawSyllable(self, syll, node=None):
        for seg in syll.edgemostSegs('L'):
            segnode = self.addNode(seg)
            self.links += '\t%s -- %s;\n' % (node, segnode)

        if syll.ons:
            onsnode = self.addNode(None, 'Onset')
            
            self.links += '\t%s -- %s;\n' % (node, onsnode)

            for seg in syll.ons:
                segnode = self.addNode(seg)
                self.links += '\t%s -- %s;\n' % (onsnode, segnode)

        rhynode = self.addNode(None, 'Rhyme')
        nucnode = self.addNode(None, 'Nucleus')

        self.links += '\t%s -- %s;\n' % (node, rhynode)
        self.links += '\t%s -- %s;\n' % (rhynode, nucnode)

        last = None
        for seg in syll.nuc:
            mornode = self.addNode(None, 'Mora')
            if seg != last:
                segnode = self.addNode(seg)
            self.links += '\t%s -- %s;\n' % (nucnode, mornode)
            self.links += '\t%s -- %s;\n' % (mornode, segnode)
            last = seg

        if syll.cod:
            codnode = self.addNode(None, 'Coda')
            self.links +='\t%s -- %s;\n' % (rhynode, codnode)

            for seg in syll.cod:
                mornode = self.addNode(None, 'Mora')
                segnode = self.addNode(seg)
                self.links += '\t%s -- %s;\n' % (codnode, mornode)
                self.links += '\t%s -- %s;\n' % (mornode, segnode)

        for seg in syll.edgemostSegs('R'):
            segnode = self.addNode(seg)
            self.links += '\t%s -- %s;\n' % (node, segnode)

    def addNode(self, elem, level=None, **kwargs):
        head = kwargs.get('head', False)
        if level is None:
            level = type(elem)

        self.strata[level] += 1

        if type(level) is str:
            nodename = '%s%d' % (level, self.strata[level])
        else:
            nodename = '%s%d' % (level.__name__, self.strata[level])

        if level == 'Mora':
            self.nodes += u'\t"%s" [label="m" shape="none"];\n' % nodename
        elif type(level) is str:
            self.nodes += u'\t"%s" [label="%s" shape="none"];\n' % (nodename,
                                                                    level[0])
        else:
            self.nodes += self.nodeString(elem, head=head)
        return nodename
            
    def nodeString(self, rep, **kwargs):
        head = kwargs.get('head', False)
        return ('\t"%s%d" [label="%s" shape="%s"];\n' % 
                (type(rep).__name__,
                 self.strata[type(rep)], 
                 self.symbols[type(rep)](rep) + (head and u'\u209b' or ''),
                 type(rep) is Segment and 'record' or 'none'))

    def drawNodes(self, rep, node=None):        
        if node is None:
            node = self.addNode(rep)

        if type(rep) is Syllable:
            self.drawSyllable(rep, node)
        else:
            for idx in range(len(rep)):
                head = (idx == rep.head)
                if len(rep) == 1: head = False

                elem = rep[idx]
                subnode = self.addNode(elem, head=head)
                self.links += '\t%s -- %s;\n' % (node, subnode)
                if type(elem) is Syllable:
                    self.drawSyllable(elem, subnode)
                else:
                    self.drawNodes(elem, subnode)

    def assembleTree(self, rep):
        for elem in rep:
            self.nodes[repr(elem)] = self.tree.add_node(repr(elem))
            self.nodes[repr(elem)].label = self.symbols[type(elem)](elem)
            if repr(rep) in self.nodes:
                self.tree.add_edge(self.nodes[repr(elem)], self.nodes[repr(rep)])

            if type(elem) != Segment:
                self.assembleTree(elem)
        return None
        out = 'graph Tree {\n'
        nodes = 0
        
        for elem in rep:
            self.drawNodes(elem)

        out += self.nodes + '\n'
        out += self.links + '\n'

        ONC = '\t{rank=same; '
        
        for level in self.strata:
            if not level in ('Onset', 'Nucleus', 'Coda'):
                out += '\t{rank=same; '                
                for idx in range(1, self.strata[level] + 1):
                    if type(level) is str:
                        out += '"%s%d"; ' % (level, idx)
                    else:
                        out += '"%s%d"; ' % (level.__name__, idx)
                out += '}\n'
            else:
                for idx in range(1, self.strata[level] + 1):
                    ONC += '"%s%d"; ' % (level, idx)
        out += ONC
        out += '}\n'

        out += '}'
        return out

def writeTree(rep, f, **kwargs):
    format = kwargs.get('format', yapgvb.formats.png)
    assembler = TreeDrawer(**kwargs)
    assembler.assembleTree(rep)
    assembler.tree.layout(yapgvb.engines.dot)
    assembler.tree.render(f, format)
