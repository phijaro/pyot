#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import with_statement

import sys, OT, re, string
from os import path
from OT import config
from OT import constraints
from OT.representations import *
from OT.gen import Gen
from ui_util import Baton
from types import FunctionType

from common import sortedFeatures

smallcaps = (u'ᴀ', u'ʙ', u'ᴄ', u'ᴅ', u'ᴇ', u'ꜰ', 
             u'ɢ', u'ʜ', u'ɪ', u'ᴊ', u'ᴋ', u'ʟ', u'ᴍ',
             u'ɴ', u'ᴏ', u'ᴘ', None, u'ʀ', u'ꜱ', 
             u'ᴛ', u'ᴜ', u'ᴠ', u'ᴡ', None, u'ʏ', u'ᴢ',)
subscript = lambda n: unichr(0x2080 + n)

def AlignFootNamer(constr):
    return u'Align-[Σ, %s; ω, %s]' % (smallcap(constr[1]), 
                                      smallcap(constr[1]))

def AlignHeadFootInWordNamer(constr):
    return u'Align-[Σₛ, %s; ω, %s]</span>' % (smallcap(constr[1]), 
                                              smallcap(constr[1]))

def MaxFeatNamer(constr):
    return 'Max-[%s]' % smallcap(constr[1])

def DepFeatNamer(constr):
    return 'Dep-[%s]' % smallcap(constr[1])

def CondAgreeNamer(constr):
    return '[%s]-Agree-[%s]' % tuple([smallcap(f) for f in constr[1:]])

def GOCPNamer(constr):
    feat = constr[1]
    morae = constr[2]
    return (u'*%s-%s-%s' %
           (smallcap(feat), u'µ' * morae, smallcap(feat)))

def StarNamer(constr):
    return nameConstraint(constr, name=('*' + constr[0].__name__[4:]))

def StarFeatsNamer(constr):
    if len(constr[1:]) == 1:
        out = '*[%s]' % smallcap(constr[1])
    else:
        out = '*Seg[%s ' % smallcap(constr[1])
        for feat in constr[2:]:
            out += u'∧ %s ' % smallcap(feat)
        out = out.rstrip() + ']'
    return out

def EntailsFeatsNamer(constr):
    if type(constr[1]) in (str, unicode):
        out = u'[%s' % smallcap(constr[1])
    else:
        out = u'[%s ' % smallcap(constr[1][0])
        for feat in constr[1][1:]:
            out += u'∧ %s ' % smallcap(feat)
    out = out.rstrip() + u'] ⊃ '
    if type(constr[2]) in (str, unicode):
        out += u'[%s' % smallcap(constr[2])
    else:
        out += u'[%s ' % smallcap(constr[2][0])
        for feat in constr[2][1:]:
            out += u'∧ %s ' % smallcap(feat)
    return out.rstrip() + u']'

constraintNamers = {
    '^Star.+$': StarNamer,
    'StarFeats': StarFeatsNamer,
    'StarFeat': StarFeatsNamer,
    'StressMaxMora': lambda x: u'Max-µ-σₛ',
    'StarLongFeat': lambda constr: u'*Seg[%s]ː' % smallcap(constr[1]),
    'VOP': lambda x: u'VOP',
    'StarComplexOnset': lambda x: u'*ComplexOnset',
    'PVL': lambda x: u'*VµD…σ]',
    'Onset': lambda x: u'Onset',
    'DepSegment': lambda x: u'Dep-seg',
    'NoCoda': lambda x: u'NoCoda',
    'MaxMora': lambda x: u'Max-µ',
    'MaxSegment': lambda x: u'Max-seg',
    'StarComplexCoda': lambda x: u'*ComplexCoda',
    'StarTriMora': lambda x: u'*σ(µµµ)',
    'StarSemiVowel': lambda x: u'*SemiVowel',
    'AgreePlaceNode': lambda x: u'NodeAgree-ᴘʟᴀᴄᴇ',
    'DepMora': lambda x: u'Dep-µ',
    'StarVoicedCoda': lambda x: u'*VoicedCoda',
    'SabSonority': lambda x: u'Sonority',
    'ParseSyll': lambda x: u'Parse-σ',
    'AlignFoot': lambda x: u'Align-Σ',
    'Max_LongV': lambda x: u'Max-V\u0304',
    'FtTypeT': lambda x: u'FtType(T)',
    'WeightToStress': lambda x: u'Weight-To-Stress',
    'FtBin': lambda x: u'FtBin',
    'StarSyllC': lambda x: u'*C̩',
    'StarVoicelessV': lambda x: u'*V̥',               
    'StarTriphthong': lambda x: u'*Triphthong',
    'StarLongCoda': lambda x: u'*LongCoda',
    'MaxPriStressSeg': lambda x: u'Max-σ́-seg',
    'MaxDepWord': lambda x: u'Dep-ω',
    'StarNCsubring': lambda x: u'*NC̥',
    'StarGeminate': lambda x: u'*Geminate'
}

symbols = {ProsodicPhrase: u'Φ',
           ProsodicWord: u'ω',
           Foot: u'Σ',
           Syllable: u'σ'}

def LenLimitNamer(constr):
    if constr[2] == 0 and constr[1] == 'cod':
        out = u'NoCoda'
    elif constr[2] == 1:
        if constr[1] == 'cod':
            out = u'*ComplexCoda'
        elif constr[1] == 'ons':
            out = u'*ComplexOnset'
        elif constr[1] == 'nuc':
            out = u'*V\u0304'
    else:
        nodes = {'ons': 'Onset',
                 'nuc': 'Nuc',
                 'cod': 'Coda'}
        out = u'LenLimit-%s-%d' % (nodes[constr[1]], constr[2])
    out = extendParms(constr, out, parms=constr[3:])
    return out

def IdentCCNamer(constr):
    out = u'Ident-CC'
    out = extendParms(constr, out)
    return out    

def notIdentCCNamer(constr):
    out = u'¬Ident-CC'
    out = extendParms(constr, out)
    return out

def fallbackNamer(constr, name=None):
    if name is None:
        name = constr[0].__name__.replace('_', '-')
    out = name
    out = extendParms(constr, out)
    return out

def extendParms(constr, out, parms=None):
    if parms is None:
        parms = constr[1:]
    if set(parms) == set(['lab', 'cor', 'dor', 'rad']):
        out += r'-art'
    else:
        for param in parms:
            if param in features.values() or param in features.keys():
                out += '-' + '[%s]' % smallcap(param)
            elif param in symbols:
                out += '-%s' % symbols[param]
            else:
                out += '-%s' % unicode(param)
    return out

def nameConstraint(constr, name=None):
    if name is None:
        name = constr[0].__name__
    namer = None
    if constraintNamers.has_key(name):
        namer = constraintNamers[name]
    else:
        for key in constraintNamers.keys():
            if re.search(key, name):
                namer = constraintNamers[key]
    if namer is None:
        try:
            namer = eval('%sNamer' % name)
        except:
            namer = fallbackNamer
    return namer(constr)

def formatUtterance(utt, paren='', ucs=True):
    out = paren
    for unit in utt:
        if type(unit) == Syllable:
            if unit.MoraCount > len(unit.nuc) + len(unit.cod):
                out += formatUtterance(unit.ons, '', ucs)
                out += formatUtterance(unit.nuc, '', ucs)
                out += ucs and u'\u02D0' or ':'
                out += formatUtterance(unit.cod)
            else:
                for seg in flatten(unit):
                    out += seg.ipa
                out += '.'
        elif type(unit) == Segment:
            out += unit.ipa
    return out.strip('.') + paren

def smallcap(u):
    out = u''
    for char in u:
        if char in string.ascii_lowercase:
            idx = string.ascii_lowercase.index(char)
            if smallcaps[idx] is None:
                out += char.upper()
            else:
                out += smallcaps[idx]
        else:
            out += char
    return out
