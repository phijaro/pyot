#!/usr/bin/env python
from __future__ import with_statement

import sys, re
from OT import config, constraints
from OT.representations import *
from OT.gen import Gen
from ui_util import Baton
from types import FunctionType

def macroHead(constr):
    return '\\OTconstraint[%s]' % constraints.__repr_form(constr)

def AlignFootNamer(constr):
    return '\\OTconstraint{Align}{-[$\\Sigma$, %s; $\\omega$, %s]}' % (constr[1], constr[1])

def AlignHeadFootInWordNamer(constr):
    return '\\OTconstraint{Align}{-[$\\Sigma_s$, %s; $\\omega$, %s]}' % (constr[1], constr[1])

def MaxFeatNamer(constr):
    return '\\OTconstraint{Max}{-[\\feat{%s}]}' % constr[1]

def DepFeatNamer(constr):
    return '\\OTconstraint{Dep}{-[\\feat{%s}]}' % constr[1]

def CondAgreeNamer(constr):
    return '\\OTconstraint{[%s]-Agree}{-[\\feat{%s}]}' % constr[1:]

def StarLongFeatNamer(constr):
    return '\\OTconstraint{*Seg}{([\\feat{%s}])\\textipa{:}}' % constr[1]

def GOCPNamer(constr):
    feat = constr[1]
    morae = constr[2]
    return ('\\OTconstraint{*}{[\\feat{%s}]-$%s$-[\\feat{%s}]}' %
           (feat, r'\mu' * morae, feat))

def StarNamer(constr):
    return nameConstraint(constr, name=('*' + constr[0].__name__[4:]))

def StarFeatsNamer(constr):
    if len(constr[1:]) == 1:
        out = '\\OTconstraint{*}{[\\feat{%s}]}' % constr[1]
    else:
        out = '\\OTconstraint{*Seg}{[\\feat{%s} ' % constr[1]
        for feat in constr[2:]:
            out += '\\ensuremath{\\wedge} \\feat{%s} ' % feat
        out = out.rstrip() + ']}'
    return out    

def EntailsFeatsNamer(constr):
    out = r'\OTconstraint{}{'
    if type(constr[1]) in (str, unicode):
        out += '[\\feat{%s}' % constr[1]
    else:
        out += '[\\feat{%s} ' % constr[1][0]
        for feat in constr[1][1:]:
            out += '\\ensuremath{\\wedge} \\feat{%s} ' % feat
    out = out.rstrip() + r'] \ensuremath{\supset} '
    if type(constr[2]) in (str, unicode):
        out += '[\\feat{%s}' % constr[2]
    else:
        out += '[\\feat{%s} ' % constr[2][0]
        for feat in constr[2][1:]:
            out += '\\ensuremath{\\wedge} \\feat{%s} ' % feat
    return out.rstrip() + ']}{}'

constraintNamers = {
    '^Star.+$': StarNamer,
    'StarFeats': StarFeatsNamer,
    'StarFeat': StarFeatsNamer,
    'StarLongFeat': StarLongFeatNamer,
    'NonFinParse': lambda x: r'\OTconstraint{NonFinality}{}',
    'StarClash': lambda x: r'\OTconstraint{*Clash}{}',
    'OnsetSonority': lambda x: '\\OTconstraint{OnsetSonority}{-%d}' % x[1],
    'VOP': lambda x: r'\OTconstraint{VOP}{}',
    'StarComplexOnset': lambda x: r'\OTconstraint{*ComplexOnset}{}',
    'PVL': lambda x: r'\OTconstraint{*V\ensuremath{_\mu}D\ldots{}\ensuremath{_\sigma}]}{}',
    'OnsetSonLow': lambda x: r'\OTconstraint{OnsSonLow}{}',
    'Onset': lambda x: r'\OTconstraint{Onset}{}',
    'DepSegment': lambda x: r'\OTconstraint{Dep}{-seg}',
    'NoCoda': lambda x: r'\OTconstraint{NoCoda}{}',
    'MaxMora': lambda x: r'\OTconstraint{Max}{-$\mu$}',
    'MaxSegment': lambda x: r'\OTconstraint{Max}{-seg}',
    'StarComplexCoda': lambda x: r'\OTconstraint{*ComplexCoda}{}',
    'StarTriMora': lambda x: r'\OTconstraint{*\ensuremath{\sigma_{\mu\mu\mu}}}{}',
    'StarSemiVowel': lambda x: r'\OTconstraint{*SemiVowel}{}',
    'StarGeminate': lambda x: r'\OTconstraint{*Geminate}{}',
    'AgreePlaceNode': lambda x: r'\OTconstraint{NodeAgree-place}{}',
    'DepMora': lambda x: r'\OTconstraint{Dep}{-$\mu$}',
    'StarVoicedCoda': lambda x: r'\OTconstraint{*Voiced-Coda}{}',
    'SabSonority': lambda x: r'\OTconstraint{Sonority}{}',
    'ParseSyll': lambda x: r'\OTconstraint{Parse}{-$\sigma$}',
    'Max_LongV': lambda x: r'\OTconstraint{Max}{-\=V}',
    'FtTypeT': lambda x: r'\OTconstraint{FtType(T)}{}',
    'WeightToStress': lambda x: r'\OTconstraint{Weight-To-Stress}{}',
    'FtBin': lambda x: r'\OTconstraint{FtBin}{}',
    'StarSyllC': lambda x: r'\OTconstraint{*\textsyllabic{C}}{}',
    'StarVoicelessV': lambda x: r'\OTconstraint{*\textsubring{V}}{}',               
    'StarTriphthong': lambda x: r'\OTconstraint{*Triphthong}{}',
    'StarLongCoda': lambda x: r'\OTconstraint{*LongCoda}{}',
    'MaxPriStressSeg': lambda x: r"\OTconstraint{$\acute{\sigma}$Max}{-seg}",
    'MaxDepWord': lambda x: r'\OTconstraint{Dep}{-$\omega$}',
    'StarNCsubring': lambda x: r'\OTconstraint{StarN\textsubring{C}}',
    'StressMaxMora': lambda x: r'\OTconstraint{Max}{-\ensuremath{\mu}-\ensuremath{\sigma{}_s}}',
    'FtTypeT': lambda x: r'\OTconstraint{Align}{{[$\sigma_s$, L; $\Sigma$, L]}}',
    'FtTypeI': lambda x: r'\OTconstraint{Align}{{[$\sigma_s$, R; $\Sigma$, R]}}',
}

symbols = {ProsodicPhrase: '$\Phi$',
           ProsodicWord: '$\omega$',
           Foot: '$\Sigma$',
           Syllable: '$\sigma$'}

from common import sortedFeatures

def LenLimitNamer(constr):
    if constr[2] == 0 and constr[1] == 'cod':
        out = r'\OTconstraint{NoCoda}{'
    elif constr[2] == 1:
        if constr[1] == 'cod':
            out = r'\OTconstraint{*ComplexCoda}{'
        elif constr[1] == 'ons':
            out = r'\OTconstraint{*ComplexOnset}{'
        elif constr[1] == 'nuc':
            out = r'\OTconstraint{*\={V}}{'
    else:
        nodes = {'ons': 'Onset',
                 'nuc': 'Nuc',
                 'cod': 'Coda'}
        out = '\\OTconstraint{LenLimit}{-%s-%d' % (nodes[constr[1]], constr[2])
    out = extendParms(constr, out, parms=constr[3:])
    return out + '}'

def IdentCCNamer(constr):
    out = r'\OTconstraint{Ident-CC}{'
    out = extendParms(constr, out)
    out += '}'
    return out    

def notIdentCCNamer(constr):
    out = r'\OTconstraint{\ensuremath{\neg}Ident-CC}{'
    out = extendParms(constr, out)
    out += '}'
    return out

def WorLorDash(violations, constr, ind, winners, dopts):
    if dopts:
        target = dopts[0][1]
    else:
        target = winners[0][1]

    if violations[constr][ind] > violations[constr][target]:
        wldash = 'W'
    elif violations[constr][ind] < violations[constr][target]:
        wldash = 'L'
    else: 
        wldash = '---'
    return wldash

def fallbackNamer(constr, name=None):
    if name is None:
        name = constr[0].__name__.replace('_', '-')
    out = r'\OTconstraint{'
    out += name + '}{'
    out = extendParms(constr, out)
    return out + '}'

def extendParms(constr, out, parms=None):
    if parms is None:
        parms = constr[1:]
    if set(parms) == set(['lab', 'cor', 'dor', 'rad']):
        out += r'-\feat{art}'
    else:
        for param in parms:
            if param in features.values() or param in features.keys():
                out += '-' + '\\feat{[%s]}' % param
            elif param in symbols:
                out += '-' + symbols[param]
            else:
                out += '-' + str(param)
    return out

def nameConstraint(constr, name=None):
    if name is None:
        name = constr[0].__name__
    namer = None
    if constraintNamers.has_key(name):
        namer = constraintNamers[name]
    else:
        for key in constraintNamers.keys():
            if re.search(key, name):
                namer = constraintNamers[key]
    if namer is None:
        try:
            namer = eval('%sNamer' % name)
        except:
            namer = fallbackNamer
    codepart = '('
    codepart += name
    codepart += ', '
    for parm in constr[1:-1]:
        codepart += getattr(parm, '__name__', repr(parm))
        codepart += ', '
    if len(constr) > 1:
        parm = constr[-1]
        codepart += getattr(parm, '__name__', repr(parm))
    else:
        codepart = codepart.rstrip()
    codepart += ')'
    out = namer(constr)
    out = out.partition(r'\OTconstraint')
    out = out[0] + out[1] + '[' + codepart + ']' + out[2]
    return out

def namedViols(viols):
    violations = {}
    for viol in viols:
        violations[nameConstraint(viol)] = viols[viol]    
    return violations

class TreeDrawer():
    def __init__(self, rep, **kwargs):
        self.rows = [[], [], []]
        self.rowDict = {Segment: self.rows[-3],
                        'feat': self.rows[-2],
                        'sonority': self.rows[-1]}
        self.virtualSegCoords = []
        self.coords = []
        self.sonority = kwargs.get('sonority', False)
        self.indices = kwargs.get('indices', False)
        pointsize = kwargs.get('pointsize', 10)
        self.scale_factor = (pointsize - 10) / 2

        for level in levels:
            if flattenable(rep, level):
                self.draw(flatten(rep, level), level)

    def draw(self, flatRep, level):
        if level is Segment:
            self.drawSegments(flatRep)
        elif level is Syllable:
            self.drawSyllables(flatRep)
        elif level is Foot:
            self.drawFeet(flatRep)
        else:
            self.drawHighLevel(flatRep)

    def lineCommand(self, fro, to):
        if type(fro[0]) is str:
            fromrow = self.rows.index(self.rowDict[fro[0]])
        else:
            fromrow = self.rows.index(self.rowDict[type(fro[0])])
        if type(to[0]) is str:
            torow = self.rows.index(self.rowDict[to[0]])
        else:
            torow = self.rows.index(self.rowDict[type(to[0])])

        fromcol = self.getCoord(*fro)
        tocol = self.getCoord(*to)
        if tocol is None:
            tocol = self.getCoordEnough(*to)

        rel = ''
        vert = torow - fromrow
        if vert > 0:
            rel += 'd' * vert
        else:
            rel += 'u' * abs(vert)
        horz = tocol - fromcol
        if horz > 0:
            rel += 'r' * horz
        else:
            rel += 'l' * abs(horz)

        return '\\B{%s}' % rel

    def identity(self, tup1, tup2):
        if len(tup1) != len(tup2):
            return False
        for idx in range(len(tup1)):
            if tup1[idx] is not tup2[idx]:
                return False
        return True

    def getVCoord(self, *args):
        for coord in self.virtualSegCoords:
            if self.identity(coord[:-1], args):
                return coord[-1]
        raise KeyError, args

    def getCoord(self, *args):
        for coord in self.coords:
            if self.identity(coord[:-1], args):
                return coord[-1]
        raise KeyError, args

    def getCoordEnough(self, *args):
        for coord in self.coords:
            if coord[:-1] == args:
                return coord[-1]
        raise KeyError, args

    def markNode(self, unit, marking='_s'):
        nodeText = self.rowDict[type(unit)][self.getCoord(unit)]
        rex = re.compile(r'^\\K[{][$]([^$]+)[$][}]((?:\\B[{][udlr]+[}])*)$')
        repl = '\\K{$\\1%s$}\\2' % marking
        nodeText = rex.sub(repl, nodeText)
        self.rowDict[type(unit)][self.getCoord(unit)] = nodeText

    def drawHighLevel(self, flatRep):
        self.rows.insert(0, [])
        self.rowDict[type(flatRep[0])] = self.rows[0]

        for unit in flatRep:
            self.locateNode(unit)
            self.rowDict[type(unit)].extend(('',) * (len(flatten(unit)) * 2))
            if type(unit) in symbols:
                node = '\K{%s}' % symbols[type(unit)]
            else:
                node = '\\K{%s}' % repr(type(unit))
            self.rowDict[type(unit)][self.getCoord(unit)] = node
            for down in unit:
                self.rowDict[type(unit)][self.getCoord(unit)] += self.lineCommand((unit,), (down,))
                if len(unit) > 1 and hasattr(unit, 'head'):                    
                    if unit[unit.head] is down:
                        self.markNode(down)
                    else:
                        self.markNode(down, '_w')

    def locateNode(self, obj):
        namemapping = {'O': ('ons',),
                       'N': ('nuc',),
                       'C': ('cod',)}
        if (type(obj) is tuple and
            obj[0] in ('O', 'N', 'C') and
            isinstance(obj[1], Syllable)):
            chunk = []
            for name in namemapping[obj[0]]:
                chunk.extend(getattr(obj[1], name))
            start = self.getVCoord(chunk[0])
            stop = self.getVCoord(chunk[-1])
            out = obj
        else:
            start = self.getCoord(obj[0])
            stop = self.getCoord(obj[-1])
            out = (obj,)
        self.coords.append(out + ((start + (stop - start) / 2),))

    def drawFeet(self, flatRep):
        syllrow = self.rows.index(self.rowDict[Syllable])
        self.rows.insert(syllrow, [])
        self.rowDict[Foot] = self.rows[syllrow]

        for foot in flatRep:
            self.rowDict[Foot].extend(('',) * (len(flatten(foot)) * 2))
            if type(foot) is Foot:
                self.locateNode(foot)
                self.rowDict[Foot][self.getCoord(foot)] = r'\K{$\Sigma$}'
                for unit in foot:
                    self.rowDict[Foot][self.getCoord(foot)] += self.lineCommand((foot,), (unit,))
                    if len(foot) > 1:
                        if foot[foot.head] is unit:
                            self.markNode(unit)
                        else:
                            self.markNode(unit, '_w')

    def drawSyllables(self, flatRep):
        segrow = self.rows.index(self.rowDict[Segment])
        self.rows.insert(segrow, [])
        self.rowDict['mora'] = self.rows[segrow]
        self.rows.insert(segrow, [])
        self.rowDict['O'] = self.rows[segrow]
        self.rowDict['N'] = self.rows[segrow]
        self.rowDict['C'] = self.rows[segrow]
        self.rows.insert(segrow, [])
        self.rowDict['R'] = self.rows[segrow]
        self.rows.insert(segrow, [])
        self.rowDict[Syllable] = self.rows[segrow]
        newrows = ('mora', 'O', 'R', Syllable)

        for syll in flatRep:
            # start = self.getCoord(syll.ons[0])
            # stop = self.getCoord(syll.ons[-1])

            # self.coords.append(('O', syll, start + (stop - start) / 2))
            # start = stop
            # moracol = stop + 1

            # stop = start + (len(syll.nuc) * 2)
            # self.coords.append(('N', syll, start + ((stop - start) / 2)))
            # start = stop
            
            # stop = start + (len(syll.cod) * 2)
            # self.coords.append(('C', syll, start + ((stop - start) / 2)))
            # start = stop
            self.locateNode(('N', syll))
            if syll.ons:
                self.locateNode(('O', syll))
            if syll.cod:
                self.locateNode(('C', syll))

            start = self.getCoord('N', syll)
            if syll.cod:
                stop = self.getCoord('C', syll)
            else:
                stop = start
            self.coords.append(('R', syll, start + ((stop - start) / 2)))           

            if syll.edgemostSegs('L'):
                start = self.getCoord(syll.edgemostSegs('L')[0])
            elif syll.ons:
                start = self.getCoord('O', syll)
            else:
                start = self.getCoord('R', syll)
            if syll.edgemostSegs('R'):
                start = self.getCoord(syll.edgemostSegs('R')[-1])
            else:
                stop = self.getCoord('R', syll)

            self.coords.append((syll, start + ((stop - start) / 2)))

            for seg in syll:
                for row in newrows:
                    self.rowDict[row].extend(('', ''))

            self.rowDict[Syllable][self.getCoord(syll)] = '\\K{$\\sigma$}'
            if syll.ons:
                self.rowDict['O'][self.getCoord('O', syll)] = '\\K{O}'
                self.rowDict[Syllable][self.getCoord(syll)] += self.lineCommand((syll,), ('O', syll))
            self.rowDict['R'][self.getCoord('R', syll)] = '\\K{R}'
            self.rowDict[Syllable][self.getCoord(syll)] += self.lineCommand((syll,), ('R', syll))
            self.rowDict['N'][self.getCoord('N', syll)] = '\\K{N}'
            self.rowDict['R'][self.getCoord('R', syll)] += self.lineCommand(('R', syll,), ('N', syll))
            if syll.cod:
                self.rowDict['C'][self.getCoord('C', syll)] = '\\K{C}'
                self.rowDict['R'][self.getCoord('R', syll)] += self.lineCommand(('R', syll,), ('C', syll))

            for seg in syll.ons:
                self.rowDict['O'][self.getCoord('O', syll)] += self.lineCommand(('O', syll), (seg,))
            for seg in syll.nuc:
                moracol = self.getVCoord(seg)
                self.rowDict['mora'][moracol] = r'\K{$\mu$}'
                self.coords.append(('mora', seg, moracol))
                self.rowDict['mora'][moracol] += self.lineCommand(('mora', seg), (seg,))
                self.rowDict['N'][self.getCoord('N', syll)] += self.lineCommand(('N', syll), ('mora', seg))
            for seg in syll.cod:
                moracol = self.getVCoord(seg)
                self.rowDict['mora'][moracol] = r'\K{$\mu$}'
                self.coords.append(('mora', seg, moracol))
                self.rowDict['mora'][moracol] += self.lineCommand(('mora', seg), (seg,))
                self.rowDict['C'][self.getCoord('C', syll)] += self.lineCommand(('C', syll), ('mora', seg))
            for seg in syll.edgemostSegs('R'):
                self.rowDict[Syllable][self.getCoord(syll)] += self.lineCommand((syll,), (seg,))
            for seg in syll.edgemostSegs('L'):
                self.rowDict[Syllable][self.getCoord(syll)] += self.lineCommand((syll,), (seg,))
                    

    def drawSegment(self, seg, wid):
        lm = ''
        if wid > 2:
            lm += ':'

        index = ''
        if self.indices:
            index += '$_{%d}$' % seg.indices['IO']
            
        drawcoord = (len(self.rowDict[Segment]) - (wid / 2))
        self.rowDict[Segment][drawcoord] = ('\\K{\\textipa{%s%s}%s}' %
                                            (seg.prettyPrint('tipa'),
                                             lm,
                                             index))
        self.rowDict['feat'][drawcoord] = self.featArray(seg)
        if self.sonority:
            self.rowDict['sonority'][drawcoord] = ('\\K{\\oldstylenums{%d}}' %
                                            gradeSonority(seg))
        self.coords.append((seg, drawcoord))
        if wid > 2:
            self.virtualSegCoords.append((seg, drawcoord + (wid / 2)))
        else:
            self.virtualSegCoords.append((seg, drawcoord))
            
        return drawcoord

    def drawSegments(self, flatRep):
        wid = 0
        last = flatRep[0]
        notdrawn = []

        for seg in flatRep[1:]:
            wid += 1
            self.rowDict[Segment].extend(('',))
            self.rowDict['feat'].extend(('',))
            self.rowDict['sonority'].extend(('',))
            if last != seg:
                wid += 1
                self.rowDict[Segment].extend(('',))
                self.rowDict['feat'].extend(('',))
                self.rowDict['sonority'].extend(('',))
                drawcoord = self.drawSegment(last, wid)
                start = (drawcoord - (wid / 2))
                wid = 0
                for nd_seg in notdrawn:
                    self.coords.append((nd_seg, drawcoord))
                    self.virtualSegCoords.append((nd_seg, start))
                    start += 2
                notdrawn = []
            else:
                notdrawn.append(last)
            last = seg
        self.rowDict[Segment].extend(('', ''))
        self.rowDict['feat'].extend(('', ''))
        self.rowDict['sonority'].extend(('', ''))
        drawcoord = self.drawSegment(seg, wid + 2)
        start = (drawcoord - (wid / 2))
        for nd_seg in notdrawn:
            self.coords.append((nd_seg, drawcoord))
            self.virtualSegCoords.append((nd_seg, start))
            start += 2

        if self.sonority:
            self.rowDict['sonority'][0] = '\K{Sonority:}'
            ff = max([len(seg) for seg in flatRep])
            ff = (((-3 - self.scale_factor) * ff) + 13) 
            for idx in range(len(self.rowDict['sonority'])):
                if r'\K' in self.rowDict['sonority'][idx]:
                    self.rowDict['sonority'][idx] = self.rowDict['sonority'][idx].replace(r'\K', '\\Kk{%d}' % ff)

    def featArray(self, seg):
        out = r' \Kk{%d}{$ \left[ \begin{array}{l}' % ((-2 * len(seg)) +
                                                       (9 - self.scale_factor))
        for feat in sortedFeatures:
            if feat in seg:
                out += '\\mbox{\\textsc{%s}}\\\\' % feat
        out = out.rstrip('\\\\')
        out += r'\end{array} \right]$}'
        return out

    def lines(self):
        yield '\\Tree{\n'
        for row in self.rows:
            yield ' & '.join(row) + ' \\\\\n'
        yield '}\n'

def assembleTableau(g, rows):
    ranking = [nameConstraint(constr) for constr in g.ranking]
    ur = '/\\textipa{' + g.inp.prettyPrint('tipa') + '}/'

    yield '\\begin{tableau}{%s}\n' % ('c|' * len(ranking)).rstrip('|')
    yield '  \\inp{%s}' % ur
    for constr in ranking:
        yield ' \\const{%s}' % constr
    yield '\\endhead'
    
    for cand in rows:
        bang = False
        yield '\n  \\cand'
        if cand in g.winners:
            yield '[\\HandRight]'
        yield '{%s}' % cand['tc']

        for ind in range(len(ranking)):
            if not bang:
                vdiff = cand['crucials'][ind]
                if vdiff > 0:
                    starbang = '%s!%s' % ('*' * (cand['vio'][ind] - 
                                                 vdiff + 1),
                                          '*' * (vdiff - 1))
                    yield '  \\vio{%s}' % starbang
                    bang = True
                else:
                    yield '  \\vio{%s}' % ('*' * 
                                           violations[constr][ind])
            else:
                yield '  \\vio{%s}' % ('*' * 
                                       violations[constr][ind])
        yield '\n'

    yield '\\end{tableau}\n\n'

def assembleComparativeTableau(g, rows, **kwargs):
    wide = kwargs.get('wide', False)
    huge = kwargs.get('long', False)

    ranking = [nameConstraint(constr) for constr in g.ranking]
    ur = '/\\textipa{' + g.inp.prettyPrint('tipa') + '}/'

    yield '\\begin{%stableau}{%s}\n' % (huge and 'long' or '',
                                        ('c|' * len(g.ranking)).rstrip('|'))
    yield '  \\inp{%s}\n' % ur
    for constr in ranking:
        yield '  \\const{%s{%s}}\n' % (wide and '\\rotatebox{270}' or '',
                                       constr)

    yield '\n'
    if g.dopts:
        for dopt in g.dopts:
            yield '  \\cand'
            if dopt in g.winners:
                yield '[\\HandRight]{%s}' % dopt['tc']
            else:
                yield '[\\OTface*]{%s}' % dopt['tc']
            for vio in dopt['vio']:
                yield '\\vio{%d}' % vio
            yield '\n'
    else:
        for cand in g.winners:
            yield ('  \\cand[\\HandRight]{%s}' % cand['tc'])
            for vio in cand['vio']:
                yield ' \\vio{%d}' % vio
            yield '\n\n'
    if huge: yield '\\endhead'

    for cand in rows:
        if (cand not in g.winners and
            cand not in g.dopts):
            yield ' \\cand[$\sim$]{%s}' % cand['tc']
            for idx in range(len(ranking)):
                wldash = cand['comprow'][idx]
                if wldash == '-':
                    wldash = '---'
                yield ' \\vio{$_%d$%s}' % (cand['vio'][idx], wldash)
            yield '\n\n'

    yield '\\end{%stableau}' % (huge and 'long' or '')

def assembleTablxComparativeTableau(g, rows, wide=True):
    ranking = ['%s{%s}' % (wide and '\\rotatebox{270}' or '', 
                           nameConstraint(constr)) for constr in g.ranking]
    ur = '/' + g.inp.prettyPrint('tipa') + '/'

    yield ('\n\n\\begin{OTcomparative}{%d}\n\t\\OTsolids{%s}\n' % 
           (len(ranking), ','.join([str(d) for d in range(1, len(ranking))])))
    yield '\t\\OTtoprow[%s]{' % ur
    yield ','.join(ranking)
    yield '}\n'

    if g.dopts:
        toprows = g.dopts
    else:
        toprows = g.winners

    for cand in toprows:
        yield '\n\t'
        if cand is toprows[-1]:
            yield r'\OTcandrow'
        else:
            yield r'\OTwinrow'
        if cand in g.winners:
            yield r'[\OThand]'
        else:
            yield r'[\OTface*]'
        yield ' {%s} ' % cand['tc']
        yield ' {%s}' % ','.join([str(d) for d in cand['vio']])

    for cand in rows:
        if not cand in toprows:
            yield '\n\t'
            yield '\OTcandrow {%s} ' % cand['tc']
            cells = [compVioCell(cand, idx) for idx in range(len(ranking))]
            yield '{%s}' % ','.join(cells)

    yield '\n\\end{OTcomparative}'

def compVioCell(cand, idx):
    wldash = cand['comprow'][idx]
    if wldash == '-':
        wldash = '---'
    return '\\OTcompviol[%d]{%s}' % (cand['vio'][idx], wldash)

def starBang(cand, idx, bang):
    if not bang:
        vdiff = cand['crucials'][idx]
        if vdiff > 0:
            starbang = '%s!%s' % ('*' * (cand['vio'][idx] - 
                                         vdiff + 1),
                                  '*' * (vdiff - 1))
            return starbang
            bang = True
        else:
            return ('*' * cand['vio'][idx])
    else:
        return ('*' * cand['vio'][idx])

def assembleTablxTableau(g, rows, **kwargs):
    ranking = [nameConstraint(constr) for constr in ranking]
    ur = '/' + ur.prettyPrint('tipa') + '/'

    yield ('\n\n\\begin{OTtableau}{%d}\n\t\\OTsolids{%s}\n' % 
           (len(ranking), ','.join([str(d) for d in range(1, len(ranking))])))
    yield '\t\\OTtoprow[%s]{' % ur
    yield ','.join(ranking)
    yield '}\n'
    
    for cand in rows:
        bang = False
        yield '\t\\OTcandrow'
        if cand in g.winners:
            yield '[\\OThand]'
        yield ' {%s} ' % cand['tc']
        viostrings = []
        for idx in range(len(ranking)):
            if not bang:
                vdiff = cand['crucials'][idx]
                if vdiff > 0:
                    starbang = '%s!%s' % ('*' * (cand['vio'][idx] - 
                                                 vdiff + 1),
                                          '*' * (vdiff - 1))
                    viostrings.append(starbang)
                    bang = True
                else:
                    viostrings.append('*' * cand['vio'][idx])
            else:
                viostrings.append('*' * cand['vio'][idx])
        yield '{%s}' % ','.join(viostrings)
        yield '\n'

    yield '\\end{OTtableau}'

def writeTableau(ranking, violations, ur, winners, 
                 **kwargs):
    comparative = (kwargs.get('tableau', 'comparative') == 'comparative')
    cands = kwargs.get('cands', None)
    verbose = kwargs.get('verbose', True)
    filename = kwargs.get('filename', 'tableau.tex')
    template = kwargs.get('template', True)
    tablx = kwargs.get('tablx', True)

    if template:
        with config.get_file('template.tex') as f:
            template = f.read()

            topntail = template.split('% Tableau goes here!')
            assert len(topntail) == 2

    with open(filename, 'wb') as f:
        if template: f.write(topntail[0])
        if comparative and tablx:
            for chunk in assembleTablxComparativeTableau(ranking,
                                                         violations,
                                                         ur,
                                                         winners,
                                                         **kwargs):
                f.write(chunk)
        elif comparative:
            for chunk in assembleComparativeTableau(ranking,
                                                    violations,
                                                    ur,
                                                    winners,
                                                    **kwargs):
                f.write(chunk)            
        elif tablx:
            for chunk in assembleTablxTableau(ranking,
                                              violations,
                                              ur,
                                              winners,
                                              **kwargs):
                f.write(chunk)
        else:
            for chunk in assembleTableau(ranking,
                                         violations,
                                         ur,
                                         winners,
                                         **kwargs):
                f.write(chunk)
        if template: f.write(topntail[1])

    return 0

def writeTree(rep, f, **kwargs):
    template = kwargs.get('template', True)
    if template:
        with config.get_file('template.tex') as t:
            template = t.read()

        topntail = template.split('% Tableau goes here!')
    else:
        topntail = ('', '')
    assert len(topntail) == 2

    td = TreeDrawer(rep, **kwargs)
    f.write(topntail[0])
    for chunk in td.lines():
        f.write(chunk)
    f.write(topntail[1])
