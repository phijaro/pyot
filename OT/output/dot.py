from __future__ import with_statement

from OT.representations import *
from OT.output import json_persist
from common import sortedFeatures

def labelSegment(seg, sonority=False):
    lines = []
    lines.append(seg['ipa'])
    for feat in sortedFeatures:
        if feat in seg['content']:
            lines.append(feat.upper())
    if sonority: lines.append('Sonority: %d' % gradeSonority(seg))
    return '{%s}' % ' | '.join(lines)

symbols = {'ProsodicPhrase': lambda x: u'\u03A6', # Phi
           'ProsodicWord': lambda x: u'\u03c9',   # omega
           'Foot': lambda x: u'\u03a3',           # Sigma
           'Syllable': lambda x: u'\u03c3',       # sigma
           'Segment': labelSegment}

def assembleTreeFromSerialized(obj, cosmetic={'shape': 'none'}):
    '''Returns the DOT source necessary to draw the trees for the
    objects represented by the dictionary obj, which is expected to be
    of the form returned by
    OT.output.json_persist.serializableObject().'''
    out = '''graph "Tree"
    {
'''

    for uuid in json_persist.span_tree(obj):
        thisobj = obj['objects'][uuid].copy()
        if uuid not in obj['content']:
            thisobj['symbol'] = symbols[thisobj['type']](thisobj)
            thisobj['cosmetic'] = cosmetic.copy()
            if thisobj['type'] == 'Segment':
                thisobj['cosmetic']['shape'] = "record"
            cosmetic_str = ' '.join('%s="%s"' % (key, value) for (key, value) in thisobj['cosmetic'].iteritems())
            thisobj['cosmetic'] = cosmetic_str

            out += '    "%(type)s-%(uuid)s" [id="%(uuid)s" label="%(symbol)s" %(cosmetic)s]\n' % thisobj
            if thisobj['type'] == 'Syllable':
                if thisobj['ons']:
                    out += '    "Onset-%(uuid)s" [id="Onset-%(uuid)s" label="O" %(cosmetic)s]\n' % thisobj
                out += '    "Rhyme-%(uuid)s" [id="Rhyme-%(uuid)s" label="R" %(cosmetic)s]\n' % thisobj
                out += '    "Nucleus-%(uuid)s" [id="Nucleus-%(uuid)s" label="N" %(cosmetic)s]\n' % thisobj
                if thisobj['cod']:
                    out += '    "Coda-%(uuid)s" [id="Coda-%(uuid)s" label="C" %(cosmetic)s]\n' % thisobj
    
    out += '\n'

    for uuid in json_persist.span_tree(obj):
        mother = obj['objects'][uuid]

        if uuid not in obj['content']:
            ons_string =  '    "Syllable-%(uuid)s" -- "Onset-%(uuid)s"\n' % mother
            nuc_string =  '    "Syllable-%(uuid)s" -- "Rhyme-%(uuid)s"\n' % mother
            nuc_string += '    "Rhyme-%(uuid)s" -- "Nucleus-%(uuid)s"\n' % mother
            cod_string =  '    "Rhyme-%(uuid)s" -- "Coda-%(uuid)s"\n' % mother

            if mother['type'] != 'Segment':
                for duuid in mother['content']:
                    daughter = obj['objects'][duuid]
                    if mother['type'] == 'Syllable':
                        if duuid in mother['ons']:
                            out += ons_string
                            out += '    "Onset-%s" -- "%s-%s"\n' % (uuid, daughter['type'], duuid)
                            ons_string = ''
                        elif duuid in mother['nuc']:
                            out += nuc_string
                            out += '    "Nucleus-%s" -- "%s-%s"\n' % (uuid, daughter['type'], duuid)
                            nuc_string = ''
                        elif duuid in mother['cod']:
                            out += cod_string
                            out += '    "Coda-%s" -- "%s-%s"\n' % (uuid, daughter['type'], duuid)
                            cod_string = ''
                        else:
                            out += '    "Syllable-%s" -- "%s-%s"\n' % (uuid, daughter['type'], duuid)
                    else:
                        out += '    "%s-%s" -- "%s-%s"\n' % (mother['type'], uuid, daughter['type'], duuid)

    lasttype = None
    out += '\n'
    for thisobj in sorted(obj['objects'].itervalues(), key=lambda x: x['type']):
        if thisobj['uuid'] not in obj['content']:
            if thisobj['type'] == lasttype:
                out += '"%(type)s-%(uuid)s"; ' % thisobj
            elif lasttype is None:
                out += '    {rank=same; "%(type)s-%(uuid)s"; ' % thisobj
                lasttype = thisobj['type']
            else:
                out += '}\n    {rank=same;  "%(type)s-%(uuid)s"; ' % thisobj
                lasttype = thisobj['type']
    out += '}\n    {rank=same; '
    
    for thisobj in obj['objects'].itervalues():
        if thisobj['type'] == 'Syllable':
            if thisobj['ons']:
                out += '"Onset-%(uuid)s"; ' % thisobj
            out += '"Nucleus-%(uuid)s"; ' % thisobj
            if thisobj['cod']:
                out += '"Coda-%(uuid)s"; ' % thisobj
    out += '}\n'

    return out + '}\n'

class TreeDrawer():
    def __init__(self, **kwargs):
        self.symbols = {ProsodicPhrase: lambda x: u'\u03A6', # Phi
                        ProsodicWord: lambda x: u'\u03c9',   # omega
                        Foot: lambda x: u'\u03a3',           # Sigma
                        Syllable: lambda x: u'\u03c3',       # sigma
                        Segment: self.labelSegment}

        self.nodes = ''
        self.links = ''
        self.sonority = kwargs.get('sonority', None)
        self.strata = {ProsodicPhrase: 0, 
                       ProsodicWord: 0,   
                       Foot: 0,           
                       Syllable: 0, 
                       Segment: 0,
                       'Onset': 0,
                       'Rhyme': 0,
                       'Nucleus': 0,
                       'Coda': 0,
                       'Mora': 0}

    def labelSegment(self, seg):
        lines = []
        lines.append(seg.prettyPrint())
        for feat in sortedFeatures:
            if feat in seg:
                lines.append(feat.upper())
        if self.sonority: lines.append('Sonority: %d' % gradeSonority(seg))
        return '{%s}' % ' | '.join(lines)

    def drawSyllable(self, syll, node=None):
        for seg in syll.edgemostSegs('L'):
            segnode = self.addNode(seg)
            self.links += '\t%s -- %s;\n' % (node, segnode)

        if syll.ons:
            onsnode = self.addNode(None, 'Onset')
            
            self.links += '\t%s -- %s;\n' % (node, onsnode)

            for seg in syll.ons:
                segnode = self.addNode(seg)
                self.links += '\t%s -- %s;\n' % (onsnode, segnode)

        rhynode = self.addNode(None, 'Rhyme')
        nucnode = self.addNode(None, 'Nucleus')

        self.links += '\t%s -- %s;\n' % (node, rhynode)
        self.links += '\t%s -- %s;\n' % (rhynode, nucnode)

        last = None
        for seg in syll.nuc:
            mornode = self.addNode(None, 'Mora')
            if seg != last:
                segnode = self.addNode(seg)
            self.links += '\t%s -- %s;\n' % (nucnode, mornode)
            self.links += '\t%s -- %s;\n' % (mornode, segnode)
            last = seg

        if syll.cod:
            codnode = self.addNode(None, 'Coda')
            self.links +='\t%s -- %s;\n' % (rhynode, codnode)

            for seg in syll.cod:
                mornode = self.addNode(None, 'Mora')
                segnode = self.addNode(seg)
                self.links += '\t%s -- %s;\n' % (codnode, mornode)
                self.links += '\t%s -- %s;\n' % (mornode, segnode)

        for seg in syll.edgemostSegs('R'):
            segnode = self.addNode(seg)
            self.links += '\t%s -- %s;\n' % (node, segnode)

    def addNode(self, elem, level=None, **kwargs):
        head = kwargs.get('head', False)
        if level is None:
            level = type(elem)

        self.strata[level] += 1

        if type(level) is str:
            nodename = '%s%d' % (level, self.strata[level])
        else:
            nodename = '%s%d' % (level.__name__, self.strata[level])

        if level == 'Mora':
            self.nodes += u'\t"%s" [label="\u03bc" shape="none"];\n' % nodename
        elif type(level) is str:
            self.nodes += u'\t"%s" [label="%s" shape="none"];\n' % (nodename,
                                                                    level[0])
        else:
            self.nodes += self.nodeString(elem, head=head)
        return nodename
            
    def nodeString(self, rep, **kwargs):
        head = kwargs.get('head', False)
        return ('\t"%s%d" [label="%s" shape="%s"];\n' % 
                (type(rep).__name__,
                 self.strata[type(rep)], 
                 self.symbols[type(rep)](rep) + (head and u'\u209b' or ''),
                 type(rep) is Segment and 'record' or 'none'))

    def drawNodes(self, rep, node=None):        
        if node is None:
            node = self.addNode(rep)

        if type(rep) is Segment:
            pass
        elif type(rep) is Syllable:
            self.drawSyllable(rep, node)
        else:
            for idx in range(len(rep)):
                head = (idx == getattr(rep, 'head', None))
                if len(rep) == 1: head = False

                elem = rep[idx]
                subnode = self.addNode(elem, head=head)
                self.links += '\t%s -- %s;\n' % (node, subnode)
                if type(elem) is Syllable:
                    self.drawSyllable(elem, subnode)
                else:
                    self.drawNodes(elem, subnode)

    def assembleTree(self, rep):
        out = 'graph Tree {\n'
        nodes = 0
        
        for elem in rep:
            self.drawNodes(elem)

        out += self.nodes + '\n'
        out += self.links + '\n'

        ONC = '\t{rank=same; '
        
        for level in self.strata:
            if not level in ('Onset', 'Nucleus', 'Coda'):
                out += '\t{rank=same; '                
                for idx in range(1, self.strata[level] + 1):
                    if type(level) is str:
                        out += '"%s%d"; ' % (level, idx)
                    else:
                        out += '"%s%d"; ' % (level.__name__, idx)
                out += '}\n'
            else:
                for idx in range(1, self.strata[level] + 1):
                    ONC += '"%s%d"; ' % (level, idx)
        out += ONC
        out += '}\n'

        out += '}'
        return out

def writeTree(rep, f, **kwargs):
    assembler = TreeDrawer(**kwargs)
    f.write(assembler.assembleTree(rep).encode('UTF-8'))
