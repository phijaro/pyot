'''Functions for converting PyOT representations into a format that
can be serialized as JSON.'''
from json import JSONEncoder
from types import FunctionType
from uuid import uuid4, UUID
import collections

import OT
from OT.representations import *
from OT import constraints

class grammarEncoder(JSONEncoder):
    def default(self, o):
        if type(o) is FunctionType:
            return o.__name__
        return JSONEncoder.default(self, o)

class UUIDMapping(collections.MutableMapping):
    '''Custom mapping object tracking one-to-one-to-one relations
    between UUIDs, objects from OT.representations and the
    dictionaries they are serialized into.'''
    def __init__(self, mapping=[]):
        self.mapping = mapping[:]

    def __repr__(self):
        return 'UUID_mapping(%s)' % repr(self.mapping)

    def __len__(self):
        return len(self.mapping)

    def __contains__(self, key):
        for triple in self.mapping:
            if key in triple:
                return True
        return False

    def __iter__(self):
        for uuid in self.mapping:
            yield uuid[0]
    
    def __getitem__(self, key):
        for uuid in self.mapping:
            for item in uuid:
                if item is key or (isinstance(key, UUID) and item == key):
                    return uuid
        if type(key) is UUID:
            raise KeyError, key
        else:
            self[uuid4()] = (key,)
            return self[key]

    def __setitem__(self, key, value):
        try:
            oper = self[key]
        except KeyError:
            self.mapping.append([None, None, None])
            oper = self.mapping[-1]

        for elem in [key] + list(value):
            if isinstance(elem, UUID):
                oper[0] = elem
            elif type(elem) is dict:
                oper[1] = elem
            else:
                oper[2] = elem

        if oper[0] is None: oper[0] == uuid4()

    def __delitem__(self, key):
        item = self[key]
        del self.mapping[self.mapping.index(item)]

    def getUUID(self, key):
        return self[key][0]

    def getObj(self, key):
        return self[key][2]

    def getDict(self, key):
        return self[key][1]

    def iterDicts(self):
        for guid in self:
            yield self.getDict(guid)

    def mapUUIDsToDicts(self):
        out = {}
        for guid in self:
            out[unicode(guid)] = self.getDict(guid)
        return out

class Encoder:
    def __init__(self):
        self.uuids = UUIDMapping()
        self.toplevel = []
    
    def encode(self, obj):
        self.uuids[obj] = (self.objToDict(obj),)
        self.toplevel.append(unicode(self.uuids.getUUID(obj)))

        while None in self.uuids.iterDicts():
            for (guid, dic, sub_obj) in self.uuids.mapping:
                if dic is None:
                    self.uuids[guid] = (self.objToDict(sub_obj), sub_obj)
        
        self.tagDaughters()

        return self.encodedState()

    def tagDaughters(self, tl_uuid=None):
        assert not None in self.uuids.iterDicts()
        if tl_uuid is None:
            for tl_uuid in self.toplevel:
                tl_uuid = UUID(tl_uuid)
                self.tagDaughters(tl_uuid)
        else:
            dic = self.uuids.getDict(tl_uuid)
            if dic['type'] != 'Segment':
                for daughter_uuid in dic['content']:
                    daughter_uuid = UUID(daughter_uuid)
                    daughter = self.uuids.getDict(daughter_uuid)
                    daughter['mother'] = unicode(tl_uuid)
                    self.tagDaughters(daughter_uuid)

    def encodedState(self):
        out = {'content': self.toplevel,
               'objects': self.uuids.mapUUIDsToDicts()}
        return out
        
    def objToDict(self, obj):
        '''Returns a dictionary that preserves all the properties of the
        input obj.'''
        out = {}
        out['uuid'] = unicode(self.uuids.getUUID(obj))
        out['type'] = obj.__class__.__name__
        out.update(obj.__dict__)

        # We have to replace the ons, nuc and cod properties of Syllable
        # objects with lists of segment UUIDs.
        if isinstance(obj, Syllable):
            for node in ('ons', 'nuc', 'cod'):
                out[node] = []
                for seg in getattr(obj, node, ()):
                    out[node].append(unicode(self.uuids.getUUID(seg)))

        if isinstance(obj, Segment):
            out['content'] = list(obj)
        else:
            out['content'] = []
            for sub_obj in obj:
                out['content'].append(unicode(self.uuids.getUUID(sub_obj)))
        return out

def trimOntology(dic):
    to_trim = dic['objects'].keys()

    for uuid in span_tree(dic):
        if uuid in to_trim:
            to_trim.remove(uuid)
        
    for uuid in to_trim:
        del dic['objects'][uuid]
    return dic

def span_tree(dic, root=None):
    if root is None:
        root = dic

    for uuid in root['content']:
        yield uuid
    for uuid in root['content']:
        if uuid in dic['objects']:
            daughter = dic['objects'][uuid]
            if daughter['type'] != 'Segment':
                for sub_uuid in span_tree(dic, daughter):
                    yield sub_uuid

def getUUID(obj, uuid_mapping):
    for uuid in uuid_mapping:
        if uuid_mapping[uuid] is obj:
            return uuid
    raise KeyError, '%s not in uuid_mapping' % repr(obj)

def serializableObject(*args):
    codec = Encoder()
    for obj in args:
        codec.encode(obj)
    return codec.encodedState()

def deserializeObjects(dic):
    out = []
    objects = dic['objects']
    uuids = {}
    for uuid in dic['content']:
        func = deserializers[objects[uuid]['type']]
        out.append(func(objects[uuid], objects, uuids))
    return out

def addToObjects(obj, objects, uuids={}):
    try:
        uuid = getUUID(obj, uuids)
    except KeyError:
        dic = objToDict(obj, uuids)
        objects[dic['uuid']] = dic
    else:
        assert uuid in objects
    return (objects, uuids)

def gatherObjects(obj, uuids={}):
    out = {}

    for level in levels:
        for sub_obj in flatten(obj, level):
            if isinstance(sub_obj, Reduplicant):               # flatten() won't give you
                out.update(gatherObjects(sub_obj, uuids)[0])   # objects inside Morphemes
            elif isinstance(sub_obj, Morpheme):                # or Reduplicants, so we
                for unit in sub_obj:                           # recurse into them
                    out.update(gatherObjects(unit, uuids)[0])
                (out, uuids) = addToObjects(sub_obj, out, uuids)
            else:
                (out, uuids) = addToObjects(sub_obj, out, uuids)

    (out, uuids) = addToObjects(obj, out, uuids)
    return (out, uuids)

def dictToObj(dic, objects, uuids={}):
    out = eval('%s()' % dic['type'])
    if 'lang' in dic:
        out.lang = dic['lang']
    if 'indices' in dic:
        out.indices = dic['indices'].copy()
    if 'head' in dic:
        out.head = dic['head']
    out.extend(uuidsToObjects(dic['content'], objects, uuids))
    uuids[dic['uuid']] = out
    return out

def dictToUtterance(dic, objects, uuids={}):
    out = dictToObj(dic, objects, uuids)
    out.lang = dic['lang']
    out.lex = dic['lex']
    out.topIndex = dic['topIndex']
    return out

def dictToSyllable(dic, objects, uuids={}, check=True):
    out = dictToObj(dic, objects, uuids)
    out.lex = dic['lex']

    for key in ('ons', 'nuc', 'cod'):
        node = getattr(out, key)
        node.extend(uuidsToObjects(dic[key], objects, uuids))
    if check:
        assert dic['MoraCount'] == out.getMoraCount()

    return out

def dictToSegment(dic, uuids={}, check=True):
    out = Segment(dic['content'], lang=dic['lang'], lex=dic['lex'])
    out.indices = dic['indices'].copy()
    if check:
        assert 'ipa' not in dic or dic['ipa'] == out.set_ipa()
        assert 'tipa' not in dic or dic['tipa'] == out.set_tipa()
        assert 'xsampa' not in dic or dic['xsampa'] == out.set_xsampa()
    uuids[dic['uuid']] = out
    return out

def uuidsToObjects(seq, objects, uuids={}):
    '''Given a sequence of UUIDs seq, a mapping of uuids to serialized
    objects objects, and a mapping of uuids to deserialized objects
    uuids, generates the corresponding sequence of deserialized
    objects.'''
    for uuid in seq:
        if uuid in uuids:
            yield uuids[uuid]
        else:
            sub_dic = objects[uuid]
            func = deserializers.get(sub_dic['type'], dictToObj)
            sub_obj = func(sub_dic, objects, uuids)
            uuids[uuid] = sub_obj
            yield sub_obj
        
def objToDict(obj, uuid_mapping):
    '''Returns a dictionary that preserves all the properties of the
    input obj.  Where other objects appear in the properties of obj,
    they are referred to by the UUIDs given in uuid_mapping.
    uuid_mapping must map UUIDs onto objects, and include, at a
    minimum, every object referred to anywhere in the properties of
    obj.'''
    out = {}
    out['uuid'] = uuid4()
    uuid_mapping[out['uuid']] = obj
    out['type'] = obj.__class__.__name__
    out.update(obj.__dict__)

    # We have to replace the ons, nuc and cod properties of Syllable
    # objects with lists of segment UUIDs.
    if isinstance(obj, Syllable):
        for node in ('ons', 'nuc', 'cod'):
            out[node] = []
            for seg in getattr(obj, node, ()):
                out[node].append(getUUID(seg, uuid_mapping))

    if isinstance(obj, Segment):
        out['content'] = list(obj)
    elif not isinstance(obj, OT.Grammar):
        out['content'] = []
        for sub_obj in obj:
            out['content'].append(getUUID(sub_obj, uuid_mapping))
    return out

deserializers = {
    'Utterance': dictToUtterance,
    'Syllable': dictToSyllable,
    'Segment': dictToSegment
}

