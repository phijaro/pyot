from OT.representations import *

sortedFeatures= ('cons',
                 'voc',
                 'obs',
                 'son',
                 'cor',
                 'lab',
                 'dor',
                 'rad',
                 'plos',
                 'cont',
                 'high',
                 'low',
                 'voi',
                 'sprgl',
                 'atr',
                 'rtr',
                 'nas',
                 'strid',
                 'rho',
                 'lat')

class TreeGridder():
    def __init__(self, rep, **kwargs):
        self.rows = [[], [], []]
        self.rowDict = {Segment: self.rows[-3],
                        'feat': self.rows[-2],
                        'sonority': self.rows[-1]}
        self.virtualSegCoords = []
        self.coords = []
        self.lines = []
        self.sonority = kwargs.get('sonority', False)
        self.indices = kwargs.get('indices', False)

        for level in levels:
            if flattenable(rep, level):
                self.draw(flatten(rep, level), level)

    def draw(self, flatRep, level):
        if level is Segment:
            self.drawSegments(flatRep)
        elif level is Syllable:
            self.drawSyllables(flatRep)
        elif level is Foot:
            self.drawFeet(flatRep)
        else:
            self.drawHighLevel(flatRep)

    def lineCommand(self, fro, to):
        self.lines.append((fro, to))

    def identity(self, tup1, tup2):
        if len(tup1) != len(tup2):
            return False
        for idx in range(len(tup1)):
            if tup1[idx] is not tup2[idx]:
                return False
        return True

    def getVCoord(self, *args):
        for coord in self.virtualSegCoords:
            if self.identity(coord[:-1], args):
                return coord[-1]
        raise KeyError, args

    def getCoord(self, *args):
        for coord in self.coords:
            if self.identity(coord[:-1], args):
                return coord[-1]
        raise KeyError, args

    def getCoordEnough(self, *args):
        for coord in self.coords:
            if coord[:-1] == args:
                return coord[-1]
        raise KeyError, args

    def markNode(self, unit, marking=None):
        pass

    def drawHighLevel(self, flatRep):
        self.rows.insert(0, [])
        self.rowDict[type(flatRep[0])] = self.rows[0]

        for unit in flatRep:
            self.locateNode(unit)
            self.rowDict[type(unit)].extend((None,) * (len(flatten(unit)) * 2))
            self.rowDict[type(unit)][self.getCoord(unit)] = unit
            for down in unit:
                self.lineCommand(unit, down)
                if len(unit) > 1 and hasattr(unit, 'head'):                    
                    if unit[unit.head] is down:
                        self.markNode(down)
                    else:
                        self.markNode(down, '_w')

    def locateNode(self, obj):
        namemapping = {'O': ('ons',),
                       'N': ('nuc',),
                       'C': ('cod',)}
        if (type(obj) is tuple and
            obj[0] in ('O', 'N', 'C') and
            isinstance(obj[1], Syllable)):
            chunk = []
            for name in namemapping[obj[0]]:
                chunk.extend(getattr(obj[1], name))
            start = self.getVCoord(chunk[0])
            stop = self.getVCoord(chunk[-1])
            out = obj
        else:
            start = self.getCoord(obj[0])
            stop = self.getCoord(obj[-1])
            out = (obj,)
        self.coords.append(out + ((start + (stop - start) / 2),))

    def drawFeet(self, flatRep):
        syllrow = self.rows.index(self.rowDict[Syllable])
        self.rows.insert(syllrow, [])
        self.rowDict[Foot] = self.rows[syllrow]

        for foot in flatRep:
            self.rowDict[Foot].extend((None,) * (len(flatten(foot)) * 2))
            if type(foot) is Foot:
                self.locateNode(foot)
                self.rowDict[Foot][self.getCoord(foot)] = foot
                for unit in foot:
                    self.lineCommand(foot, unit)

    def drawSyllables(self, flatRep):
        segrow = self.rows.index(self.rowDict[Segment])
        self.rows.insert(segrow, [])
        self.rowDict['mora'] = self.rows[segrow]
        self.rows.insert(segrow, [])
        self.rowDict['O'] = self.rows[segrow]
        self.rowDict['N'] = self.rows[segrow]
        self.rowDict['C'] = self.rows[segrow]
        self.rows.insert(segrow, [])
        self.rowDict['R'] = self.rows[segrow]
        self.rows.insert(segrow, [])
        self.rowDict[Syllable] = self.rows[segrow]
        newrows = ('mora', 'O', 'R', Syllable)

        for syll in flatRep:
            if isinstance(syll, Syllable):
                self.locateNode(('N', syll))
                if syll.ons:
                    self.locateNode(('O', syll))
                if syll.cod:
                    self.locateNode(('C', syll))

                start = self.getCoord('N', syll)
                if syll.cod:
                    stop = self.getCoord('C', syll)
                else:
                    stop = start
                self.coords.append(('R', syll, start + ((stop - start) / 2)))           

                if syll.edgemostSegs('L'):
                    start = self.getCoord(syll.edgemostSegs('L')[0])
                elif syll.ons:
                    start = self.getCoord('O', syll)
                else:
                    start = self.getCoord('R', syll)
                if syll.edgemostSegs('R'):
                    start = self.getCoord(syll.edgemostSegs('R')[-1])
                else:
                    stop = self.getCoord('R', syll)

                self.coords.append((syll, start + ((stop - start) / 2)))

                for seg in syll:
                    for row in newrows:
                        self.rowDict[row].extend((None, None))

                self.rowDict[Syllable][self.getCoord(syll)] = syll
                if syll.ons:
                    self.rowDict['O'][self.getCoord('O', syll)] = ('O', syll)
                    self.lineCommand(syll, ('O', syll))
                self.rowDict['R'][self.getCoord('R', syll)] = ('R', syll)
                self.lineCommand(syll, ('R', syll))
                self.rowDict['N'][self.getCoord('N', syll)] = ('N', syll)
                self.lineCommand(('R', syll,), ('N', syll))
                if syll.cod:
                    self.rowDict['C'][self.getCoord('C', syll)] = ('C', syll)
                    self.lineCommand(('R', syll,), ('C', syll))

                for seg in syll.ons:
                    self.lineCommand(('O', syll), seg)
                for seg in syll.nuc:
                    moracol = self.getVCoord(seg)
                    self.rowDict['mora'][moracol] = ('mora', seg)
                    self.coords.append(('mora', seg, moracol))
                    self.lineCommand(('mora', seg), seg)
                    self.lineCommand(('N', syll), ('mora', seg))
                for seg in syll.cod:
                    moracol = self.getVCoord(seg)
                    self.rowDict['mora'][moracol] = ('mora', seg)
                    self.coords.append(('mora', seg, moracol))
                    self.lineCommand(('mora', seg), seg)
                    self.lineCommand(('C', syll), ('mora', seg))
                for seg in syll.edgemostSegs('R'):
                    self.lineCommand(syll, seg)
                for seg in syll.edgemostSegs('L'):
                    self.lineCommand(syll, seg)
                    

    def drawSegment(self, seg, wid):
        lm = ''
        if wid > 2:
            lm += ':'

        index = ''
        if self.indices:
            index += '$_{%d}$' % seg.indices['IO']
            
        drawcoord = (len(self.rowDict[Segment]) - (wid / 2))
        self.rowDict[Segment][drawcoord] = seg
        self.coords.append((seg, drawcoord))
        if wid > 2:
            self.virtualSegCoords.append((seg, drawcoord + (wid / 2)))
        else:
            self.virtualSegCoords.append((seg, drawcoord))
            
        return drawcoord

    def drawSegments(self, flatRep):
        wid = 0
        last = flatRep[0]
        notdrawn = []

        for seg in flatRep[1:]:
            wid += 1
            self.rowDict[Segment].extend((None,))
            self.rowDict['feat'].extend((None,))
            self.rowDict['sonority'].extend((None,))
            if last != seg:
                wid += 1
                self.rowDict[Segment].extend((None,))
                self.rowDict['feat'].extend((None,))
                self.rowDict['sonority'].extend((None,))
                drawcoord = self.drawSegment(last, wid)
                start = (drawcoord - (wid / 2))
                wid = 0
                for nd_seg in notdrawn:
                    self.coords.append((nd_seg, drawcoord))
                    self.virtualSegCoords.append((nd_seg, start))
                    start += 2
                notdrawn = []
            else:
                notdrawn.append(last)
            last = seg
        self.rowDict[Segment].extend((None, None))
        self.rowDict['feat'].extend((None, None))
        self.rowDict['sonority'].extend((None, None))
        drawcoord = self.drawSegment(seg, wid + 2)
        start = (drawcoord - (wid / 2))
        for nd_seg in notdrawn:
            self.coords.append((nd_seg, drawcoord))
            self.virtualSegCoords.append((nd_seg, start))
            start += 2

        if self.sonority:
            self.rowDict['sonority'][0] = '\K{Sonority:}'
            ff = max([len(seg) for seg in flatRep])
            ff = (((-3 - self.scale_factor) * ff) + 13) 
            for idx in range(len(self.rowDict['sonority'])):
                if r'\K' in self.rowDict['sonority'][idx]:
                    self.rowDict['sonority'][idx] = self.rowDict['sonority'][idx].replace(r'\K', '\\Kk{%d}' % ff)

    def featArray(self, seg):
        out = r' \Kk{%d}{$ \left[ \begin{array}{l}' % ((-2 * len(seg)) +
                                                       (9 - self.scale_factor))
        for feat in sortedFeatures:
            if feat in seg:
                out += '\\mbox{\\textsc{%s}}\\\\' % feat
        out = out.rstrip('\\\\')
        out += r'\end{array} \right]$}'
        return out

    def lines(self):
        yield '\\Tree{\n'
        for row in self.rows:
            yield ' & '.join(row) + ' \\\\\n'
        yield '}\n'
