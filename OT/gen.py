from representations import *
from types import FunctionType
import constraints, os, config, pprint, ui_util, sys

def ccTETU(ranking):
    '''Tests whether the constraints in ranking call
    for CC-correspondence-conditioned emergence of the unmarked:
    i.e. do all IO-faithfulness constraints dominate all
    CC-correspondence-based constraints.'''
    if not constraints.CC_Corr in [c[0] for c in ranking]:
        return False
    
    cc_top = False
    for constr in ranking:
        if constr[0] in (constraints.notIdentCC,
                         constraints.IdentCC,
                         constraints.CC_Corr):
            cc_top = True
        if (constr[0] in (constraints.Max,
                          constraints.Dep,
                          constraints.Ident)
            and constraints.mode(constr) == 'IO'
            and cc_top):
            return False
    return True

def distilRanking(ranking, mode=None):
    out = set()
    for constr in ranking:
        if constr[0] in (constraints.Ident,
                         constraints.Max_C,
                         constraints.Max_V,
                         constraints.Dep_C,
                         constraints.Dep_V,
                         constraints.DepMora,
                         constraints.CC_Corr,
                         constraints.DepFeat,
                         constraints.MaxFeat):
            if mode is None:
                out.add(constr)
            elif constraints.mode(constr) == mode:
                out.add(constr)
    return out

def invIdent(segs, ranking, **kwargs):
    csegs = kwargs.get('csegs', invIdentScript(segs, ranking, **kwargs))
    sofar = kwargs.get('sofar', [Utterance(lang=kwargs.get('lang', 'fallback'))])
    baton = kwargs.get('baton', None)

    if len(sofar[0]) == len(csegs):
        return sofar

    for seq in sofar[:]:
        sofar.remove(seq)
        for seg in csegs[len(seq)]:
            sofar.append(seq.copy())
            sofar[-1].append(seg)
            if baton: baton.twirl()

    kwargs['csegs'] = csegs
    kwargs['sofar'] = sofar

    return invIdent(segs, ranking, **kwargs)

def invIdentScript(segs, ranking, **kwargs):
    ipas = config.get_ipas(kwargs.get('lang', 'fallback'), 'ipa', lex=False)
    baton = kwargs.get('baton', None)
    feats = []
    csegs = []
    for constr in ranking:
        if (constr[0] is constraints.Ident
            and constraints.mode(constr) == 'IO'):
            feats.append(constr[1])

    for seg in segs:
        csegs.append([])
        for torfs in TorF(len(feats)):
            cand = seg.copy()
            for idx in range(len(torfs)):
                if (feats[idx] in cand) != torfs[idx]:
                    cand.toggleFeature(feats[idx])
                if baton: baton.twirl()
            if cand.set_ipa() in ipas:
                csegs[-1].append(cand)
        if not feats:
            csegs[-1].append(seg)

    return csegs

def generateFromScript(script, **kwargs):
    sofar = kwargs.get('sofar', Utterance())
    baton = kwargs.get('baton', None)
    idx = kwargs.get('idx', 0)
    if getattr(sofar, 'lang', None) is None:
        for slot in script:
            for seg in slot:
                sofar.set_language(getattr(seg, 'lang', None))
                break
        sofar.set_language = 'fallback'

    if idx == len(script):
        if baton: baton.twirl()
        yield sofar
    else:
        for seg in script[idx]:
            out = sofar.copy()
            out.extend(seg)
            for cand in generateFromScript(script, sofar=out, idx=idx+1):
                yield cand

def prettyPrintScript(script):
    out = '\n'
    scheme = config.select_term_scheme()
    zero = Utterance().prettyPrint(scheme)

    max_cell_length = len(zero)
    for slot in script:
        for segs in slot:
            cell_length = ui_util.ulen(''.join([seg.prettyPrint(scheme) for seg in segs]))
            if cell_length > max_cell_length:
                max_cell_length = cell_length

    max_slot_length = max([len(slot) for slot in script])
    if len(str(max_slot_length)) > max_cell_length:
        max_cell_length = len(str(max_slot_length))

    for row in range(max_slot_length):
        for slot in script:
            if len(slot) > row:
                if slot[row] == []:
                    cell = zero
                else:
                    cell = ''.join([seg.prettyPrint(scheme) for seg in slot[row]])
            else:
                cell = ''
            padding = ' ' * (max_cell_length - ui_util.ulen(cell) + 1)
            out += cell
            out += padding
        out += '\n'
    for slot in script:
        cell = str(len(slot))
        padding = ' ' * (max_cell_length - ui_util.ulen(cell))
        out += cell
        out += padding
        if scheme == 'ipa':
            out += u'\u2715'
        else:
            out += 'x'
    out = out[:-1]
    out += ' = %d' % countSegVars(script)

    return out

def countSegVars(script):
    out = 1
    for slot in script:
        out = out * len(slot)
    return out

def alreadyInSlot(cand, slot, **kwargs):
    if cand == [] and [] in slot:
        return True
    for old in slot:
        thiscand = True
        if len(old) == len(cand):
            for seg in old:
                if not seg in cand:
                    thiscand = False
        if thiscand: return True

def ccMarkSeg(seg, ranking):
    for constr in ranking:
        if constr[0] == constraints.CC_Corr and seg.issuperset(constr[1:]):
            return True
    return False

def segVarScript(segs, ranking, **kwargs):
    lang = kwargs.get('lang', getattr(segs, 'lang', None))
    if lang is None:
        lang = getattr(segs[0], 'lang', 'fallback')
    ipas = config.get_ipas(lang, 'ipa', lex=False)
    baton = kwargs.get('baton', None)
    filt = kwargs.get('filter', ())
    max_feats = set([])
    dep_feats = set([])
    max_seg_feats = []
    dep_seg_feats = []
    dep_mora = False
    cc_coindex = kwargs.get('cc_coindex', False)

    for constr in ranking:
        if constr == (constraints.Max_C,):
            max_seg_feats.append('cons')
        elif constr == (constraints.Max_V,):
            max_seg_feats.append('voc')
        elif constr[0] is constraints.Dep:
            dep_seg_feats.append(constr[1])
        elif constr[0] is constraints.MaxFeat:
            max_feats.add(constr[1])
        elif constr[0] is constraints.DepFeat:
            dep_feats.add(constr[1])
        elif constr[0] is constraints.Ident:
            max_feats.add(constr[1])
            dep_feats.add(constr[1])
        elif constr[0] is constraints.DepMora:
            dep_mora = True

    csegs = []
    last = set([])
    if baton:
        baton.end = None

    for seg in segs:
        if type(seg).__name__ == 'Morpheme':
            csegs.append([])
            for var in seg:
                csegs[-1].append(var.copy())
        elif type(seg).__name__ != 'Segment':
            csegs.append([[seg]])
        else:
            for feat in dep_seg_feats:
                csegs.append([[]])
                for key in ipas:
                    if feat in ipas[key]:
                        csegs[-1].append([Segment(ipa, lang=lang, lex=False)])
#                    if baton: baton.twirl()

            csegs.append([])
            for feats in ipas.values():
                feats = set(feats)
                if (((seg - feats) <= max_feats)
                    and ((feats - seg) <= dep_feats)):
                    cand = seg.copy()
                    cand.lex = False
                    cand.lang = lang
                    cand.clear()
                    cand.update(feats)
                    cand.set_ipa()
                    if not alreadyInSlot([cand], csegs[-1], **kwargs):
                        csegs[-1].append([cand])
                    if baton: baton.twirl()
            if not csegs[-1]:
                csegs[-1].append([Segment(seg.set_ipa(), lang=lang, lex=False)])
                csegs[-1][-1][0].indices = seg.indices.copy()
                            
            for feat in max_seg_feats:
                if feat in seg and not [] in csegs[-1]:
                    csegs[-1].append([])
#                if baton: baton.twirl()
        last = seg

    if baton: baton.end = None
    for feat in dep_seg_feats:
        csegs.append([[]])
        for key in ipas:
            if feat in ipas[key]:
                csegs[-1].append([Segment(ipa, lang=lang, lex=False)])
            if baton: baton.twirl()
    if cc_coindex:
        for slot in csegs:
            for segs in slot[:]:
                addend = []
                for seg in segs:
                    if ccMarkSeg(seg, ranking):
                        addend.append(seg.copy())
                        addend[-1].indices['CC'] = True
                if addend:
                    slot.append(addend)
    if dep_mora:
        for slot in csegs:
            for segs in slot[:]:
                if len(segs) == 1 and type(segs[0]) is Segment and 'voc' in segs[0]:
                    slot.append([segs[0].copy(), segs[0].copy()])
    return csegs

def combineScripts(*args):
    out = []
    for script in args:
        if len(script) > len(out):
            out = script

    for script in args:
        for idx in range(len(script)):
            for seg in script[idx]:
                add = True
                if not seg in out[idx]:
                    out[idx].append(seg)
        for idx in range(len(script), len(out)):
            if not [] in out[idx]:
                out[idx].append([])

    return out

def Ident(segs, feat, **kwargs):
    '''Given a sequence of segments segs and a feature feat, returns a
    sequence of variations on segs exhausting all the possibilities
    with respect to specification for feat.  In other words, generates
    violations of Ident-[feat].'''
    baton = kwargs.get('baton', None)
    out = []

    for torfs in TorF(len(segs)):
        out.append(Utterance(lang=kwargs.get('lang', 'fallback')))
        for idx in range(len(torfs)):
            out[-1].append(segs[idx].copy())
            if torfs[idx]:
                out[-1][-1].add(feat)
                for pair in binary:
                    if feat == pair[0]:
                        out[-1][-1].discard(pair[1])
                    elif feat == pair[1]:
                        out[-1][-1].discard(pair[0])
            else:
                out[-1][-1].discard(feat)
                for pair in binary:
                    if feat == pair[0]:
                        out[-1][-1].add(pair[1])
                    elif feat == pair[1]:
                        out[-1][-1].add(pair[0])
            out[-1][-1].indices['IO'] = segs[idx].indices.get('IO', None)
            out[-1][-1].set_ipa()
            out[-1][-1].set_tipa()
            if not possibleSegment(out[-1][-1]):
                out = out[:-1]
                break
        if baton:
            baton.twirl()

    return out

def yieldIdent(segs, feat, **kwargs):
    '''Given a sequence of segments segs and a feature feat, returns a
    sequence of variations on segs exhausting all the possibilities
    with respect to specification for feat.  In other words, generates
    violations of Ident-[feat].'''
    baton = kwargs.get('baton', None)
    out = []
    torfses = kwargs.get('torfses', TorF(len(segs)))

    for torfs in torfses:
        y = True
        out = Utterance()
        for idx in range(len(torfs)):
            out.append(segs[idx].copy())
            if torfs[idx]:
                out[-1].add(feat)
                for pair in binary:
                    if feat == pair[0]:
                        out[-1].discard(pair[1])
                    elif feat == pair[1]:
                        out[-1].discard(pair[0])
            else:
                out[-1].discard(feat)
                for pair in binary:
                    if feat == pair[0]:
                        out[-1].add(pair[1])
                    elif feat == pair[1]:
                        out[-1].add(pair[0])
            out[-1].set_ipa()
            out[-1].set_tipa()
            if not possibleSegment(out[-1]):
                y = False
                break
        if y: yield out
        if baton:
            baton.twirl()

def Max(segs, feat=None, **kwargs):
    '''Given a sequence of segments segs, returns a list of candidates
    representing all possible combinations of deletion
    vs. non-deletion of the segments in segs.  If the optional feature
    feat is specified, segments not having the feature feat are not
    deleted from any candidate in the output list.  In otherwords,
    generates violations of Max-seg.'''
    baton = kwargs.get('baton', None)
    out = []
    torfses = kwargs.get('torfses', TorF(len(segs)))

    for torfs in torfses:
        out.append(Utterance())
        for idx in range(len(torfs)):
            if feat in segs[idx] or feat is None:
                if torfs[idx]:
                    out[-1].append(segs[idx].copy())
            else:
                out[-1].append(segs[idx].copy())
        if out[-1] in out[:-1]:
            out = out[:-1]
        if baton:
            baton.twirl()

    return out

def yieldMax(segs, feat=None, **kwargs):
    '''Given a sequence of segments segs, returns a list of candidates
    representing all possible combinations of deletion
    vs. non-deletion of the segments in segs.  If the optional feature
    feat is specified, segments not having the feature feat are not
    deleted from any candidate in the output list.  In other words,
    generates violations of Max-seg.'''
    baton = kwargs.get('baton', None)
    out = []
    torfses = kwargs.get('torfses', TorF(len(segs)))

    for torfs in torfses:
        out = Utterance()
        for idx in range(len(torfs)):
            if feat in segs[idx] or feat is None:
                if torfs[idx]:
                    out.append(segs[idx].copy())
            else:
                out.append(segs[idx].copy())
        if baton:
            baton.twirl()

        yield out

def Dep(segs, feat=None, **kwargs):
    ipas = config.get_ipas(kwargs.get('lang', 'fallback'), 'ipa', lex=False)
    baton = kwargs.get('baton', None)
    positions = range(len(segs) + 1)
    addends = []
    out = []
    for seg in ipas:
        if feat in ipas[seg]:
            addends.append(seg)
        elif feat is None: 
            addends.append(seg)          
        if baton: baton.twirl()

    for pos in positions:
        for addend in addends:
            out.append(segs.copy())
            out[-1].insert(pos, Segment(addend))

    return out

def DepMora(segs, **kwargs):
    baton = kwargs.get('baton', None)
    feat = kwargs.get('feat', 'voc')
    out = []

    for torf in TorF(len(segs)):
        addend = Utterance()
        for idx in range(len(segs)):
            addend.append(segs[idx].copy())
            if (idx < len(segs) - 1 and 
                segs[idx + 1] != segs[idx] and
                (feat in segs[idx] or not feat) and
                torf[idx]):
                    addend.append(segs[idx].copy())
        if not addend in out:
            out.append(addend)
        if baton: baton.twirl()

    return out

def CCableIndices(utt, ranking):
    for seg in flatten(utt):
        if type(seg) is Segment:
            if ranking is None:
                yield seg
            elif ccMarkSeg(seg, ranking):
                yield seg

def CCVariantByKey(utt, ranking=None, key=0, **kwargs):
    '''Returns the candidate that would be output nth (counting from
    0) by Max_CC(utt).'''
    indices = kwargs.get('indices', CCableIndices(utt, ranking))
    ops = 0
    for op in CCableIndices(utt, ranking):
        ops += 1
    ops = 2 ** ops

    while key < ops:
        out = utt.copy()

        torf = intToTorF(key, ops)
        if torf.count(True) > 1:
            marks = operations('cc_mark', CCableIndices(out, ranking), torf)
            for mark in marks:
                mark[1].indices['CC'] = mark[2]

            newkey = yield (out, key)
        else:
            newkey = None

        if newkey is None:
            key += 1
        else:
            key = newkey

def Max_CC(utt, ranking=None, **kwargs):
    for (cand, key) in CCVariantByKey(utt, ranking):
        yield cand

def compressSegments(cand):
    out = {'lang': cand.lang,
           'segments': [],
           'IO': [],
           'BR': [],
           'CC': []}
    for seg in cand:
        out['segments'].append(seg.set_ipa())
        for key in out:
            if not key in ('segments', 'lang'):
                out[key].append(seg.indices.get(key, None))
    lengths = set([])
    for key in out:
        if key != 'lang':
            lengths.add(len(out[key]))
    assert len(lengths) == 1
    return out

def inflateSegments(sparse):
    out = Utterance(lang=sparse['lang'])
    index_keys = sparse.keys()
    index_keys.remove('segments')

    for idx in range(len(sparse['segments'])):
        out.append(Segment(sparse['segments'][idx], lang=sparse['lang']))
        for key in index_keys:
            out[-1].indices[key] = sparse[key][idx]
    return out

def applyFilter(ur, cand, filt, **kwargs):
    '''Returns True iff none of the constraints in the sequence filt
    are violated by the input/output pair of ur and cand.'''
    scheme = kwargs.get('scheme', 'ipa')
    for constr in filt:
        if constr[0](ur, cand, *constr[1:]):
            if kwargs.get('verbose', False): 
                print >> sys.stderr, ur.prettyPrint(scheme), cand.prettyPrint(scheme), constr[0].__name__
            return False
    return cand

def genSegs(ur, ranking, **kwargs):
    baton = kwargs.get('baton', None)
    dopts = kwargs.get('dopts', [])
    filt = kwargs.get('filter', [])
#    hashes = kwargs.get('hashes', [])
    kwargs['hashes'] = []
    kwargs['torfses'] = TorF(len(ur) + 1)
    if baton: baton.twirl()

    ranking = list(ranking)
    done = True
    constr = None

    for idx in range(len(ranking) - 1, -1, -1):
        if ranking[idx][0] in (constraints.Ident,
                               constraints.Max_V,
                               constraints.Max_C):
            constr = ranking.pop(idx)
            break
    
    if constr is not None:
        for cand in violGenerator(ur, constr, **kwargs):
            if baton: baton.twirl()
            for segs in genSegs(cand, ranking, **kwargs):
                done = False
#                if not segs in hashes:
                yield segs
#                    hashes.append(hashSegments(ur, segs))
            if done and applyFilter(ur, cand, filt):
                yield cand                    
    
def GenDepthFirst(ur, ranking, **kwargs):
    '''Given an underlying representation ur, and a ranking, generates
    candidates based on the faithfulness constraints in the ranking,
    specified up to the unit passed as the optional keyword level.  If
    level is not specified, currently the maximal projection is the
    foot.'''
    level = kwargs.get('level', Foot)
    baton = kwargs.get('baton', None)
    dopts = kwargs.get('dopts', [])
    filt = kwargs.get('filter', [])

    ur = flatten(ur, Segment)
    segses = []
    
    for segs in genSegs(ur, ranking, **kwargs):
        if not segs in segses:
            segses.append(segs)
            if level is Segment:
                yield segs
            else:
                for sylls in syllabify(segs):
                    if level is Syllable:
                        if applyFilter(ur, sylls, filt):
                            yield sylls
                    else:
                        for feets in footGeneral(sylls):
                            if level is Foot:
                                if applyFilter(ur, sylls, filt):
                                    yield feets

def violGenerator(ur, constr, **kwargs):
    '''Returns the generator output by the function in the gen module
    that generates violations of the constraint constr.'''
    if constr[0] is constraints.Ident:
        return yieldIdent(ur, constr[1], **kwargs)
    if constr[0] is constraints.Max_V:
        return yieldMax(ur, 'voc', **kwargs)
    if constr[0] is constraints.Max_C:
        return yieldMax(ur, 'cons', **kwargs)

def composeMenu(level, ranking):
    menu = []

    if level != Segment:
        for l in levels[1:]:
            if l == Syllable:
                menu.append((syllabificationByKey,))
            elif l == Foot:
                menu.append((listKeyWrapper, (footGeneral,)))
            else:
                menu.append((listKeyWrapper, (hiLevelParse, l)))
            if l == level:
                break
    if constraints.CC_Corr in [c[0] for c in ranking]:
        menu.append((CCVariantByKey, ranking))

    return menu

def menuFunctionMessage(builder, buildon):
    foldin = ['', buildon.prettyPrint(config.select_term_scheme()), '']
    if builder[0] == listKeyWrapper:
        builder = builder[1]
    if builder[0] in (Max_CC, CCVariantByKey):
        foldin[0] = 'CC-indexing'
    elif builder[0] == hiLevelParse:
        foldin[0] = 'Parsing'
        foldin[2] = 'into %ss' % builder[1].__name__
    elif builder[0] in (syllabify, syllabificationByKey):
        foldin[0] = 'Syllabifying'
    elif builder[0] == footGeneral:
        foldin[0] = 'Footing'
    return ' '.join(foldin)

def Gen(ur, ranking, scheme='ipa', filter=(), menu=None, level=ProsodicPhrase,
        baton=None):
    '''Given an underlying representation ur, and a ranking, generates
    candidates based on the faithfulness constraints in the ranking,
    specified up to the unit passed as the optional keyword level.  If
    level is not specified, the maximal projection is the prosodic
    phrase.'''
    out = [[]]

    segs = flatten(ur)
    io_ranking = list(distilRanking(ranking, 'IO'))
#    cc_coindex = (constraints.CC_Corr in [c[0] for c in ranking])

    if baton:
        baton.message('Assembling segment script')
    script = segVarScript(segs, io_ranking, baton=baton)
    if baton:
        baton.message(prettyPrintScript(script))
        baton.message('Generating candidates')
    seg_menu = segBuildMenu(ur, ranking)
    if menu is None:
        menu = composeMenu(level, ranking)
    for func in menu:
        out.append([])

    segvar_iter = menuRecurse(ur, segVariantByKey(script), 
                              seg_menu, filt=filter)
    if baton:
        baton.end = float(countSegVars(script))
        baton.percent = True

    for (cand, key) in menuRecurse(ur, segvar_iter, menu, 
                                   filt=filter, baton=baton, gen_all=True):
        seq = out[len(key) - 1]
        entry = {'key': key,
                    'tc': cand.prettyPrint(scheme)}
        entry['vio'] = []
        for constr in ranking:
            entry['vio'].append(constr[0](ur, cand, *constr[1:]))
        added = False
        for idx in range(len(seq)):
            if seq[idx]['vio'] > entry['vio']:
                seq.insert(idx, entry)
                added = True
                break
        if not added:
            seq.append(entry)

    return (out, menu, seg_menu, script)

    # if kwargs.get('parallel_gen', False):
    #     import gen_mp
    #     for cand in gen_mp.Gen(ur, ranking, **kwargs):
    #         yield cand
    # else:
    #     level = kwargs.get('level', ProsodicPhrase)
    #     baton = kwargs.get('baton', None)
    #     dopts = kwargs.get('dopts', [])
    #     filt = kwargs.get('filter', [])
    
    #     cc_coindex = constraints.CC_Corr in [constr[0] for constr in ranking]

    #     for cand in genRecurse(ur, segVariants(ur, ranking, **kwargs),
    #                            level, filter=filt, baton=baton,
    #                            cc_coindex=cc_coindex):
    #         cand.set_language(kwargs.get('lang', 'fallback'))
    #         yield cand

    # for dopt in dopts:
    #     yield dopt

def makeBRScript(cand, ranking):
    br_ranking = list(distilRanking(ranking, 'BR'))
    for idx in range(len(br_ranking)):
        br_ranking[idx] = list(br_ranking[idx])
        br_ranking[idx].remove('BR')
        br_ranking[idx] = tuple(br_ranking[idx])
    base = cand.copy()
    base.get_reduplicant().populate([])
    base.expand_reduplicants()
    return segVarScript(base, br_ranking)

def reduplicate(cand, ranking, key=0, script=None, baton=None):
    if script is None:
        script = makeBRScript(cand, ranking)

    while key < countSegVars(script):
        rup = segVariantByKey(script, key=key).next()[0]
        out = cand.copy()
        out.get_reduplicant().populate(rup)
        out.expand_reduplicants()
        newkey = yield (out, key)
        if newkey is None:
            key += 1
        else:
            key = newkey

def segVariants(ur, ranking, **kwargs):
    baton = kwargs.get('baton', None)
    filt = kwargs.get('filter', [])
    lang = kwargs.get('lang', 'fallback')
    verbose = kwargs.get('verbose', True)

    segs = flatten(ur)
    io_ranking = list(distilRanking(ranking, 'IO'))

    script = segVarScript(segs, io_ranking, **kwargs)
    if verbose:
        print >> sys.stderr, prettyPrintScript(script)

    if ur.get_reduplicant():
        br_ranking = list(distilRanking(ranking, 'BR'))
        for idx in range(len(br_ranking)):
            br_ranking[idx] = list(br_ranking[idx])
            br_ranking[idx].remove('BR')
            br_ranking[idx] = tuple(br_ranking[idx])
        for segs in generateFromScript(script):
            base = segs.copy()
            base.get_reduplicant().populate([])
            base.expand_reduplicants()
            for rup in segVariants(base, br_ranking, **kwargs):
                cand = segs.copy()
                cand.get_reduplicant().populate(rup)
                cand.expand_reduplicants()
                if applyFilter(ur, cand, filt):
                    yield cand
    else:
        if baton: 
            count = countSegVars(script)
            baton.message('%d potential segmental variants' % count)
            baton.twirl()
            baton.count = 0
            baton.end = float(count)
#            baton.percent = True
        for cand in generateFromScript(script):
            if applyFilter(ur, cand, filt):
                yield cand
            if baton: baton.twirl()

def segVarRecipe(n, script, **kwargs):
    '''Returns a recipe for the segmental variant that would be the
    nth (counting from 0) to be generated by
    generateFromScript(script).'''
    recipe = kwargs.get('recipe', None)
    divisors = kwargs.get('divisors', [])
    if recipe is None:
        recipe = [0] * len(script)
    if not divisors:
        for idx in range(len(script)):
            divisors.append(countSegVars(script[idx:]))
        divisors = divisors[1:]

    for idx in range(len(divisors)):
        if n >= divisors[idx]:
            recipe[idx] = n / divisors[idx]
            return segVarRecipe(n % divisors[idx], script, 
                                recipe=recipe, divisors=divisors)

    recipe[-1] = n 
    return recipe

def getCandidate(key, menu, seg_menu, script):
    buildon = segVariantByKey(script, key[0][0]).next()[0]
    for func in seg_menu:
        key_idx = seg_menu.index(func) + 1
        buildon = func[0](buildon, key=key[0][key_idx], *func[1:]).next()[0]

    for idx in range(1, len(key)):
        func = menu[idx - 1]
        buildon = func[0](buildon, key=key[idx], *func[1:]).next()[0]

    return buildon

def listKeyWrapper(buildon, builder, key=0, baton=None):
    nvs = builder[0](buildon, *builder[1:])

    while key < len(nvs):
        newkey = yield (nvs[key], key)
        if newkey is None:
            key += 1
            if baton:
                baton.twirl()
        else:
            key = newkey

def getScriptLang(script):
    for slot in script:
        for seq in slot:
            for item in seq:
                if isinstance(item, Segment) and hasattr(item, 'lang'):
                    return item.lang
    return 'fallback'

def segVariantByKey(script, key=0, **kwargs):
    lang = kwargs.get('lang', getScriptLang(script))
    baton = kwargs.get('baton', None)

    while key < countSegVars(script):
        recipe = segVarRecipe(key, script)
        out = Utterance(lang=lang)
        for idx in range(len(recipe)):
            for seg in script[idx][recipe[idx]]:
                out.append(seg.copy())
        newkey = yield (out, key)
        if newkey is None:
            key += 1
            if baton:
                baton.Update(baton.count)
        else:
            key = newkey

def oldSegVariants(ur, ranking, baton=None, **kwargs):
    level = kwargs.get('level', ProsodicPhrase)
    if baton is None:
        baton = kwargs.get('baton', None)
    filt = kwargs.get('filter', [])
    lang = kwargs.get('lang', 'fallback')

    segs = flatten(ur, Segment)
    out = [segs]

    MaxDone=False
    IdentDone=False

    io_ranking = list(distilRanking(ranking, 'IO'))

    if (constraints.Max_C in [c[0] for c in io_ranking] and
        constraints.Max_V in [c[0] for c in io_ranking]):
        if baton: baton.message("Working on Max.")
        out.extend(Max(segs, None, baton=baton))
        MaxDone=True

    if (constraints.Ident in [c[0] for c in io_ranking] and
        inventoryFilter in [c[0] for c in filt]):
        if baton: baton.message("Working on Ident.")
        for cand in out[:]:
            out.extend(invIdent(cand, io_ranking, baton=baton, lang=lang))
        IdentDone=True

    for idx in range(len(io_ranking) - 1, -1, -1):
        if (io_ranking[idx][0] == constraints.Ident and not IdentDone):
            for cand in out[:]:
                out.extend(Ident(cand, io_ranking[idx][1], 
                                 baton=baton))
        elif (io_ranking[idx][0] == constraints.Max_V and not MaxDone):
            for cand in out[:]:
                out.extend(Max(cand, 'voc', baton=baton))
        elif (io_ranking[idx][0] == constraints.Max_C and not MaxDone):
            for cand in out[:]:
                out.extend(Max(cand, 'cons', baton=baton))
        elif (io_ranking[idx][0] == constraints.Dep_V):
            for cand in out[:]:
                out.extend(Dep(cand, 'voc', baton=baton))
        elif (io_ranking[idx][0] == constraints.Dep_C):
            for cand in out[:]:
                out.extend(Dep(cand, 'cons', baton=baton))
        elif (io_ranking[idx][0] == constraints.DepMora):
            for cand in out[:]:
                out.extend(DepMora(cand, baton=baton))

    if ur.get_reduplicant():
        br_ranking = list(distilRanking(ranking, 'BR'))
        for idx in range(len(br_ranking)):
            br_ranking[idx] = list(br_ranking[idx])
            br_ranking[idx].remove('BR')
            br_ranking[idx] = tuple(br_ranking[idx])
        reds = out
        out = []
        for segs in reds:
            base = segs.copy()
            base.get_reduplicant().populate([])
            base.expand_reduplicants()
            for rup in segVariants(base, br_ranking, baton=baton, **kwargs):
                out.append(segs.copy())
                out[-1].get_reduplicant().populate(rup)
                out[-1].expand_reduplicants()

    return out

def trimSegVariants(ur, dupes, **kwargs):
    baton = kwargs.get('baton', None)
    filt = kwargs.get('filter', [])
    if baton:
        baton.message('Trimming segmental variants.')
        baton.twirl()
    dupes.sort(key=lambda x: x['segments'])
    out = []
    last = []
    for cand in dupes:
        if cand != last:
            out.append(cand)
            last = cand
        if baton: baton.twirl()

    return out

def segBuildMenu(ur, ranking):
    seg_menu = []
    if ur.get_reduplicant():
        seg_menu.append((reduplicate, ranking))
    # if ccTETU(ranking):
    #     seg_menu.append((CCVariantByKey, ranking))
    return seg_menu

def menuRecurse(ur, seq, menu, filt=(), current=None, baton=None, gen_all=False):
    if not menu:
        end_condition = True
    else:
        if current is None:
            idx = 0
        else:
            idx = menu.index(current) + 1
        end_condition = (idx >= len(menu))
        
    for (cand, key) in seq:
        if applyFilter(ur, cand, filt):
            if gen_all or end_condition:
                yield (cand, (key,))
            if not end_condition:
                next_seq = menu[idx][0](cand, baton=baton, *menu[idx][1:])
                if baton:
                    baton.message(menuFunctionMessage(menu[idx], cand), False)
                for (ncand, nkey) in menuRecurse(ur, next_seq, menu, 
                                                 current=menu[idx], filt=filt, 
                                                 baton=baton, gen_all=gen_all):
                    yield (ncand, (key,) + nkey)
        if current is None and baton is not None:
            baton.twirl()

def genRecurse(ur, seq, level, **kwargs):
    ranking = kwargs.get('ranking')
    currentLevel = kwargs.get('clevel', Segment)
    baton = kwargs.get('baton', None)
    filt = kwargs.get('filter', ())
    cc_coindex = kwargs.get('cc_coindex', False)

    level_mapping = {Syllable: (syllabificationByKey,),
                     Foot: (footGeneral,),
                     ProsodicWord: (hiLevelParse, ProsodicWord),
                     ProsodicPhrase: (hiLevelParse, ProsodicPhrase)}
    for (cand, idx) in seq:
        key = (idx,)
        yield {'form': cand,
               'key': (key,),
               'vio': []}
        if applyFilter(ur, cand, filt):
            if currentLevel is level:
                if cc_coindex:
                    for (ncand, idx) in genRecurse(ur, CCVariantByKey(cand, ranking),
                                                   level, clevel=currentLevel,
                                                   filter=filt, cc_coindex=False,
                                                   ranking=ranking):
                        yield ncand          
                else:
                    yield cand
            else:
                nextLevel = levels[levels.index(currentLevel) + 1]
                if nextLevel is Syllable:
                    for ncand in genRecurse(ur, syllabify(cand), level, 
                                            clevel=nextLevel, filter=filt,
                                            cc_coindex=cc_coindex):
                        yield ncand
                elif nextLevel is Foot:
                    for ncand in genRecurse(ur, footGeneral(cand), level, 
                                            clevel=nextLevel, filter=filt,
                                            cc_coindex=cc_coindex):
                        yield ncand
                else:
                    for ncand in genRecurse(ur, hiLevelParse(cand, nextLevel), 
                                            level, clevel=nextLevel, 
                                            filter=filt, cc_coindex=cc_coindex):
                        yield ncand                

def hiLevelParse(cand, level=ProsodicWord):
    out = []
    lang = flatten(cand)[0].lang

    permu = additPermutations(len(cand))

    cop = cand.copy()
    for perm in permu:
        pos = 0
        out.append(Utterance(lang=lang))
        for ln in perm:
            out[-1].append(level(lang=lang))
            for item in cop[pos:pos + ln]:
                out[-1][-1].append(item)
                if level is ProsodicWord:
                    if (type(item) is Foot and 
                        not hasattr(out[-1][-1], 'head')):
                        out[-1][-1].head = out[-1][-1].index(item)
            if level is not ProsodicWord:
                out[-1][-1].head = 0
            pos += ln
        out.append(out[-1].copy())
        needNew = False
        if level is ProsodicWord:
            for new in out[-1]:
                footcount = 0
                for elem in new:
                    if isinstance(elem, Foot):
                        footcount += 1
                if footcount > 1:
                    needNew = True
        for new in out[-1]:
            if level is not ProsodicWord:
                if len(new) > 1: needNew = True
            if level is ProsodicWord:
                for idx in range(len(new) - 1, -1, -1):
                    if type(new[idx]) is Foot:
                        new.head = idx
                        break
            else:
                new.head = (len(new) - 1)
        if not needNew: out = out[:-1]

    return out

def countCands(ur, ranking, level=Foot):
    out = 1

    for constr in ranking:
        if type(constr) is tuple and constr[0] is constraints.Ident:
            out = 2 ** out
        elif constr is constraints.Max_V:
            Vs = 0
            for seg in flatten(ur, Segment):
                if 'voc' in seg:
                    Vs += 1
            out = (2 ** Vs) * out
        elif constr is constraints.Max_C:
            Cs = 0
            for seg in flatten(ur, Segment):
                if 'cons' in seg:
                    Cs += 1
            out = (2 ** Cs) * out
    if level is Segment:
        return out
    
    if out == 0:
        return out
    
    out = 2 ** (out - 1)

    if level is Syllable:
        return out

    return 2 * (3 ** (out - 1))

def intToTorF(at, num):
    '''Converts an integer at into a list of length num of bools
    representing binary digits.'''
    if at >= 2 ** num:
        raise ValueError, ('int to convert should be less than 2 ** (length) = %d' %
                           (num, 2 ** num))
    test = at
    out = []
    dig = 0
    while dig < num:
        out.append(bool(test & 1))
        test = test >> 1
        dig += 1
    return out

def TorF(num, sofar=[]):
    '''Generates lists, being the exhaustive set of permutations of
    True and False of length num.  So, if num is 2, the output will be
    [[True, True], [True, False], [False, True], [False, False]] (or
    the same lists in a different order).'''
    at = 0
    while at < 2 ** num:
        yield intToTorF(at, num)
        at += 1

def additPermutations(target, max=None, **kwargs):
    '''Generates lists, being the exhaustive set of permutations of
    integers larger than 0 (and smaller than max, if max is not None)
    that sum to target. So, if target == 3, the output will be [1, 1,
    1], [1, 2], [2, 1], [3] (or the same lists in a different order).'''
    sofar = kwargs.get('sofar', [])
    if max is not None and max < target:
        mort = max
    else:
        mort = target

    for num in range(mort, 0, -1):
        out = sofar[:]
        out.append(num)
        if sum(out) == target:
            yield out
        elif sum(out) < target:
            for l in additPermutations(target, max, sofar=out):
                yield l

def syllabificationByKey(segs, key=(0, 0), baton=None):
    '''Generates tuples of syllabifications of segs and keys, where
    key is a 2-tuple of ints (n, n) corresponding to the nth TorF for
    syllable breaks and unparsed [s]s.  If a key is sent to the
    generator, the next output will be the syllabification for that
    key.'''
    lang = getattr(segs, 'lang', 'fallback')
    wbp = getattr(segs, 'weight_by_position', True)
    break_key = key[0]
    parse_key = key[1]
    newkey = None

    break_ops = 0
    for l in breakSyllAfter(segs):
        break_ops += 1
    break_ops = 2 ** break_ops

    while break_key < break_ops:
        broken = Utterance(lang=lang, weight_by_position=wbp)
        copies = segs.copy()
        brks = operations('break', breakSyllAfter(copies), intToTorF(break_key, break_ops))
        broken.append(Syllable(lang=lang))
        try:
            brk = brks.next()
        except StopIteration:
            brk = None

        for seg in copies:
            syll = broken[-1]
            syll.append(seg)
            if brk is not None:
                if (brk[1] is seg):
                    if brk[2]:
                        syll.hierarchize()
                        broken.append(Syllable(lang=lang))
                    try:
                        brk = brks.next()
                    except StopIteration:
                        brk = None
        syll.hierarchize()

        parse_ops = 0
        for l in canLeaveUnparsed(broken):
            parse_ops += 1
        parse_ops = 2 ** parse_ops

        while parse_key < parse_ops:
            out = broken.copy()
            ups = operations('parse', canLeaveUnparsed(out), intToTorF(parse_key, parse_ops))
            try:
                up = ups.next()
            except StopIteration:
                up = None

            for syll in out:
                if up is not None:
                    if (up[1] is syll[0]):
                        if up[2]:
                            syll.ons.remove(up[1])
                        try:
                            up = ups.next()
                        except StopIteration:
                            up = None
                if up is not None:
                    if (up[1] is syll[-1]):
                        if up[2]:
                            syll.cod.remove(up[1])
                        try:
                            up = ups.next()
                        except StopIteration:
                            up = None
                syll.getMoraCount()

            newkey = yield (out, (break_key, parse_key))
            if newkey is None:
                parse_key += 1
            else:
                break
        if newkey is None:
            break_key += 1
            parse_key = 0
        else:
            (break_key, parse_key) = newkey

def breakSyllAfter(segs):
    '''Generates Segments from the sequence segs after which it is
    permissible to put a syllable boundary.'''
    last = segs[0]
    for seg in segs[1:]:
        if seg != last or 'voc' not in last:
            yield last
        last = seg

def possibleSegments(seg, ipas, max_feats, dep_feats):
    for feats in ipas.values():
        feats = set(feats)
        if ((seg - feats) <= max_feats
            and (feats - seg) <= dep_feats):
            out = seg.copy()
            out.clear()
            out.update(feats)
            yield out

def canLeaveUnparsed(sylls):
    '''Generates Segments from the sequence sylls which it is
    permissible to leave unparsed into a Syllable's pre-terminal node
    (ons, nuc or cod).  Currently, this means it generates all the
    [s]s that are edgemost in their Syllables.'''
    for syll in sylls:
        if ((not syll[0] ^ set(["cons", "obs", "strid", "cont", "cor"]))
            and idIn(syll[0], syll.ons)):
            yield syll[0]
        if ((not syll[-1] ^ set(["cons", "obs", "strid", "cont", "cor"]))
            and idIn(syll[-1], syll.cod)):
            yield syll[-1]

def operations(oper, test_gen, torf=None):
    '''Generates tuples (oper, elem) for each elem yielded by
    test_gen.  If a torf is supplied, the tuples yielded have a
    third, boolean element taken from it.'''
    for elem in test_gen:
        if torf is None:
            yield (oper, elem)
        else:
            yield (oper, elem, torf.pop(0))

def idIn(cmp, seq):
    for l in seq:
        if l is cmp:
            return True
    return False

def syllabify(segs, wbp=False, baton=None):
    '''Generates all possible syllabifications of the sequence of
    segments segs (and many that are probably impossible too!).  If
    wbp is True, generates two variants of each syllabification: one
    in which syllables have weight by position, and one in which they
    do not.  If wbp is False, weight by position is inherited from
    segs or set as the default for Utterance objects.'''
    baton = kwargs.get('baton', None)
    out = []

    permu = additPermutations(len(segs))

    for perm in permu:
        next = False
        edgeS = 0
        cop = Utterance(lang=segs[0].lang, lex=False)
        for seg in segs:
            cop.append(seg.copy())
        pos = 0
        out.append(Utterance(lang=segs[0].lang, lex=False))
        for ln in perm:
            if len(cop) > pos + ln:
                if (cop[pos + ln] == cop[pos + ln - 1] 
                    and 'voc' in cop[pos + ln]):
                    out = out[:-1] # Don't split long vowels!
                    next = True
                    break

            out[-1].append(Syllable(lang=segs[0].lang))
            out[-1][-1].extend(cop[pos:pos + ln])
            out[-1][-1].hierarchize()
            out[-1][-1].getMoraCount()
            if not (out[-1][-1][0] ^ 
                    set(["cons", "obs", "strid", "cont", "cor"])):
                edgeS += 1
            if not (out[-1][-1][-1] ^ 
                    set(["cons", "obs", "strid", "cont", "cor"])):
                edgeS += 1
            pos += ln
            if baton: baton.Update(baton.count)

        if next: continue
        parsed = out[-1]
        for torf in TorF(edgeS):
            new = parsed.copy()
            for idx in range(len(new)):
                if not (new[idx][0] ^ 
                    set(["cons", "obs", "strid", "cont", "cor"])):
                    if torf.pop() and len(new[idx].ons) > 1:
                        new[idx].ons = new[idx].ons[1:]
                if not (new[idx][-1] ^
                    set(["cons", "obs", "strid", "cont", "cor"])):
                    if torf.pop() and len(new[idx].cod) > 1:
                        new[idx].cod = new[idx].cod[:-1]
                if baton: baton.Update(baton.count)
            if not new in out:
                out.append(new)
    return out

def footGeneral(sylls, **kwargs):
    '''Returns a list of all possible footings of the sequence of
    syllables sylls.  Also returns sequences in which some (or all)
    syllables are not parsed.'''
    out = []
    footOrNots = TorF(len(sylls))
    footOrNots.next() # At least one foot must be parsed.

    for footOrNot in footOrNots:
        toAdd = [Utterance([])]
        template = [footOrNot[0], 1]

        state = template[0]
        for boo in footOrNot[1:]:
            if boo == state:
                template[-1] += 1
            else:
                template.append(1)

        state = template[0]

        pos = 0
        for seq in template[1:]:
            if state:
                for addend in toAdd[:]:
                    toAdd.remove(addend)
                    toAdd.extend([Utterance(addend + fs) for fs in 
                                  footSequence(sylls[pos:pos + seq], **kwargs)])
            else:
                for addend in toAdd[:]:
                    addend.extend(sylls[pos:pos + seq])
            pos += seq
            state = not state

        for addend in toAdd:
            if not addend in out:
                out.append(addend)

    return out

def trimHeaded(head, perm):
    for ind in range(len(perm)):
        if perm[ind] == 1 and not head[ind]:
            return False
    return True

def footSequence(sylls, **kwargs):
    '''Returns a list of parses of the sequence sylls into feet.  All
    syllables are parsed.'''
    out = []
    filter = kwargs.get('filter', ())
    permu = additPermutations(len(sylls), (constraints.FtBin in filter and 2 or None))
    lang = flatten(sylls)[0].lang

    for perm in permu:
        headed = TorF(len(perm))
        for head in headed:
            if trimHeaded(head, perm):
                pos = 0
                out.append(Utterance(lang=lang))
                for ind in range(len(head)):
    #                print pos, perm, ind
                    out[-1].append(Foot(lang=lang))
                    out[-1][-1][:] = sylls[pos:pos + perm[ind]]
                    if head[ind]:
                        out[-1][-1].head = len(out[-1][-1]) - 1
                    else:
                        out[-1][-1].head = 0
                    pos += perm[ind]

    return out

def inventoryFilter(inp, outp, *args):
    if args:
        ipas = config.get_ipas(args[0], 'ipa', lex=False)
    else:
        ipas = config.get_ipas('fallback', 'ipa', lex=False)
    for seg in flatten(outp, Segment):
        if not seg.ipa in ipas:
            return 1
    return 0

