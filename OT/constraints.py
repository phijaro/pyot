from representations import *
from types import FunctionType

modes = ('IO', 'BR', 'CC')

def __repr_form(constr):
    out = '(%s' % constr[0].__name__
    for parm in constr[1:]:
        if parm in levels:
            parm_str = parm.__name__
        else:
            parm_str = repr(parm)
        out += ', %s' % parm_str
    return out

def mode(constr):
    for mode in modes:
        if mode in constr:
            return mode
    return 'IO'

def GlideFeats(inp, outp, *featspec):
    '''Assess a violation for each segment that is the rightmost
    constituent of a syllable nucleus, and does not contain all the
    features in featspec.'''
    out = 0

    for syll in flatten(outp, Syllable):
        if len(__uniqueSegments(syll.nuc)) > 1:
            if set(featspec) - syll.nuc[-1] == set(featspec):
                out += 1

    return out

def StressMaxMora(inp, outp):
    '''Positional variant of MaxMora that applies only to stressed
    syllables.'''
    out = 0

    last = None
    flat = flatten(outp, Foot)
    for foot in flat:
        if isinstance(foot, Foot):
            test = Utterance()
            syll = foot[foot.head]
            test.append(syll.nuc)
            vio = MaxMora(inp, test)
            out += vio
        last = foot

    return out

def NonFinParse(inp, outp):
    out = 0

    for prwd in flatten(outp, ProsodicWord):
        if isinstance(prwd[-1], Foot):
            out += 1

    return out

def NonFinality(inp, outp):
    out = 0

    for prwd in flatten(outp, ProsodicWord):
        if isinstance(prwd[-1], Foot):
            if prwd[-1].head == (len(prwd[-1]) - 1):
                out += 1

    return out

def Inalterable(inp, outp):
    out = 0
    last = None

    for seg in flatten(outp):
        if last is not None:
            if seg.indices == last.indices and seg ^ last:
                out += 1
        last = seg

    return out

def CondAgree(inp, outp, p_featspec, q_featspec):
    out = 0
    p = []
    q = []
    last = None

    if type(p_featspec) in (str, unicode):
        p = set((p_featspec,))
    else:
        p = set(p_featspec)
    if type(q_featspec) in (str, unicode):
        q = set((q_featspec,))
    else:
        q = set(q_featspec)

    for seg in __uniqueSegments(outp):
        if last is not None:
            if seg.issuperset(p) and last.issuperset(p):
                if (q - seg) != (q - last):
                    out += 1
        last = seg

    return out

def CodaCond(inp, outp, feat):
    out = 0

    for syll in flatten(outp, Syllable):
        for seg in syll.cod:
            if feat in seg:
                out += 1

    return out

def AlignFoot(inp, outp, direction):
    out = 0

    outp = flatten(outp, Foot)
    intervening = []

    for ind in range(len(outp)):
        if type(outp[ind]) is Foot:
            if direction == 'L':
                intervening = outp[:ind]
            elif direction == 'R':
                intervening = outp[ind + 1:]
            out += len(flatten(intervening, Syllable))

    return out

def AlignHeadFootInWord(inp, outp, direction):
    out = 0

    for word in flatten(outp, ProsodicWord):
        if hasattr(word, 'head'):
            if direction == 'L':
                return word.head
            elif direction == 'R':
                return len(word) - word.head - 1

    return out

def HavePlace(inp, outp):
    out = 0

    for seg in __uniqueSegments(outp):
        v = True
        for feat in ('cor', 'dor', 'lab', 'rad'):
            if feat in seg:
                v = False
        if v: out += 1

    return out

def GOCP(inp, outp, feat, sep_num):
    '''Assess a violation for pairs of Segments possessing the feature
    feat that are separated by material amounting to no more than
    sep_num morae.'''
    out = 0

    lag = None
    for seg in flatten(outp):
        if feat in seg:
            if lag is not None and lag <= sep_num:
                out += 1
            lag = 0
        elif lag is not None:
            lag += 1

    return out

def StarGeminate(inp, outp):
    out = 0
    last = set([])
    
    for seg in flatten(outp):
        if 'cons' in seg and (not seg ^ last):
            out +=1
        last = seg

    return out

def StressedIdent(inp, outp, feat, mode='IO'):
    out = 0

    for foot in flatten(outp, Foot):
        if type(foot) is foot:
            out += Ident(inp, foot[foot.head], feat, mode)

    return out

def IdentCC(inp, outp, *featspec):
    pair = []
    out = 0

    for seg in flatten(outp):
        if seg.indices.get('CC'):
            pair.append(seg)
            while len(pair) > 2:
                pair.pop(0)
            if len(pair) == 2:
                if (set(featspec) - pair[0]) != (set(featspec) - pair[1]):
                    out += 1

    return out

def notIdentCC(inp, outp, *featspec):
    pair = []
    out = 0

    for seg in flatten(outp):
        if seg.indices.get('CC', False):
            pair.append(seg)
            while len(pair) > 2:
                pair.pop(0)
            if len(pair) == 2:
                if (set(featspec) - pair[0]) == (set(featspec) - pair[1]):
                    out += 1

    return out

def OFTwixtSonV(inp, outp):
    out = 0

    segs = flatten(outp)
    behind = 0
    ahead = 2
    for seg in segs[1:-1]:
        if ('son' in segs[behind] and
            'son' in segs[ahead] and
            len(set(('lab', 'cor', 'dor')) - seg) != 3 and
            seg.issuperset(('cons', 'obs', 'cont')) and
            (not 'voi' in seg)):
            out += 1
        behind += 1
        ahead += 1

    return out

def IOralFricV(inp, outp):
    out = 0

    segs = flatten(outp)
    behind = 0
    ahead = 2
    for seg in segs[1:-1]:
        if ('voc' in segs[behind] and
            'voc' in segs[ahead] and
            len(set(('lab', 'cor', 'dor')) - seg) != 3 and
            seg.issuperset(('cons', 'obs', 'cont')) and
            (not 'voi' in seg)):
            out += 1
        behind += 1
        ahead += 1

    return out

def IOV(inp, outp):
    out = 0

    segs = flatten(outp)
    behind = 0
    ahead = 2
    for seg in segs[1:-1]:
        if ('voc' in segs[behind] and
            'voc' in segs[ahead] and
            'obs' in seg and
            (not 'voi' in seg)):
            out += 1
        behind += 1
        ahead += 1

    return out

def OralFricStrid(inp, outp):
    out = 0

    for seg in __uniqueSegments(outp):
        if (len(set(('lab', 'cor', 'dor')) - seg) != 3 and
            seg.issuperset(('cons', 'cont', 'obs')) and
            (not 'strid' in seg)):
            out += 1

    return out

def OralFricVoiceless(inp, outp):
    out = 0

    for seg in __uniqueSegments(outp):
        if (len(set(('lab', 'cor', 'dor')) - seg) != 3 and
            seg.issuperset(('cons', 'cont', 'obs', 'voi'))):
            out += 1

    return out

def StarLongFeat(inp, outp, feat):    
    '''Assess a violation for every long segment in the output that
    has the feature feat.'''
    out = 0

    indicesDone = []
    segs = flatten(outp)
    for seg in __uniqueSegments(outp):
        if __isLong(seg, outp) and feat in seg:
            out += 1

    return out
    
def CC_Corr(inp, outp, featspec):
    out = 0

    if type(featspec) is str:
        featspec = (featspec,)

    for seg in flatten(outp):
        if seg.issuperset(featspec) and not seg.indices.get('CC', False):
            out += 1

    return out

def StressToWeight(inp, outp):
    out = 0

    for foot in flatten(outp, Foot):
        if type(foot) == Foot and foot[foot.head].getMoraCount() < 2:
            out += 1

    return out

def EntailsFeats(inp, outp, p_featspec, q_featspec):
    out = 0
    p = []
    q = []
    if type(p_featspec) in (str, unicode):
        p = (p_featspec,)
    else:
        p = p_featspec
    if type(q_featspec) in (str, unicode):
        q = (q_featspec,)
    else:
        q = q_featspec

    for seg in __uniqueSegments(outp):
        if seg.issuperset(p) and not seg.issuperset(q):
            out += 1

    return out
        
def StarFeats(inp, outp, *featspec):
    out = 0

    for seg in __uniqueSegments(outp):
        if seg.issuperset(featspec):
            out += 1

    return out

# Note: this constraint has been inserted for backwards compatibility
def StarFeat(inp, outp, *featspec):
    out = 0

    for seg in __uniqueSegments(outp):
        if seg.issuperset(featspec):
            out += 1

    return out

def MaxDepWord(inp, outp, fcons=None, mode='IO'):
    (inp, outp) = __BR_faith(inp, outp, mode)
    ocount = 0
    icount = 0
    if not flatten(outp, ProsodicWord):
        return 0
    for word in flatten(outp, ProsodicWord):
        ocount += 1
    for word in flatten(inp, ProsodicWord):
        icount += 1
    if fcons is None:
        return abs(ocount - icount)
    if fcons == 'Dep' and ocount > icount:
        return ocount - icount
    if fcons == 'Max' and icount > ocount:
        return icount - ocount
    return 0

# def AlignHeadFootInWord(inp, outp, *args):
#     out = 0

#     for word in flatten(outp, ProsodicWord):
#         if hasattr(word, 'head'):
#             fcount = 0
#             first = None
#             for idx in range(len(word)):
#                 if type(word[idx]) is Foot:
#                     if first is None:
#                         first = idx
#                     fcount += 1
#             if args[0] == 'L' and word.head != first:
#                 out += 1
#             elif args[0] == 'R' and word.head != fcount:
#                 out += 1
#         else:
#             out += 1

#     return out

def MaxFeat(inp, outp, feat, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)

    inp = inp.expand_morphemes()
    if len(inp) > 1:
        viols = []
        for inpt in inp:
            viols.append(MaxFeat(inpt, outp, feat))
        return min(viols)
    elif len(inp) == 1:
        inp = inp[0]
        for seg in flatten(outp):
            corr = inp.unitByIndex(seg.indices.get('IO', None))
            if corr:
                if ((feat in corr) and (not (feat in seg))):
                    out += 1

    return out

def DepFeat(inp, outp, feat, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)

    inp = inp.expand_morphemes()
    if len(inp) > 1:
        viols = []
        for inpt in inp:
            viols.append(DepFeat(inpt, outp, feat))
        return min(viols)
    elif len(inp) == 1:
        inp = inp[0]
        for seg in flatten(outp):
            corr = inp.unitByIndex(seg.indices.get('IO', None))
            if corr:
                if ((feat not in corr) and (feat in seg)):
                    out += 1

    return out

def DiphHigh(inp, outp):
    '''Assess a violation for every diphthong in the output whose
    final element lacks the feature "high"'''
    out = 0
    
    for syll in flatten(outp, Syllable):
        v = False
        if len(syll.nuc) > 1 and not 'high' in syll.nuc[-1]:
            last = syll.nuc[0]
            for seg in syll.nuc:
                if seg != last:
                    v = True
                last = seg

        if v: out += 1
    return out

def Max_C_NonFinal(inp, outp):
    out = 0
    
    for seg in flatten(inp, Segment)[:-1]:
        if not outp.unitByIndex(seg.indices.get('IO', None)):
            out += 1

    return out

def Max_V_NonFinal(inp, outp):
    out = 0

    for syll in flatten(inp, Syllable)[:-1]:
        for seg in syll:
            if 'voc' in seg and not outp.unitByIndex(seg.indices.get('IO', None)):
                out += 1

    return out
   
def OnsetSonority(inp, outp, num):
    out = 0
    
    for syll in flatten(outp, Syllable):
        viol = False
        
        for ind in range(len(syll.ons) - 1):
            son_here = max(gradeSonority(syll.ons[ind]), 1)
            son_next = max(gradeSonority(syll.ons[ind + 1]), 1)

            if son_here > son_next:
                viol = True
            elif son_next - son_here < num:
                viol = True
        if viol:
            out += 1

    return out

def CodaSonority(inp, outp, num):
    out = 0
    
    for syll in flatten(outp, Syllable): 
        viol = False
        if len(syll.cod) > 1:
            for ind in range(len(syll.cod) - 1, 0, -1):
                if (gradeSonority(syll.cod[ind]) > 
                    gradeSonority(syll.cod[ind - 1])):
                    viol = True
                elif (gradeSonority(syll.cod[ind - 1]) - 
                      gradeSonority(syll.cod[ind]) < num):
                    viol = True

        if viol:
            out += 1

    return out

def NucSonority(inp, outp, num):
    out = 0
    
    for syll in flatten(outp, Syllable): 
        viol = False
        last = None
        for seg in __uniqueSegments(syll.nuc):
            if last is not None:
                for ind in range(len(syll.nuc) - 1, 0, -1):
                    if (gradeSonority(seg) > 
                        gradeSonority(last)):
                        viol = True
                    elif (gradeSonority(last) - 
                          gradeSonority(seg) < num):
                        viol = True
            last = seg

        if viol:
            out += 1

    return out

def StarSemiVowel(inp, outp):
    '''Assess a violation for every semivowel in outp.'''
    out = 0

    indsDone = []
    for seg in flatten(outp):
        if "son" in seg and ("cons" in seg and
                             not ("rho" in seg or
                                  "lat" in seg or
                                  "plos" in seg)):
            if not seg.indices.get('IO', None) in indsDone:
                out += 1
                indsDone.append(seg.indices.get('IO', None))

    return out

def StarVoicelessV(inp, outp):
    '''Assesses a violation for every vowel in outp that lacks the
    feature [voice].'''
    out = 0

    for seg in flatten(outp, Segment):
        if 'voc' in seg and not 'voi' in seg:
            out += 1

    return out

def OnsetSonLow(inp, outp):
    '''Assesses a number of violations equal to the combined sonority
    score of the segments of every syllable onset in outp.'''
    out = 0

    for syll in flatten(outp, Syllable):
        for seg in syll.ons:
            out += gradeSonority(seg)

    return out

def MinNucSon(inp, outp, num=3):
    '''Assesses a violation for every syllable in outp whose nucleus
    contains a segment whose sonority is lower than num.'''
    out = 0

    for syll in flatten(outp, Syllable):
        syllSon = min([gradeSonority(seg) for seg in syll.nuc])
        if syllSon < num:
            out += 1

    return out

def StarSyllC(inp, outp):
    '''Assesses a violation for every syllable in outp whose nucleus
    begins with a segment with the feature [cons].'''
    out = 0
    for syll in flatten(outp, Syllable):
        if 'cons' in syll.nuc[0]:
            out += 1
    return out

def Onset(inp, outp):
    '''Assesses a violation for every syllable in outp which lacks an
    onset.'''
    out = 0
    for syll in flatten(outp, Syllable):
        if len(syll.ons) < 1:
            out += 1
    return out

def StarTriMora(inp, outp):
    out = 0
    for syll in flatten(outp, Syllable):
        if syll.getMoraCount() >= 3:
            out += 1
    return out

def NoCoda(inp, outp):
    out = 0
    for syll in flatten(outp, Syllable):
        if syll.cod:
            out += 1
    return out

def StarVoicedCoda(inp, outp):
    out = 0
    for syll in flatten(outp, Syllable):
        viol = False
        for seg in syll.cod:
            if 'voi' in seg:
                viol = True
        if viol:
            out += 1
    return out

def StarNCsubring(inp, outp):
    out = 0

    segs = flatten(outp)

    for ind in range(len(segs)):
        if ind < len(segs) - 1:
            if 'nas' in segs[ind]:
                if 'cons' in segs[ind + 1] and \
                   not 'nas' in segs[ind + 1] and \
                   not 'voi' in segs[ind + 1]:
                    out += 1

    return out

def WeightByPosition(inp, outp):
    out = 0

    for syll in flatten(outp, Syllable):
        if syll.getMoraCount() < len(syll.nuc) + len(syll.cod):
            out += 1

    return out

def Dep(inp, outp, unit=Segment, mode='IO'):
    (inp, outp) = __BR_faith(inp, outp, mode)

    inUnits = len(flatten(inp, unit))
    outUnits = len(flatten(outp, unit))
    out = outUnits - inUnits
    if out > 0: return out
    else: return 0

def Max(inp, outp, unit=Segment, mode='IO'):
    (inp, outp) = __BR_faith(inp, outp, mode)

    inUnits = len(flatten(inp, unit))
    outUnits = len(flatten(outp, unit))
    out = inUnits - outUnits
    if out > 0: return out
    else: return 0

def Dep_V(inp, outp, mode='IO'):
    (inp, outp) = __BR_faith(inp, outp, mode)

    out = 0

    for seg in flatten(outp, Segment):
        if 'voc' in seg and seg.indices.get('IO', None) == 0:
            out += 1

    return out

def ParseSeg(inp, outp, syllnode=None):
    out = 0

    for syll in flatten(outp, Syllable):
        if syllnode is None:
            out += len(syll.edgemostSegs('R'))
            out += len(syll.edgemostSegs('L'))
        elif syllnode == 'ons':
            out += len(syll.edgemostSegs('L'))
        elif syllnode == 'cod':
            out += len(syll.edgemostSegs('R'))
    
    return out

def RedLength(inp, outp, unit, num):
    red = __getReduplicant(outp, unit)
    count = 0

    for unit in flatten(red, unit):
        if type(unit) is unit:
            count += 1
            if count > num:
                return 1
    return 0

def LenLimit(inp, outp, syllnode, num, mode='IO'):
    out = 0

    if mode == 'BR': outp = __getReduplicant(outp, Syllable)

    for syll in flatten(outp, Syllable):
        unit = eval('syll.%s' % syllnode)
        if len(unit) > num:
            out += 1

    return out

def Dep_C(inp, outp, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)
                
    for seg in flatten(outp, Segment):
        if 'cons' in seg and seg.indices.get('IO', None) is None:
            out += 1

    return out

def Max_V(inp, outp, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)
                
    for seg in flatten(inp, Segment):
        if 'voc' in seg and not outp.unitByIndex(seg.indices.get('IO', None)):
            out += 1

    return out

def Max_C(inp, outp, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)

    for seg in flatten(inp):
        if 'cons' in seg and not outp.unitByIndex(seg.indices.get('IO', None)):
            out += 1

    return out

def Max_LongV(inp, outp, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)

    if flattenable(inp, Syllable):
        for syll in flatten(inp, Syllable):
            viol = False
            if len(syll.nuc) == 1:
                if syll.getMoraCount() > len(syll.nuc) + len(syll.cod):
                    if not outp.unitByIndex(syll.nuc[0].indices.get('IO', None)):
                        out += 1
            else:
                for seg in syll.nuc:
                    if not outp.unitByIndex(seg.indices.get('IO', None)):
                        viol = True
                    for osyll in flatten(outp, Syllable):
                        if (len(osyll.nuc) == 1 and 
                            (seg.indices.get('IO', None) == osyll.nuc[0].indices.get('IO', None))):
                            viol = True
                if viol:
                    out += 1
    else:
        for ind in range(len(inp)):
            seg = inp[ind]
            if 'long' in seg:
                if not outp.unitByIndex(seg.indices.get('IO', None)):
                    out += 1
            if ind != 0 and 'voc' in seg:
                if 'voc' in inp[ind - 1]:
                    if not outp.unitByIndex(seg.indices.get('IO', None)):
                        out += 1
            if ind != len(inp) - 1 and 'voc' in seg:
                if 'voc' in inp[ind + 1]:
                    if not outp.unitByIndex(seg.indices.get('IO', None)):
                        out += 1
            
    return out
    
def DepMora(inp, outp, mode='IO'):   
    out = 0
    (inp, outp) = __BR_faith(inp, outp, mode)

    for syll in flatten(outp, Syllable):
        for node in (syll.nuc, syll.cod):
            for seg in __uniqueSegments(node):
                if __isLong(seg, outp):
                    corr = inp.unitByIndex(seg.indices.get('IO', None))
                    if not __isLong(corr, inp):
                        out += 1

    return out

    # inMorae = 0
    # outMorae = 0
    # for syll in flatten(inp, Syllable):
    #     inMorae += syll.MoraCount
    # for syll in flatten(outp, Syllable):
    #     outMorae += syll.MoraCount
    # out = outMorae - inMorae
    # if out > 0: return out
    # else: return 0

def MaxMora(inp, outp, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)

    for seg in __uniqueSegments(inp):
        if __isLong(seg, inp):
            corr = outp.unitByIndex(seg.indices.get('IO', None))
            if corr is not None:
                if not __isLong(corr, outp):
                    out += 1

    return out
    
    # for syll in flatten(inp, Syllable):
    #     for seg in syll.nuc:
    #         if not outp.unitByIndex(seg.indices.get('IO', None)):
    #             out += 1
    #     for seg in syll.cod:
    #         if not outp.unitByIndex(seg.indices.get('IO', None)):
    #             out += 1

    # last = None
    # doppels = {}
    # for seg in flatten(inp, Segment):
    #     if last is not None:
    #         if seg.indices.get('IO', None) == last.indices.get('IO', None):
    #             doppels[seg.indices.get('IO', None)] = 0
    #     last = seg

    # for seg in flatten(outp, Segment):
    #     if seg.indices.get('IO', None) in doppels:
    #         doppels[seg.indices.get('IO', None)] += 1

    # for idx in doppels:
    #     if doppels[idx] == 1:
    #         out += 1

    # return out              

def PVL(inp, outp):
    out = 0
    for syll in flatten(outp, Syllable):
        if len(syll.nuc) == 1 and \
           syll.MoraCount == len(syll.nuc) + len(syll.cod):
            if syll.cod:
                feats = syll.cod[0]
                if ('cons' in feats and 
                    'voi' in feats and 
                    not 'cont' in feats):
                    out += 1
    return out

def Ident(inp, outp, feat, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)

    inp = inp.expand_morphemes()
    if len(inp) > 1:
        viols = []
        for inpt in inp:
            viols.append(Ident(inpt, outp, feat, mode))
        return min(viols)
    elif len(inp) == 1:
        inp = inp[0]
        for seg in flatten(outp):
            corr = inp.unitByIndex(seg.indices.get('IO', None))
            if corr is not None:
                if ((feat in seg) != (feat in corr)):
                    out += 1

    return out

def OnsMax(inp, outp, feat=None, mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)
                
    for syll in flatten(inp, Syllable):
        for seg in syll.ons:
            if not outp.unitByIndex(seg.indices.get('IO', None)):
                if feat is None:
                    out += 1
                elif feat in seg:
                    out += 1
    
    return out

def OnsIdent(inp, outp, feat='voi', mode='IO'):
    out = 0

    (inp, outp) = __BR_faith(inp, outp, mode)
                
    inp = inp.expand_morphemes()
    if len(inp) > 1:
        viols = []
        for inpt in inp:
            viols.append(Ident(inpt, outp, feat))
        return min(viols)
    else:
        inp = inp[0]
    for syll in flatten(outp, Syllable):
        for seg in syll.ons:
            if inp.unitByIndex(seg.indices.get('IO', None)):
                if ((feat in seg) != 
                    (feat in inp.unitByIndex(seg.indices.get('IO', None)))):
                        out += 1

    return out

def VOP(inp, outp):
    out = 0

    indsDone = []
    for seg in flatten(outp):
        if 'voi' in seg and 'obs' in seg:
            if not seg.indices.get('IO', None) in indsDone:
                out += 1
                indsDone.append(seg.indices.get('IO', None))

    return out

def __isLong(seg, po):
    if not isinstance(seg, Segment):
        return False
    ind = flatten(po).index(seg)
    test = flatten(po).copy()
    indices = seg.indices
    test.pop(ind)

    for tseg in test:
        if getattr(tseg, 'indices', None) == indices:
            return True
    return False

def __testAgreePlace(seq):
    out = 0

    for ind in range(len(seq) - 1):
        if ind is 0: place = []
        place1 = []
        for feat in ('lab', 'cor', 'dor', 'rad', 'high', 'low', 'atr', 'rtr'):
            if feat in seq[ind] and ind is 0:
                place.append(feat)
            if feat in seq[ind + 1]:
                place1.append(feat)
        if place != place1:
            out += 1
        place = place1

    return out

def StarClash(inp, outp):
    out = 0
    last = None

    for foot in flatten(outp, Foot):
        if last is not None:
            if isinstance(foot, Foot) and isinstance(last, Foot):
                if ((last.head == (len(last) - 1))
                    and
                    (foot.head == 0)):
                    out += 1
    return out

def SyllStarGeminate(inp, outp):
    '''Assesses a violation for every geminate consonant in the output
    which is dominated by only one syllable.'''
    out = 0

    for syll in flatten(outp, Syllable):
        out += StarGeminate(None, syll)

    return out

def ClusterHomorganic(inp, outp):
    '''Assess a violation for every consecutive pair of segments in
    the output that differs only with respect to place features.'''
    out = 0
    last = None

    for seg in flatten(outp):
        if last is not None:
            sdiff = seg ^ last
            if sdiff.issubset(('lab', 'cor', 'dor', 'rad', 'high', 'low', 'atr', 'rtr')):
                out += 1
        last = seg

    return out

def NodeAgree(inp, outp, feat):
    out = 0

    for syll in flatten(outp, Syllable):
        for node in (syll.ons, syll.nuc, syll.cod):
            last = None
            for seg in __uniqueSegments(node):
                if last is not None:
                    if (feat in seg) != (feat in last):
                        out += 1
                last = seg

    return out

def DiphAgree(inp, outp, feat):
    out = 0

    for syll in flatten(outp, Syllable):
        last = None
        for seg in __uniqueSegments(syll.nuc):
            if last is not None:
                if (feat in seg) != (feat in last):
                    out += 1
            last = seg

    return out
    
def AgreePlaceNode(inp, outp):
    out = 0

    for syll in flatten(outp, Syllable):
        for node in (syll.ons, syll.nuc, syll.cod):
            out += __testAgreePlace(node)

    return out
    
def AgreePlace(inp, outp):
    out = 0
    segs = flatten(outp, Segment)

    out = __testAgreePlace(segs)
    
    return out

def Agree(inp, outp, feat='voi'):
    out = 0

    segs = flatten(outp)

    for ind in range(len(segs)):
        if 'cons' in segs[ind]: # If seg[ind] is a consonant,
            if ind < len(segs) - 1: # and non-final
                if 'cons' in segs[ind + 1]: # and the following segment is also
                                            # a consonant
                    if ((feat in segs[ind]) != 
                        (feat in segs[ind + 1])):
#                                          and they don't agree
#                                          in respect of the feature feat,
                        out += 1 #         assign a violation.

    return out

# def Align(inp, outp, args={'low': Foot,
#                            'ledge': 0,
#                            'high': ProsodicWord,
#                            'hedge': 0}):
#     out = 0

#     for hunit in flatten(outp, args['high']):
#         for lunit in hunit:
#             if type(lunit) is args['low']:
#                 if hunit.index(lunit) != args['hedge']:
#                     intervening = flatten(hunit[:hunit.index(lunit)], 
#                                           Syllable)
#                     out += len(intervening)

#     return out

# Doing Generalized Alignment with one constraint schema is a nice
# idea, but at the moment it doesn't work.

def OCP(inp, outp, feat):
    out = 0

    last = []
    for seg in __uniqueSegments(outp):
        if feat in seg and feat in last:
            out += 1
        last = seg

    return out

def FtBin(inp, outp):
    out = 0
    
    for Ft in flatten(outp, Foot):
        totalMoraCount = 0
        if type(Ft) is Foot:
            for syll in Ft:
                totalMoraCount += syll.getMoraCount() 

            if len(Ft) > 2:
                out += 1
            elif len(Ft) < 2:
                if totalMoraCount < 2:
                    out += 1
            # else:
            #     if totalMoraCount != 2:
            #         out += 1

    return out

def WeightToStress(inp, outp):
    out = 0

    for word in flatten(outp, ProsodicWord):
        for elem in word:
            if isinstance(elem, Syllable):
                if elem.getMoraCount() > 1:
                    out += 1
            elif isinstance(elem, Foot):
                for syll in elem:
                    if syll.getMoraCount() > 1 and elem[elem.head] != syll:
                        out += 1
    
    return out

def FtTypeT(inp, outp):
    out = 0

    for Ft in flatten(outp, Foot):
        if type(Ft) is Foot:
            if Ft.head != 0:
                out += 1

    return out

def FtTypeI(inp, outp):
    out = 0

    for Ft in flatten(outp, Foot):
        if type(Ft) is Foot:
            if Ft.head != (len(Ft) - 1):
                out += 1

    return out

def ParseSyll(inp, outp):
    out = 0

    for pwd in flatten(outp, ProsodicWord):
	for unit in pwd:
	    if type(unit) == Syllable:
		out += 1

    return out
    # cont = False
    # for tl in outp:
    #     if type(tl) in (Foot, ProsodicWord, ProsodicPhrase):
    #         cont = True
    # if not cont:
    #     return 0

    for parent in outp:
        if type(parent) == Syllable:
            if not type(outp) == Foot:
                out += 1
        elif type(parent) == Segment:
            pass
        else:
            out += ParseSyll(inp, parent, True)

    return out

def IVV(inp, outp):
    segs = flatten(outp, Segment)
    out = 0
    
    for idx in range(1, len(segs) - 1):
        if ('cons' in segs[idx] and
            'voc' in segs[idx - 1] and
            'voc' in segs[idx + 1] and
            (not 'voi' in segs[idx])):
            out += 1

    return out

def UnparsedEdgemost(inp, outp):
    '''Assess a violation for every Segment in outp that appears in a
    Syllable but not in the ons, nuc, or cod and has a Segment between
    it and the relevant edge of the Syllable.'''
    out = 0
    
    for syll in flatten(outp, Syllable):
        unparsed_segs = syll[:]
        for seg in syll:
            for node in (syll.ons, syll.nuc, syll.cod):
                for nseg in node:
                    if seg is nseg:
                        unparsed_segs.remove(seg)
        rev_cod = syll.cod[:]
        rev_cod.reverse()
        for node in (syll.ons, syll.rev_cod):
            vio = False
            for seg in node:
                parsed = True
                for useg in unparsed_segs:
                    if useg is seg and vio:
                        out += 1
                        parsed = False
                if parsed:
                    vio = True
    return out

        
def MaxPriStressSeg(inp, outp):
    try:
        pwds = flatten(inp, ProsodicWord)
        segs = flatten(outp, Segment)
        indices = []

        for pwd in pwds:
            if hasattr(pwd, 'head'):
                head = pwd[pwd.head]
                head = head[head.head]
                assert type(head) is Syllable
                for seg in head:
                    indices.append(seg.indices.get('IO', None))

        for seg in segs:
            while seg.indices.get('IO', None) in indices:                
                indices.remove(seg.indices.get('IO', None))

    except:
        print outp.prettyPrint()
        raise
    return len(indices)

def __uniqueSegments(cand):
    indsDone = []
    segs = flatten(cand)
    out = []

    for seg in segs:
        if isinstance(seg, Segment) and not seg.indices in indsDone:
            out.append(seg)
            indsDone.append(seg.indices)
    return out

def __BR_faith(inp, outp, *args):
    o = outp
    if 'BR' in args:
        inp = Utterance()
        o = Utterance()
        for seg in flatten(outp):
            if seg.indices.get('BR', ' ')[0] == 'B':
                seg.indices['IO'] = int(seg.indices['BR'][1:])
                inp.append(seg)
            elif seg.indices.get('BR', ' ')[0] == 'R':
                seg.indices['IO'] = int(seg.indices['BR'][1:])
                o.append(seg)

    return (inp, o)

def __getReduplicant(cand, level=Syllable):
    out = Utterance()
    for unit in flatten(cand, level):
        if type(unit) is Segment:
            if seg.indices.get('BR', ' ')[0] == 'R':
                out.append(seg)
        elif type(unit) is Syllable:
            add = False
            for seg in unit:
                if seg.indices.get('BR', ' ')[0] == 'R':
                    add = True
            if add:
                addend = unit.copy()
                for seg in addend[:]:
                    if not seg.indices.get('BR', ' ')[0] == 'R':
                        addend.remove(seg)
                for sub in (addend.ons, addend.nuc, addend.cod):
                    for seg in sub:
                        if not seg in addend:
                            sub.remove(seg)
                out.append(addend)
        # This function is not finished!
    return out

# Con = []
# for name in locals().keys():
#     if eval('type(%s) == FunctionType' % name):
# ##        print name
#         if name == 'Ident':
#             for feat in features:
#                 Con.append((eval(name), feat))
#         else:
#             Con.append(eval(name))
