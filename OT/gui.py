#!/usr/bin/env python
import wx, OT, sys, os, pickle, inspect, re
from OT import config, constraints
from OT import representations as reps
from OT.output import unicode as ucso
from OT.output.common import TreeGridder
from wx.gizmos import TreeListCtrl, EditableListBox
try:
    import treemixin 
except ImportError:
    from wx.lib.mixins import treemixin
from wx.lib.mixins.listctrl import ListCtrlAutoWidthMixin
from wx.lib import ogl

def bestUnicodeFont():
    ps = int(wx.NORMAL_FONT.GetPointSize())
    out = wx.Font(ps, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
    if 'wxMSW' in wx.PlatformInfo:
        out.SetPointSize(ps * 1.2)
        for face in ('Charis SIL', 'FreeSans', 'Doulos SIL', 'Cardo', 'FreeSerif'):
            if out.SetFaceName(face):
                return out
    return out

def constraintSchemaTest(func):
    return (inspect.isfunction(func)
            and (inspect.getmodule(func) == constraints)
            and (not (func.__name__.startswith('_')))
            and (not (func.__name__ == 'mode')))

class batonWrapper:
    def __init__(self, prompt, endmsg='done', 
                 end=0, enable=False, **kwargs):
        self.prompt = prompt
        self.end = end
        self.stream = True
        self.parent = kwargs['parent']
    
    def __enter__(self):
        self.dialog = wx.ProgressDialog(self.prompt, self.prompt, maximum=self.end, 
                                        parent=self.parent)
        self.count = 0

    def message(self, msg, newline=True):
        self.msg = msg
        self.dialog.Update(self.count, msg)

    def twirl(self, ch=None):
        if self.end:
            self.count += 1
            self.dialog.Update(self.count)
        else:
            self.dialog.Pulse()

    def __exit__(self, *args):
        self.dialog.Destroy()

#-------------------------------------------------------------------------------

class treeDialog(wx.Dialog):
    def __init__(self, parent, ID, title='View input...', 
                 size=wx.DefaultSize, pos=wx.DefaultPosition, 
                 style=(wx.RESIZE_BORDER | wx.CAPTION | wx.CLOSE_BOX | 
                        wx.MAXIMIZE_BOX | wx.SYSTEM_MENU), 
                 utt=None):
        wx.Dialog.__init__(self, parent, ID, title, pos, size, style)
        self.utt = utt
        self.initialize()

    def initialize(self):
        ogl.OGLInitialize()

        btnsizer = wx.StdDialogButtonSizer()
        btn = wx.Button(self, wx.ID_OK)
        btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btnsizer.AddButton(btn)
        btnsizer.Realize()        

        tree = treeCanvas(self, self.utt)
        
        sizer = wx.GridBagSizer()

        sizer.Add(tree,
                  (0,0),
                  (1,1),
                  wx.EXPAND)
        sizer.Add(btnsizer,
                  (1,0),
                  (1,1))

        sizer.AddGrowableRow(0)
        sizer.AddGrowableCol(0)
        self.SetSizerAndFit(sizer)

#-------------------------------------------------------------------------------

class treeCanvas(ogl.ShapeCanvas):
    def __init__(self, parent, utt):
        ogl.ShapeCanvas.__init__(self, parent)
        self.gridder = TreeGridder(utt)

#        self.SetBackgroundColour( "LIGHT BLUE" )
        self.diagram = ogl.Diagram()
        self.SetDiagram(self.diagram)
        self.diagram.SetCanvas(self)

        self.font = bestUnicodeFont()
        ps = int(self.font.GetPointSize() * 1.2)
        self.font.SetPointSize(ps)

        self.shapes = []
        self.placeObjects()
        self.drawLines()
        self.diagram.ShowAll(1)

    def getShape(self, obj):
        if type(obj) is tuple:
            for (o, shape) in self.shapes:
                if type(o) is tuple and len(obj) == len(o):
                    found = True
                    for idx in range(len(obj)):
                        if obj[idx] is not o[idx]:
                            found = False
                    if found:
                        return shape
        else:
            for (o, shape) in self.shapes:
                if o is obj:
                    return shape
            for (o, shape) in self.shapes:
                if o == obj:
                    return shape
        raise KeyError, obj

    def drawLines(self):
        for line in self.gridder.lines:
            fromshape = self.getShape(line[0])
            toshape = self.getShape(line[1])

            line = ogl.LineShape()
            line.SetCanvas(self)
            line.SetPen(wx.BLACK_PEN)
            line.SetBrush(wx.BLACK_BRUSH)
            line.MakeLineControlPoints(2)
            fromshape.AddLine(line, toshape)
            self.AddShape(line)

    def placeObjects(self, width=1000, height=1000):
        row_height = 70
        col_width = 40

        row_coord = 20
        for row in self.gridder.rows:
            col_coord = 0
            for col in row:
                if col is not None:
                    self.addObject(col, col_coord, row_coord)
                col_coord += col_width
            row_coord += row_height

    def addObject(self, obj, x, y):
        shape = ogl.TextShape(30, 30)
        self.shapes.append([obj, shape])
        shape.SetCanvas(self)
        shape.SetX(x)
        shape.SetY(y)
        shape.SetPen(wx.BLACK_PEN)
        shape.SetBrush(wx.BLACK_BRUSH)
        shape.SetFont(self.font)
        if type(obj) in (reps.Segment, reps.Morpheme, reps.Reduplicant):
            shape.AddText(obj.prettyPrint('ipa'))
        elif type(obj) is tuple:
            if obj[0] == 'mora':
                shape.AddText(u'\u00b5')
            else:
                shape.AddText(obj[0])
        else:
            shape.AddText(ucso.symbols[type(obj)])
        self.AddShape(shape)
        shape.Show(True)
        return shape

#-------------------------------------------------------------------------------

class rankingBox(wx.ListCtrl, ListCtrlAutoWidthMixin):
    def __init__(self, parent):
        wx.ListCtrl.__init__(self, parent, -1, size=(200,200),
                             style=(wx.LC_REPORT | 
                                    wx.LC_NO_HEADER |
                                    wx.LC_SINGLE_SEL | 
                                    wx.LC_VIRTUAL |
                                    wx.LC_EDIT_LABELS))

        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.tryToEditItem)

        self.InsertColumn(0, '')
        self.setResizeColumn(0)
        self.resizeLastColumn(0)

    def tryToEditItem(self, event):
        item_idx = event.m_itemIndex
        self.EditLabel(item_idx)

    def setRanking(self, ranking=[]):
        self.ranking = ranking
        self.rankingUpdated()

    def rankingUpdated(self):
        self.SetItemCount(len(self.ranking) + 1)
        self.RefreshItems(0, len(self.ranking))
    
    def OnGetItemText(self, item, col=0):
        string = u''
        if item < len(self.ranking):
            constr = self.ranking[item]
            try:
                string = ucso.nameConstraint(constr)
            except:
                string = constr[0].__name__
                for parm in constr[1:]:
                    string += ', '
                    string += parm
        return string

    def remConstraint(self, event, index=None):
        if index is None:
            index = self.getSelectedIndex()
        if index < len(self.ranking):
            self.ranking.pop(index)
            self.rankingUpdated()

    def moveConstraintUp(self, event, index=None):
        if index is None:
            index = self.getSelectedIndex()
        if index < len(self.ranking):
            constr = self.ranking.pop(index)
            self.SetItemState(index, 0, wx.LIST_STATE_SELECTED)
            index -= 1
            self.ranking.insert(index, constr)
            self.rankingUpdated()
            self.SetItemState(index, wx.LIST_STATE_SELECTED, 
                              wx.LIST_STATE_SELECTED)
        
    def moveConstraintDown(self, event, index=None):
        if index is None:
            index = self.getSelectedIndex()
        if index < len(self.ranking):
            constr = self.ranking.pop(index)
            self.SetItemState(index, 0, wx.LIST_STATE_SELECTED)
            index += 1
            self.ranking.insert(index, constr)
            self.rankingUpdated()
            self.SetItemState(index, wx.LIST_STATE_SELECTED, 
                              wx.LIST_STATE_SELECTED)

    def clearRanking(self, event):
        del self.ranking[:]
        self.rankingUpdated()

    def getSelectedIndex(self):
        return self.GetNextItem(-1, wx.LIST_NEXT_ALL, wx.LIST_STATE_SELECTED)

#-------------------------------------------------------------------------------

class rankingDialog(wx.Dialog):
    def __init__(self, parent, ID, title='Modify ranking...', 
                 size=wx.DefaultSize, pos=wx.DefaultPosition, 
                 style=(wx.RESIZE_BORDER | wx.CAPTION | wx.CLOSE_BOX | 
                        wx.MAXIMIZE_BOX | wx.SYSTEM_MENU), 
                 ranking=()):
        wx.Dialog.__init__(self, parent, ID, title, pos, size, style)
        self.inRanking = ranking
        self.outRanking = list(self.inRanking[:])
        self.initialize()

    def initConstraintSchemas(self):
        self.schemas = inspect.getmembers(constraints, constraintSchemaTest)

    def updateRankingBox(self, ranking=None):
        if ranking is None:
            ranking = self.outRanking

        strings = []
        for constr in ranking:
            try:
                strings.append(ucso.nameConstraint(constr))
            except:
                string = constr[0].__name__
                for parm in constr[1:]:
                    string += ', '
                    string += parm
                strings.append(string)
        self.rankingBox.SetStrings(strings)

    def initialize(self):
        self.initConstraintSchemas()

        btnsizer = wx.StdDialogButtonSizer()
        btn = wx.Button(self, wx.ID_OK)
        btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btnsizer.AddButton(btn)
        btnsizer.Realize()        

        sizer = wx.GridBagSizer()

        choices = [schema[0] for schema in self.schemas]
        self.schemaBoxLabel = wx.StaticText(self, -1, 'Constraint schemas:')
        self.schemaBox = wx.ListBox(self, -1, 
                                    choices=choices, 
                                    style=wx.LB_SINGLE)
        
        self.rankingBoxLabel = wx.StaticText(self, -1, 'Ranking:')
        self.rankingBox = rankingBox(self)

#        self.rankingBox.Bind(wx.EVT_LIST_END_LABEL_EDIT, self.ef)

        self.addSchemaButton = wx.Button(self, wx.ID_ADD)
        self.remConstraintButton = wx.Button(self, wx.ID_REMOVE)
        self.clrRankingButton = wx.Button(self, wx.ID_CLEAR)
        self.constraintUpButton = wx.Button(self, wx.ID_UP)
        self.constraintDownButton = wx.Button(self, wx.ID_DOWN)

        self.schemaBox.Bind(wx.EVT_LISTBOX_DCLICK, self.addSchemaToRanking)
        self.addSchemaButton.Bind(wx.EVT_BUTTON, self.addSchemaToRanking)
        self.clrRankingButton.Bind(wx.EVT_BUTTON, self.rankingBox.clearRanking)
        self.remConstraintButton.Bind(wx.EVT_BUTTON, 
                                      self.rankingBox.remConstraint)
        self.constraintUpButton.Bind(wx.EVT_BUTTON, 
                                     self.rankingBox.moveConstraintUp)
        self.constraintDownButton.Bind(wx.EVT_BUTTON, 
                                     self.rankingBox.moveConstraintDown)
        self.rankingBox.Bind(wx.EVT_LIST_END_LABEL_EDIT, self.parseEdit)

        sizer.Add(self.schemaBoxLabel,
                  (0,0),
                  (1,1))
        sizer.Add(self.schemaBox,
                  (1,0),
                  (4,1),
                  wx.EXPAND)

        sizer.Add(self.rankingBoxLabel,
                  (0,2),
                  (1,1),
                  wx.EXPAND)
        sizer.Add(self.rankingBox,
                  (1,2),
                  (4,3),
                  wx.EXPAND)

        sizer.Add(self.addSchemaButton,
                  (1,1),
                  (1,1))
        sizer.Add(self.remConstraintButton,
                  (3,1),
                  (1,1))
        sizer.Add(self.clrRankingButton,
                  (4,1),
                  (1,1))
        sizer.Add(self.constraintUpButton,
                  (5,2),
                  (1,1))
        sizer.Add(self.constraintDownButton,
                  (5,3),
                  (1,1))

        sizer.Add(btnsizer,
                  (5,0),
                  (1,1),
                  wx.EXPAND)

        sizer.AddGrowableRow(1)
        sizer.AddGrowableCol(4)
        self.SetSizerAndFit(sizer)

        self.rankingBox.setRanking(self.outRanking)

    def parseEdit(self, event):
        idx = event.GetIndex()
        text = event.GetText()
        constr_text = splitToParms(text)
        constr = []
        for schema in self.schemas:
            if constr_text[0].strip(''''"''') == schema[0]:
                constr.append(schema[1])
                constr_text.pop(0)
                break
        if not constr:
            if idx < len(self.outRanking):
                constr.append(self.outRanking[idx][0])
            else:
                event.Veto()
                return

        for parm in constr_text:
            if type(parm) is tuple:
                constr.append(parm)
            elif parm.strip(''''"[]''') in reps.features:
                constr.append(reps.features[parm.strip(''''"[]''')])
            elif parm.strip(''''"[]''') in reps.features.values():
                constr.append(parm.strip(''''"[]'''))
            elif parm.strip(''''"''') in constraints.modes:
                constr.append(parm)
            elif parm.strip(''''"''') in ('L', 'R', 'ons', 'nuc', 'cod'):
                constr.append(parm)
            else:
                rt = repType(parm)
                if rt is not None:
                    constr.append(rt)
                else:
                    try:
                        constr.append(int(parm))
                    except:
                        event.Veto()
                        return

        constr = tuple(constr)
        if idx < len(self.outRanking):
            self.outRanking[idx] = constr
        else:
            self.outRanking.append(constr)
        self.rankingBox.rankingUpdated()

    def addSchemaToRanking(self, event, schema_idx=None):
        if schema_idx is None:
            schema_idx = self.schemaBox.GetSelections()[0]
        self.rankingBox.ranking.append((self.schemas[schema_idx][1],))
        self.rankingBox.rankingUpdated()

class candsModel(object):
    def __init__(self, grammar):
        self.g = grammar

    def indexClass(self, indices, offset=None):
        if offset is None:
            offset = self.computeOffset()

        if len(indices) > 1:
            return 'CAND'
        elif indices[0] < len(offset[0]):
            return 'DOPT'
        elif indices[0] < (len(offset[0]) + len(offset[1])):
            return 'WINR'
        return 'CAND'

    def computeOffset(self):
        offset = [[], []]

        if self.g.dopt:
            offset[0].append(self.g.dopt)

        for cand in self.g.winners:
            if not cand in offset:
                offset[1].append(cand)

        return tuple(offset)

    def isWinner(self, cand):
        for wincand in self.g.winners:
            if wincand == cand:
                return True
        return False

    def selectSigil(self, indices, offset=None):
        if offset is None:
            offset = self.computeOffset()
        cand = self.getCandidate(indices, offset)
        ucs = (sys.platform != 'win32')
        if (cand in self.g.winners) and (self.g.dopt == {}):
            sigil = 0                         # FIST
        elif (cand in self.g.winners) and (cand is self.g.dopt):
            sigil = 0                         # FIST
        elif cand in self.g.winners:
            sigil = 1                         # BOMB
        elif cand is self.g.dopt:
            sigil = 2                         # FACE
        else:
            sigil = 3                         # TILDE
        return sigil

    def cellContents(self, indices, column=0):
        offset = self.computeOffset()
        cand = self.getCandidate(indices, offset)
        cc = self.indexClass(indices, offset)
        if column == 0:
            return cand['tc']
        else:
            vio_idx = column - 1
            if vio_idx >= len(cand['vio']):
                return ''
            if cc == 'WINR' and not offset[0]:
                return unicode(cand['vio'][vio_idx])
            elif cc == 'DOPT':
                return unicode(cand['vio'][vio_idx])
            else:
                vios = ucso.subscript(cand['vio'][vio_idx])
                target = self.getCandidate((0,), offset)
                if target['vio'][vio_idx] < cand['vio'][vio_idx]:
                    return vios + 'W'
                elif target['vio'][vio_idx] > cand['vio'][vio_idx]:
                    return vios + 'L'
                else:
                    return vios + u'\u2014' # EM DASH

    def getCandidate(self, indices, offset=None):
        if offset is None:
            offset = self.computeOffset()
        offset_len = len(offset[0]) + len(offset[1])
        start = indices[0]
        if start < len(offset[0]):
            return offset[0][start]
        start -= len(offset[0])
        if start < len(offset[1]):
            return offset[1][start]
        start -= len(offset[1])
        assert len(indices) <= len(self.g.cands)
        idx = 0
        for cand in self.g.cands[0]:
            if ((cand is self.g.dopt or
                 cand in self.g.winners) and
                (not self.g.hasChildren(cand))):
                pass
            elif idx == start:
                break
            else:
                idx += 1
        for idx in indices[1:]:
            children = self.g.getChildren(cand)
            cidx = 0
            for child in children:
                if ((child is self.g.dopt or
                     child in self.g.winners) and
                    (not self.g.hasChildren(child))):
                    pass
                elif cidx == idx:
                    cand = child
                    break
                else:
                    cidx += 1
                
        return cand

    def numberOfChildren(self, indices):
        offset = self.computeOffset()
        assert len(indices) <= len(self.g.cands)
        out = 0
        if indices == ():
            children = self.g.cands[0]
            out += len(offset[0])
            out += len(offset[1])
        elif indices[0] < (len(offset[0]) + len(offset[1])):
            return 0
        else:
            cand = self.getCandidate(indices)
            children = self.g.getChildren(cand)
        for child in children:
            if (child == self.g.dopt or
                child in self.g.winners):
                if self.g.hasChildren(child):
                    out += 1
            else:
                out += 1
        return out

class TreeTableau(treemixin.VirtualTree, treemixin.DragAndDrop,
                  treemixin.ExpansionState, TreeListCtrl):
    def __init__(self, *args, **kwargs):
        kwargs['style'] = wx.TR_ROW_LINES | wx.TR_TWIST_BUTTONS | wx.TR_FULL_ROW_HIGHLIGHT
        self.setGrammar(kwargs['grammar'])
        del kwargs['grammar']
        super(TreeTableau, self).__init__(*args, **kwargs)
        if hasattr(self.grammar, 'inp'):
            self.AddColumn('/%s/' % self.grammar.inp.prettyPrint())
        else:
            self.AddColumn('//')
        for nul in range(100):
            self.AddColumn('')
        self.startExpanded = []
        self.createImageList()

    def createImageList(self):
        size = (20, 20)
        self.imageList = wx.ImageList(*size)
        for fn in ('fist.png', 'bomb.png', 'face.png', 'tilde.png'):
            absp = config.get_file_path(fn)
            img = wx.Image(absp, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
            self.imageList.Add(img)
        self.AssignImageList(self.imageList)

    def setGrammar(self, grammar):
        self.grammar = grammar
        self.model = candsModel(self.grammar)

    def OnGetChildrenCount(self, index):
        out = self.model.numberOfChildren(index)
        return out

    def OnGetItemText(self, index, column=0):
        out = self.model.cellContents(index, column)
        return out

    def OnGetItemImage(self, indices, which, column=0):
        if column == 0:
            out = self.model.selectSigil(indices)
        else:
            out = super(TreeTableau, self).OnGetItemImage(indices, which, column)
           
        return out

    # def OnGetItemBackgroundColour(self, indices):
    #     cand = self.model.getCandidate(indices)
    #     if (self.model.indexClass(indices) == 'DOPT'
    #         and not self.model.isWinner(cand)):
    #         return wx.RED
    #     elif self.model.isWinner(cand):
    #         return wx.GREEN
    #     else:
    #         return super(TreeTableau, self).OnGetItemBackgroundColour(indices)

    def OnGetItemTextColour(self, indices):
        target = self.model.getCandidate((0,))
        cand = self.model.getCandidate(indices)
        if (self.grammar.climbHarmCompare(cand, target) and 
            self.grammar.hasChildren(cand)):
            self.startExpanded.append(indices)
            return wx.LIGHT_GREY
        else:
            return super(TreeTableau, self).OnGetItemTextColour(indices)

    def OnDrop(self, dropTarget, dragItem):
        dropIndex = self.GetIndexOfItem(dropTarget)
        dragIndex = self.GetIndexOfItem(dragItem)
        dragCand = self.model.getCandidate(dragIndex)
        direction = self.dragDirection(dropIndex, dragIndex)

        if (dragCand == self.grammar.dopt and 
            direction == 'DOWN'):
            self.grammar.dopt = {}
        elif (dragCand != self.grammar.dopt and
              direction == 'UP'):
            self.grammar.dopt = dragCand

        self.RefreshItems()

    def dragDirection(self, dropIndex, dragIndex):
        comparands = min(len(dropIndex), len(dragIndex))
        for idx in range(comparands):
            if dropIndex[idx] < dragIndex[idx]:
                return 'UP'
            elif dropIndex[idx] > dragIndex[idx]:
                return 'DOWN'

    def redrawColumns(self):
        self.SetColumnText(0, '/%s/' % self.grammar.inp.prettyPrint())
        for idx in range(1, 100):
            cidx = idx - 1
            if cidx < len(self.grammar.ranking):
                self.SetColumnText(idx, ucso.nameConstraint(self.grammar.ranking[cidx]))
            else:
                self.SetColumnText(idx, '')
            idx += 1

    def RefreshItems(self):
        self.redrawColumns()
        super(TreeTableau, self).RefreshItems()
        for indices in self.startExpanded:
            item = self.GetItemByIndex(indices)
            self.Expand(item)
        self.startExpanded = []

class GrammarGUI(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title)
        self.parent = parent
        self.history = []
        self.initialize()

    def initialize(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.grammarPanel = GrammarPanel(self)
        sizer.Add(self.grammarPanel, 1, wx.EXPAND)
        self.SetSizer(sizer)
        self.SetAutoLayout(True)
        self.Show(True)
        
class GrammarPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1)
        self.parent = parent
        self.history = []
        self.initialize()

    def loadState(self):
        self.state = {'rankings': [],
                      'ranking': None,
                      'inputs': [],
                      'input_idx': None,
                      'expansion': None,
                      'grammar': self.grammar}
        for dr in config.paths:
            try:
                absp = os.path.join(dr, 'gui_state')
                with open(absp, 'rb') as f:
                    self.state.update(pickle.load(f))
            except IOError:
                pass
        return self.state

    def saveState(self):
        for dr in config.paths:
            try:
                absp = os.path.join(dr, 'gui_state')
                with open(absp, 'wb') as f:
                    pickle.dump(self.state, f)
                break
            except:
                continue

    def initialize(self):
        widgetFont = bestUnicodeFont()
        self.grammar = OT.Grammar()
        sizer = wx.GridBagSizer()

        self.loadState()

        self.goButton = wx.Button(self, -1, 'Evaluate')
        self.inputSelectorLabel = wx.StaticText(self, -1, label='Input:')
        self.inputSelector = wx.ComboBox(self, -1, style=(wx.CB_DROPDOWN
                                                          | wx.TE_PROCESS_ENTER))
        self.rankingSelectorLabel = wx.StaticText(self, -1, label='Ranking:')
        self.rankingSelector = wx.Choice(self, -1, size=(200,-1))

        self.levelSelectorLabel = wx.StaticText(self, -1, label='Level:')
        self.levelSelector = wx.Choice(self, -1, choices=[lvl.__name__ for lvl in reps.levels])

        self.tableau = TreeTableau(self, -1, grammar=self.grammar)

        if widgetFont is not wx.NORMAL_FONT:
            for widget in (self.inputSelector, self.rankingSelector, self.tableau):
                widget.SetFont(widgetFont)

        self.goButton.Bind(wx.EVT_BUTTON, self.go)
        self.inputSelector.Bind(wx.EVT_TEXT_ENTER, self.newInputSelected)
        self.inputSelector.Bind(wx.EVT_COMBOBOX, self.newInputSelected)
        self.rankingSelector.Bind(wx.EVT_CHOICE, self.rankingSelected)
        self.tableau.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.viewCandidate)
        self.tableau.Bind(wx.EVT_LIST_COL_CLICK, self.viewInput)

        self.Centre(wx.BOTH)
        
        sizer.Add(self.inputSelectorLabel,
                  (0,0),
                  (1,1),
                  wx.EXPAND)
        sizer.Add(self.inputSelector,
                  (0,1),
                  (1,1))
        sizer.Add(self.rankingSelectorLabel,
                  (0,2),
                  (1,1))
        sizer.Add(self.rankingSelector,
                  (0,3),
                  (1,1),
                  wx.EXPAND)

        sizer.Add(self.tableau,
                  (1,0),
                  (1,4),
                  wx.EXPAND)

        sizer.Add(self.goButton,
                  (2,2),
                  (1,2),
                  wx.EXPAND)
        sizer.Add(self.levelSelectorLabel,
                  (2,0),
                  (1,1))
        sizer.Add(self.levelSelector,
                  (2,1),
                  (1,1))

        sizer.AddGrowableCol(3)
        sizer.AddGrowableRow(1)

        self.syncToState()
        self.SetSizerAndFit(sizer)

    def viewInput(self, event):
        # if event.m_col == 0:
        if True:
            i_dialog = treeDialog(self, -1, title='View input...', utt=self.grammar.inp)
            i_dialog.Show()

    def viewCandidate(self, event):
        indices = self.tableau.GetIndexOfItem(event.GetItem())
        cand = self.tableau.model.getCandidate(indices)

        if 'form' in cand:
            utt = cand['form']
        else:
            utt = self.grammar.getCandidate(cand['key'])

        c_dialog = treeDialog(self, -1, title='View candidate...', utt=utt)
        c_dialog.Show()

    def rankingSelected(self, event):
        ranking_idx = event.GetInt()
        if ranking_idx < len(self.state['rankings']):
            self.state['ranking'] = self.state['rankings'][ranking_idx]
        else:                  
            r_dialog = rankingDialog(self, -1, ranking=self.grammar.ranking)
            retval = r_dialog.ShowModal()
            if retval == wx.ID_OK:
                self.state['ranking'] = tuple(r_dialog.outRanking)
                if not self.state['ranking'] in self.state['rankings']:
                    self.state['rankings'].append(self.state['ranking'])

        self.syncToState()
        self.syncToGrammar()
        
    def newInputSelected(self, event):
        trans = self.inputSelector.GetValue().strip('/')
        if trans == 'Modify input...':
            self.viewInput(event)
        else:
            mstate = {'inputs': self.state['inputs'],
                      'input_idx': None}
            idx = 0
            for inp in mstate['inputs']:
                if trans == inp.prettyPrint():
                    mstate['input'] = inp
                    mstate['input_idx'] = idx
                idx += 1
            if mstate['input_idx'] is None:
                mstate['inputs'].append(OT.Utterance(trans))
                mstate['input_idx'] = (len(mstate['inputs']) - 1)
            self.state.update(mstate)

            self.syncToState()
            self.syncToGrammar()

    def representRanking(self, ranking=None):
        if ranking is None:
            ranking = self.grammar.ranking
        out = ucso.nameConstraint(ranking[0])
        for constr in ranking[1:]:
            if sys.platform != 'win32':
                out += u' \u226B '
            else:
                out += ' >> '
            out += ucso.nameConstraint(constr)
        if not self.rankingSelector.SetStringSelection(out):
            idx = self.rankingSelector.Append(out)
            self.rankingSelector.SetSelection(idx)
        return out

    def go(self, event):
        if self.nextLevel == OT.Segment:
            with batonWrapper('Evaluating...', 'done', parent=self.parent) as baton:
                self.grammar.cands = self.grammar.seedCandsTable(script=None, baton=baton)
        else:
            self.grammar.level = self.nextLevel
            self.grammar.composeMenu()
            with batonWrapper('Evaluating...', 'done', parent=self.parent) as baton:
                self.grammar.cands = self.grammar.buildStructure(baton=baton)
                self.grammar.pickWinners(baton=baton)
        self.syncToGrammar()

    def pickLevelForButton(self):
        if not self.grammar.cands:
            self.goButton.Enable()
            self.nextLevel = OT.Segment
        else:
            level_idx = OT.levels.index(self.grammar.level)
            if level_idx < (len(OT.levels) - 1):
                self.goButton.Enable()
                self.nextLevel = OT.levels[level_idx + 1]
            else:
                self.goButton.Disable()
        self.levelSelector.SetSelection(OT.levels.index(self.nextLevel))

    def syncToGrammar(self):
        self.pickLevelForButton()
        ppi = '/%s/' % self.grammar.inp.prettyPrint()
        if not self.inputSelector.SetStringSelection(ppi):
            idx = self.inputSelector.Append(ppi)
            self.inputSelector.SetSelection(idx)
        self.representRanking()
        if self.grammar.cands is not None:
            self.state['expansion'] = self.tableau.GetExpansionState()
            self.tableau.RefreshItems()
        
    def syncToState(self):
        self.rankingSelector.Clear()
        for ranking in self.state['rankings']:
            self.representRanking(ranking)
        self.rankingSelector.Append('Modify ranking...')
        if self.state['ranking'] is None:
            self.rankingSelector.SetSelection(len(self.state['rankings']))
        else:
            self.rankingSelector.SetSelection(self.state['rankings'].index(self.state['ranking']))

        self.inputSelector.Clear()
        for inp in self.state['inputs']:
            self.inputSelector.Append('/%s/' % inp.prettyPrint())
        self.inputSelector.Append('Modify input...')
        if self.state['input_idx'] is None:
            self.inputSelector.SetSelection(len(self.state['inputs']))
        else:
            self.inputSelector.SetSelection(self.state['input_idx'])

        self.grammar = self.state['grammar']
        self.tableau.setGrammar(self.grammar)
        if self.state['input_idx'] is not None:
            if getattr(self.grammar, 'inp', None) != self.state['inputs'][self.state['input_idx']]:
                self.grammar.inp = self.state['inputs'][self.state['input_idx']]
                self.grammar.cands = None
                self.grammar.level = OT.Segment
        if self.state['ranking']:
            if self.grammar.ranking != self.state['ranking']:
                self.grammar.ranking = self.state['ranking']
                self.grammar.cands = None
                self.grammar.level = OT.Segment
        if self.grammar.cands is not None:
            self.tableau.RefreshItems()
            if self.state['expansion'] is not None:
                self.tableau.SetExpansionState(self.state['expansion'])

        self.pickLevelForButton()

def splitToParms(text):
    tuple_rex = re.compile(r'(\([^)]*\)?)')
    parms = []
    
    stoff = ''
    while text:
        mat = tuple_rex.match(text)
        if mat is None:
            stoff += text[0]
            text = text[1:]
        else:
            tuple_found = True
            for elem in stoff.split(','):
                parm = elem.strip()
                if parm:
                    parms.append(parm)
            stoff = ''
            try:
                tup = eval(mat.group(0))
                for feat in tup:
                    if not feat.strip(''''"[]''') in reps.features.values():
                        tuple_found = False
                        break
            except:
                tuple_found = False
            if tuple_found:
                parms.append(tup)
            else:
                stoff += mat.group(0)
            text = text[len(mat.group(0)):]
    if stoff:
        for elem in stoff.split(','):
            parm = elem.strip()
            if parm:
                parms.append(parm)
    return parms

def repType(parm):
    for rep in reps.levels:
        if rep.__name__ == parm:
            return rep

def main():
    app = wx.App()
    frame = GrammarGUI(None, -1, 'PyOT')
    app.MainLoop()
    return 0

if __name__ == '__main__':
    sys.exit(main())
