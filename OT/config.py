#!/usr/bin/env python
from __future__ import with_statement

try:
    import simplejson as json
except ImportError:
    import json
import os, OT, locale
from os import path

paths=(path.dirname(OT.__file__),
       '/etc/PyOT', 
       path.normpath(path.expanduser('~/../All Users/Application Data/PyOT')),
       path.expanduser('~/.PyOT'),
       path.expanduser('~/Application Data/PyOT'),
       '.')

ipas={'ipa': {},
      'tipa': {},
      'xsampa': {}}

lex_ipas={'ipa': {},
          'tipa': {},
          'xsampa': {}}

def select_term_scheme():
    locale.setlocale(locale.LC_ALL)
    code = locale.getpreferredencoding()

    try:
        test = u'\u02d0'.encode(code)
        return 'ipa'
    except:
        return 'xsampa'

def read_if_present(fn):
    '''Returns a dict read from the json in the file named fn.  If
    there is no such file, or another error occurs, returns {}.'''
    try:
        with open(fn, 'rb') as f:
            return json.load(f)
    except IOError:
        return {}

def get_ipas(lang='latin', scheme='ipa', **kwargs):
    lang = kwargs.get('lang', lang)
    scheme = kwargs.get('scheme', scheme)
    lex = kwargs.get('lex', True)
    out = {}
    if lex and lex_ipas[scheme].has_key(lang):
        return lex_ipas[scheme][lang]
    elif ipas[scheme].has_key(lang):
        return ipas[scheme][lang]
    else:
        for dr in paths:
            if lex:
                out.update(read_if_present(path.join(dr, 'ipa', 
                                                     '%s.lex.%s' % (lang, scheme))))
            if not out:
                out.update(read_if_present(path.join(dr, 'ipa', 
                                                     '%s.%s' % (lang, scheme))))
    if out and lex:
        lex_ipas[scheme][lang] = out
        return out
    elif out:
        ipas[scheme][lang] = out
        return out

    raise IOError('Could not load any %s mappings for language %s' %
                  (scheme.upper(), lang)) 

def get_diacritics(scheme='ipa'):
    out = {}

    for dr in paths:
        out.update(read_if_present(path.join(dr, 'ipa', 
                                             'diacritics.%s' % scheme)))
    if out: return out

    raise IOError("Could not load diacritics for output scheme `%s'" % scheme) 

def get_file_path(fn, sd='output'):
    pth = None
    for dr in paths:
        test = path.join(dr, sd, fn)
        if path.exists(test) and path.isfile(test):
            pth = test
    
    if pth is None:
        raise Exception, fn
    return pth

def get_file(fn, sd='output', mode='rb'):
    absp = get_file_path(fn, sd)
    return open(absp, mode)
