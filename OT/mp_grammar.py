from gen_mp import Gen
from ui_util import Baton
from types import FunctionType, GeneratorType, MethodType

import gen, OT, multiprocessing

class Grammar(OT.Grammar):
    def __init__(self, **kwargs):
        OT.Grammar.__init__(self, **kwargs)

    def __enter__(self):
        self.pool = multiprocessing.Pool()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            self.pool.terminate()
        else:
            self.pool.close()
        return False

    def Eval(self, inp, **kwargs):
        self.parsekwargs(kwargs)

        self.violations = kwargs.get('violations', self.violations)
        self.tabCands = kwargs.get('tabCands', self.tabCands)
        dopts = kwargs.get('dopts', [])

        self.inp = inp
        self.winners = []

        popViols = []

        for constr in self.ranking:
            if not constr in self.violations:
                self.violations[constr] = []
                popViols.append(constr)

        idx = 0

        with Baton('Evaluating', 'done', self.numCands, self.verbose) as baton:
            if type(self.cands) in (FunctionType, MethodType):
                if self.numCands:
                    cands = self.cands(inp, self.ranking, **kwargs)
                else:
                    cands = self.cands(inp, self.ranking, baton=baton, **kwargs)
            else: cands = self.cands
            self.numCands = 0

            for cand in (cands or Gen(inp, self.ranking, 
                                      level=self.level, baton=baton,
                                      filter=self.filter, pool=self.pool)):


def lessHarmonic(inp, c1, c2, violations, ranking):
    '''Returns the least harmonic with respect to ranking of c1 and c2,
    or None if they are equally harmonic.'''
    for constr in ranking:
        v1 = violations[constr][1]
        v2 = violations[constr][2]

        if v1 is None:
            v1 = constr[0](inp, c1[0], *constr[1:])
        if v2 is None:
            v2 = constr[0](inp, c2[0], *constr[1:])

        if v1 < v2:
            return c2
        if v2 < v1:
            return c1
    return None
