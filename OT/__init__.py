#!/usr/bin/env python
from __future__ import with_statement

from representations import *
from constraints import *
from output import *
from gen import Gen
from ui_util import Baton
from types import FunctionType, GeneratorType, MethodType

import sys, output, gen, config, locale, ui_util

locale.setlocale(locale.LC_ALL)
code = locale.getpreferredencoding()

class Grammar():
    def __init__(self, **kwargs):
        self.verbose = True
        self.cands = None
        self.tabCands = []
        self.numCands = 0
        self.level = Foot
        self.menu = None
        self.tableau = 'comparative'
        self.ranking = ()
        self.scheme = config.select_term_scheme()
        self.filter = ()
        self.winners = []
        self.dopts = []
        self.dopt = {}
        self.lang = 'fallback'
        self.violations = {}
        self.makeTabCands = True
        self.parsekwargs(kwargs)
        self.script = None

    def guessIndices(self, inp, outp, ranking):
        isegs = flatten(inp, OT.Segment)
        segs = flatten(outp, OT.Segment)
        Max = []
        Dep = []

        for constr in ranking:
            if constr[0] == Max_C:
                Max.append('cons')
            elif constr[0] == Max_V:
                Max.append('voc')

        script = gen.invIdentScript(isegs, ranking)
        opos = 0
        for idx in range(len(script)):
            iviol = False
            for seg in script[idx]:
                if not (seg ^ segs[opos]):
                    segs[opos].index = isegs[idx].index
                    opos += 1
                    iviol = True
                    break
            if not iviol:
                mviol = False
                for feat in Max:
                    if feat in isegs[idx]:
                        mviol = True
                if not mviol:
                    break
            if opos == len(segs):
                break
        if not opos == len(segs): 
            return None
        return outp

    def tabCandsGen(self, inp, ranking, **kwargs):
        baton = kwargs.get('baton', None)
        verbose = kwargs.get('verbose', False)
        for cand in self.tabCands:
            o = self.guessIndices(inp, Utterance(cand), ranking)
            if o: 
                yield o
            elif verbose: 
                print >> sys.stderr, ("Warning: couldn't assign indices to cand %s, ignoring..."
                                      % cand)
            if baton: baton.twirl()

    def countCands(self, inp, **kwargs):
        '''Returns the number of candidates generated for the input inp
        and ranking, by gen.Gen if no cands argument is passed, or by
        cands if it is. (If cands is a sequence, this function is
        equivalent to len(cands)).'''
        self.parsekwargs(kwargs)

        out = 0

        with Baton('Counting candidates', 'done', None, self.verbose) as baton:
            if type(self.cands) is FunctionType:
                cands = self.cands(inp, self.ranking, 
                                   level=self.level, baton=baton)
            else: cands = self.cands

            for cand in (cands or 
                         Gen(inp, self.ranking, level=self.level, baton=baton)):
                out += 1
                baton.twirl()

        return out

    def winLoseOrDraw(self, constr, ind):
        if self.dopts:
            target = self.dopts[0][1]
        else:
            target = self.winners[0][1]

        if self.violations[constr][ind] > self.violations[constr][target]:
            wldash = 1
        elif self.violations[constr][ind] < self.violations[constr][target]:
            wldash = -1
        else: 
            wldash = 0
        return wldash

    def flattenStrata(self, strata):
        out = []
        for stratum in strata:
            for constr in stratum:
                out.append(constr)
        return out

    def RCD(self, **kwargs):
        strata = kwargs.get('strata', [])
        done = kwargs.get('done', [(self.dopts and self.dopts[0][1]
                                    or self.winners[0][1])])
        localdone = {}
        for constr in self.ranking:
            localdone[constr] = []

        freew = self.ranking[:]
        for constr in self.flattenStrata(strata):
            freew.remove(constr)

        for ind in range(len(self.violations[self.ranking[0]])):
            if not ind in done:
                for constr in self.ranking:
                    if (self.winLoseOrDraw(constr, ind) == -1 
                        and constr in freew):
                        freew.remove(constr)
                    elif (self.winLoseOrDraw(constr, ind) == 1 and 
                          constr in freew):
                        localdone[constr].append(ind)

        strata.append([])
        for constr in freew:
            done.extend(localdone[constr])
            strata[-1].append(constr)

        if not freew:
            for constraint in self.ranking:
                if not constraint in self.flattenStrata(strata):
                    strata[-1].append(constraint)
            if not strata[-1]: 
                strata = strata[:-1]
            else:
                print >> sys.stderr, 'Ranking is intractable.'
            return strata
        else:
            return self.RCD(strata=strata, done=done)

    def moreHarmonic(self, inp, c1, c2, **kwargs):
        '''Returns the most harmonic with respect to ranking of c1 and c2,
        or None if they are equally harmonic.'''
        ranking = kwargs.get('ranking', self.ranking)
        for constr in ranking:
            v1 = self.violations[constr][c1[1]]
            v2 = self.violations[constr][c2[1]]

            if v1 is None:
                v1 = constr[0](inp, c1[0], *constr[1:])
                self.violations[constr][c1[1]] = v1
            if v2 is None:
                v2 = constr[0](inp, c2[0], *constr[1:])
                self.violations[constr][c2[1]] = v1
               
            if v1 > v2:
                return c2
            if v2 > v1:
                return c1
        return None

    def writeTableau(self, mod=HTML, **kwargs):
        tableau = kwargs.get('tableau', 'comparative')
        skiphb = kwargs.get('skiphb', False)
        with Baton('Drawing tableau', 'done', None, kwargs.get('verbose', True)) as baton:
            mod.writeTableau(self, rows=self.tableauRows(tableau, skiphb, baton=baton), baton=baton, 
                             **kwargs)

    def getCandEntry(self, key):
        '''Returns the dictionary from the cands table representing
        the candidate with the key key.'''
        series = self.cands[len(key) - 1]
        for cand in series:
            if cand['key'] == key:
                return cand

    def getCandidate(self, key, menu=None, seg_menu=None, script=None):
        if menu is None:
            menu = self.menu
        if seg_menu is None:
            seg_menu = gen.segBuildMenu(self.inp, self.ranking)
        if script is None:
            script = self.script

        buildon = gen.getCandidate(key, menu, seg_menu, script)
            
        return buildon

    def seedCandsTable(self, **kwargs):
        baton = kwargs.get('baton', None)
        script = kwargs.get('script', self.script)
        filter = kwargs.get('filter', self.filter)

        if script is None:
            segs = flatten(self.inp)
            io_ranking = list(gen.distilRanking(self.ranking, 'IO'))
            cc_coindex = (CC_Corr in [c[0] for c in self.ranking]
                          and not gen.ccTETU(self.ranking))

            if baton:
                baton.message('Assembling segment script')
            script = gen.segVarScript(segs, io_ranking, 
                                      cc_coindex=cc_coindex,
                                      **kwargs)
            self.script = script

        if baton:
            baton.message(gen.prettyPrintScript(script))

        cands = [[]]
        tabCands = [[]]
        segvars = cands[-1]
        if baton:
            baton.message('Evaluating segmental variants')
            baton.twirl()
            baton.count = 0
            baton.percent = True
            baton.end = float(gen.countSegVars(script))

        seg_menu = gen.segBuildMenu(self.inp, self.ranking)
        seq = gen.segVariantByKey(script, baton=baton)

        for (segvar, key) in gen.menuRecurse(self.inp, seq, seg_menu, filt=filter, 
                                             baton=baton):
            segvars.append({'key': (key,),
                            'vio': []})
            for constr in self.ranking:
                segvars[-1]['vio'].append(constr[0](self.inp, segvar, *constr[1:]))
            segvars[-1]['tc'] = segvar.prettyPrint(self.scheme)
            if baton: baton.Update(baton.count)

        segvars = cands[0]
        segvars.sort(key=lambda d: d['vio'])
#        cands = self.ccTETUcheck(cands, filter=filter, baton=baton)
        return cands

    def ccTETUcheck(self, cands, filter, baton=None):
        segvars = cands[0]
        segvars.sort(key=lambda d: d['vio'])
        if gen.ccTETU(self.ranking):
            if baton:
                baton.message('Assigning CC indices...')
                baton.end = None
            maskedVio = maskCCViolations(segvars[0]['vio'], self.ranking)
            cands.append([])
            ccvars = cands[-1]

            for segvar_data in segvars:
                segvar = self.getCandidate(segvar_data['key'], menu=[])
                sv_vio = maskCCViolations(segvar_data['vio'], self.ranking)
                if sv_vio <= maskedVio:
                    for (cc_var, cc_idx) in gen.CCVariantByKey(segvar, self.ranking):
                        if gen.applyFilter(self.inp, cc_var, filter):
                            ccvars.append({'key': segvar_data['key'] + (cc_idx,),
                                           'vio': []})
                            for constr in self.ranking:
                                ccvars[-1]['vio'].append(constr[0](self.inp, cc_var, *constr[1:]))
                            ccvars[-1]['tc'] = cc_var.prettyPrint(self.scheme)
                        if baton:
                            baton.Update(baton.count)
                else:
                    break
            ccvars.sort(key=lambda d: d['vio'])
        return cands

    def composeMenu(self, level=None):
        if level is None: level = self.level

        menu = gen.composeMenu(level, self.ranking)
        self.menu = menu
        return menu

    def mostHarmonicNotDone(self, cands, tasks_done=[]):
        out = [None] * len(cands)

        for idx in range(len(cands) - 1):
            out[idx] = None
            for cand in cands[idx]:
                done = False
                if cand['key'] in tasks_done:
                    done = True
                else:
                    for ncand in cands[idx + 1]:
                        if ncand['key'][:-1] == cand['key']:
                            done = True
                            break
                if not done:
                    out[idx] = cand
                    break

        return out

    def harmonicityScores(self, mh_notdone, topend=None):
        some = []
        for cand in mh_notdone:
            if cand is not None:
                some.append(cand)
        if cand is not None:
            assert topend is not None
            some[-1] = topend
        elif topend is not None:
            some.append(topend)
        some.sort(key=lambda d: d['vio'])

        scores = []
        for cand in mh_notdone[:-1]:
            if cand is None:
                scores.append(None)
            else:
                scores.append(some.index(cand) + 1)
        return scores

    def climbHarmCompare(self, simple, comple):
        svio = []
        cvio = []

        for constr in self.ranking:
            idx = self.ranking.index(constr)
            if constr[0] == CC_Corr:
                svio.append(0)
                cvio.append(0)
            else:
                svio.append(simple['vio'][idx])
                cvio.append(comple['vio'][idx])

        return svio <= cvio

    def buildStructure(self, cands=None, **kwargs):
        if cands is None:
            cands = self.cands
        baton = kwargs.get('baton', None)
        filter = kwargs.get('filter', self.filter)
        done = kwargs.get('done', [])

        while len(cands) < (len(self.menu) + 1):
            cands.append([])
        next_job = None
        mh_notdone = self.mostHarmonicNotDone(cands)
        for nd in mh_notdone:
            if nd is not None:
                next_job = nd['key']
                break

        while next_job:
            todo = []
            highest = None
            task = next_job
            done.append(task)

            buildon = self.getCandidate(task)
            builder = self.menu[len(task) - 1]

            if baton:
                baton.message(gen.menuFunctionMessage(builder, buildon), False)
            nvs = builder[0](buildon, *builder[1:])

            newvar_idx = 0
            for nv in nvs:
                if type(nv) is tuple:
                    key = task + (nv[1],)
                    nv = nv[0]
                else:
                    key = task + (newvar_idx,)
                newvars = cands[len(key) - 1]
                if gen.applyFilter(self.inp, nv, filter):
                    newcand = {'key': key,
                               'vio': []}
                    for constr in self.ranking:
                        newcand['vio'].append(constr[0](self.inp, nv, *constr[1:]))
                    newcand['tc'] = nv.prettyPrint(self.scheme)
                    added = False
                    for idx in range(len(newvars)):
                        if newvars[idx]['vio'] > newcand['vio']:
                            newvars.insert(idx, newcand)
                            added = True
                            break
                    if not added:
                        newvars.append(newcand)
                newvar_idx += 1
                if baton: baton.Update(baton.count)

            mh_notdone = self.mostHarmonicNotDone(cands, done)
            last = None
            for idx in range(len(mh_notdone)):
                comple = None
                if cands[idx]:
                    comple = cands[idx][0]
                for bidx in range(idx):
                    simple = mh_notdone[bidx]
                    if simple is not None and comple is not None:
                        if self.climbHarmCompare(simple, comple):
                            if simple['key'] not in todo:
                                todo.append(simple['key'])
                    if simple is not None:
                        highest = simple
                if comple is None and highest is not None:
                    if highest['key'] not in todo:
                        todo.append(highest['key'])
            todo.sort(key=lambda x: len(x))
            if todo:
                next_job = todo[0]
            else:
                next_job = None
            # if baton:
            #     msg = 'Harmonicity cline:'
            #     for score in self.harmonicityScores(mh_notdone, comple):
            #         msg += ' '
            #         msg += str(score)
            #     baton.message(msg, True)
            
        return cands

    def smartGenEval(self, inp, **kwargs):
        verbose = kwargs.get('verbose', self.verbose)
        hint = kwargs.get('hint', None)
        with Baton('Evaluating', 'done', None, verbose) as baton:
            dopts = kwargs.get('dopts', self.dopts)
            filter = kwargs.get('filter', self.filter)
            level = kwargs.get('level', self.level)
            script = kwargs.get('script', self.script)

            self.inp = inp
	    # if inp.get_reduplicant():
	    # 	return self.Eval(inp, **kwargs)

            done = []
            if hint is None:
                cands = self.seedCandsTable(baton=baton, **kwargs)
                self.cands = cands
                script = self.script
            else:
                menu = self.composeMenu(level=hint)
                while (gen.CCVariantByKey, self.ranking) in menu:
                    menu.remove((gen.CCVariantByKey, self.ranking))
                (self.cands, self.menu, self.seg_menu, self.script) = \
                    Gen(inp, self.ranking, scheme=self.scheme,
                        filter=filter, menu=menu, baton=baton)
                cands = self.cands
                for series in cands[:-1]:
                    done.extend([d['key'] for d in series])
            menu = self.composeMenu(level=level)

            baton.message('Building higher-order structure')
            baton.end = None
            baton.twirl()

            cands = self.buildStructure(cands, done=done, baton=baton)
            
            self.winners = self.pickWinners(cands, baton=baton, verbose=verbose)
        self.cands = cands
        return self.winners

    def pickWinners(self, cands=None, **kwargs):
        baton = kwargs.get('baton', None)
        self.verbose = kwargs.get('verbose', self.verbose)

        if cands is None:
            cands = self.cands

        self.winners = []
        if cands[-1]:
            best = min(cands[-1], key=lambda d: d['vio'])
            for cand in cands[-1]:
                if cand['vio'] == best['vio']:
                    self.winners.append(cand)
        for cand in self.winners:
            cand['form'] = self.getCandidate(cand['key'])

        if self.verbose:
            scheme = config.select_term_scheme()
            if len(self.winners) is 1:                    
                msg = ("And the winner is: %s" % 
                       self.winners[0]['form'].prettyPrint(scheme))
            elif len(self.winners) > 1:
                msg = "And the winners are:\n"
                for winner in self.winners:
                    msg += ('\t%s\n' % 
                           winner['form'].prettyPrint(scheme))
                msg = msg.strip()
            else:
                msg = "No winners found."
            if baton is not None and baton.stream:
                baton.message(msg, newline=True)
            else:
                print >> sys.stderr, msg

        return self.winners

    def Eval(self, inp, ranking=None, **kwargs):
        '''If self.cands is defined, selects the optimal candidate(s)
        in self.cands[-1].  If self.cands is None, calls Gen to build
        a cands table.'''
        self.inp = inp
        if ranking is not None:
            self.ranking = ranking
        self.parsekwargs(kwargs)
        with Baton('Evaluating', 'done', None, self.verbose) as baton:
            if self.cands is None:
                (self.cands, self.menu, self.seg_menu, self.script) = \
                    Gen(inp, self.ranking, scheme=self.scheme, menu=self.menu,
                        filter=self.filter, level=self.level, baton=baton)
        
        return self.pickWinners()

    def clone(self, *omits):
        '''Returns a Grammar object which matches the initial state of
        self in every respect, except for the property names listed as
        positional arguments, which are set to the default.'''
        newparts = self.__dict__.copy()
        del newparts['cands']
        for omit in omits:
            del newparts[omit]
        return Grammar(**newparts)

    def parsekwargs(self, kwargs):
        '''Parses keyword arguments into a tuple (verbose, cands,
        numCands, level, kwargs), computing numCands if necessary.'''
        self.lang = kwargs.get('lang', self.lang)
        self.verbose = kwargs.get('verbose', self.verbose)
        self.cands = kwargs.get('cands', self.cands)
        self.numCands = kwargs.get('numCands', self.numCands)
        self.level = kwargs.get('level', self.level)
        self.ranking = kwargs.get('ranking', self.ranking)
        self.filter = kwargs.get('filter', self.filter)
        self.scheme = kwargs.get('scheme', self.scheme)

    def computeViolations(self, inp, **kwargs):
        '''Returns a dict of lists x such that x[{constraint}][{candidate
        number}] is an integer recording the number of violations of that
        constraint for that numbered candidate.'''
        self.parsekwargs(kwargs)

        out = {}

        for constr in self.ranking:
            out[constr] = []

        if type(cands) is FunctionType:
            itover = cands(inp, self.ranking, **kwargs)
        else:
            itover = (cands or Gen(inp, self.ranking, **kwargs))

        with Baton('Computing violations for tableau...',
                   'done',
                   self.numCands,
                   verbose) as baton:
            for cand in itover:
                for constr in self.ranking:
                    out[constr].append((constr[0](inp, cand, constr[1:])))
                baton.twirl()
        return out

    def violationsForCand(self, ci, **kwargs):
        '''Returns a list of violations for the candidate numbered ci,
        in the same order as self.ranking.'''
        if self is not None:
            ranking = self.ranking
            violations = self.violations
        elif (kwargs.has_key('ranking') and kwargs.has_key('violations')):
            ranking = kwargs['ranking']
            violations = kwargs['violations']
        else:
            raise TypeError
            
        out = []
        for constr in ranking:
            out.append(violations[constr][ci])
        return out

    @staticmethod
    def harmonicBounding(c1, c2):
        '''If the candidates c1 and c2 are in a harmonic bounding
        relationship, returns the most harmonic of them.  Otherwise,
        returns None.'''
        c1viols = c1['vio']
        c2viols = c2['vio']
        
        retc1 = True
        retc2 = True

        for idx in range(len(c1viols)):
            if c1viols[idx] > c2viols[idx]:
                retc1 = False
            elif c2viols[idx] > c1viols[idx]:
                retc2 = False

        if retc1 and retc2:
            return None
        if retc1:
            return c1
        if retc2:
            return c2
        return None

    def addCompRows(self):
        if self.dopts:
            toprow = self.dopts[0]['vio']
        else:
            toprow = self.winners[0]['vio']

        for series in self.cands:
            for cand in series:
                cand['comprow'] = []
                for idx in range(len(toprow)):
                    if toprow[idx] < cand['vio'][idx]:
                        cand['comprow'].append('W')
                    elif toprow[idx] > cand['vio'][idx]:
                        cand['comprow'].append('L')
                    else:
                        cand['comprow'].append('-')
                
    def tableauRows(self, tableau=None, skiphb=False, smartEval=True, baton=None):
        if self.dopts:
            toprow = self.dopts[0]['vio']
        else:
            toprow = self.winners[0]['vio']
        if skiphb:
            for cand in self.skipHBcandidates(self.tableauRows(tableau, skiphb=False, baton=baton)):
                yield cand
        else:
            if baton:
                end = 0
                for series in self.cands:
                    end += len(series)
                baton.end = end
                baton.percent = True
            if tableau is None:
                tableau = self.tableau

            if tableau == 'comparative':
                for cand in self.winners:
                    if cand not in self.dopts:
                        cand['comprow'] = []
                        for idx in range(len(toprow)):
                            if toprow[idx] < cand['vio'][idx]:
                                cand['comprow'].append('W')
                            elif toprow[idx] > cand['vio'][idx]:
                                cand['comprow'].append('L')
                            else:
                                cand['comprow'].append('-')
                        yield cand
                    if baton:
                        baton.twirl()

            if smartEval:
                tabSeries = reversed(self.cands)
            else:
                tabSeries = self.cands[-1]

            for series in tabSeries:
                for cand in series:
                    if not self.hasChildren(cand):
                        if tableau == 'comparative':
                            if (cand not in self.winners and
                                cand not in self.dopts):
                                cand['comprow'] = []
                                for idx in range(len(toprow)):
                                    if toprow[idx] < cand['vio'][idx]:
                                        cand['comprow'].append('W')
                                    elif toprow[idx] > cand['vio'][idx]:
                                        cand['comprow'].append('L')
                                    else:
                                        cand['comprow'].append('-')
                                yield cand
                        else:
                            cand['crucials'] = []
                            for idx in range(len(toprow)):
                                cand['crucials'].append(cand['vio'][idx] - 
                                                        toprow[idx])
                            yield cand
                    if baton: baton.twirl()


    def getChildren(self, cand):
        '''Returns the a list of candidates from the tree that are
        children of cand.'''
        out = []
        key = cand['key']
        if len(key) == len(self.cands):
            return out
        series = self.cands[len(key)]
        for comple in series:
            if comple['key'][:len(key)] == key:
                out.append(comple)
        return out

    def hasChildren(self, cand):
        '''Returns True if there are any nodes in the cands tree that
        are children of cand, otherwise False'''
        out = 0
        key = cand['key']
        if len(key) == len(self.cands):
            return False
        series = self.cands[len(key)]
        for comple in series:
            if comple['key'][:len(key)] == key:
                return True
        return False

    def skipHBcandidates(self, series):
        '''Generates candidates from the iterator series that are not
        harmonically bounded.  Assumes that the series has already
        been sorted for harmonicity.'''
        sofar = []

        for cand in series:
            go = True
            for yet in sofar:
                hb = self.harmonicBounding(yet, cand)
                if hb is not None:
#                    assert hb is yet
                    go = False
            if go:
                sofar.append(cand)
                yield cand

def doEasyFactorialTypology(ranking, violations):
    rankings = permu(ranking)

    results = [selectOptimalCandidate(ranking, violations) for ranking in rankings]

    return results, set(results)

def numberDopts(dopts, cands, inp, ranking, **kwargs):
    level = kwargs.get('level', Foot)
    verbose = kwargs.get('verbose', True)
    out = []
    num = 0

    if type(cands) is FunctionType:
        cands = cands(inp, ranking, level=level,
                      dopts=dopts)

    with Baton('Numbering desired optima', 
               'done', 
               0,
               verbose) as baton:
        if type(cands) in (FunctionType, GeneratorType):
            for cand in cands:
                if cand in dopts:
                    out.append((cand, num))
                baton.twirl()
                num += 1
        else:
            for dopt in dopts:
                out.append(dopt, cands.index(dopt))
                baton.twirl()
                num += 1

    return out

def permu(xs):
    '''Returns a list of every possible permutation of the members of
    the sequence xs.'''
    if xs:
        r , h = [],[]
        for x in xs:
            if x not in h:
                ts = xs[:]; ts.remove(x)
                for p in permu(ts):
                    r.append([x]+p)
            h.append(x)
        return r
    else:
        return [[]]

def maskCCViolations(vio, ranking):
    '''Given a sequence of integers vio representing violations of the
    constraints in ranking, returns a sequence that matches vio except
    that violations of CC-correspondence constraints are replaced by
    0.'''
    out = []

    for idx in range(len(vio)):
        constr = ranking[idx]
        if constr[0] in (constraints.notIdentCC,
                         constraints.IdentCC,
                         constraints.CC_Corr):
            out.append(0)
        else:
            out.append(vio[idx])

    return out

def main(target=HTML, **kwargs):
    '''Demonstration function: replicates the tableau on page 274 of
    Lombardi (1999) (but with an extra candidate!)'''
    tableau = kwargs.get('tableau', 'comparative')
    fn = kwargs.get('filename', 'testtab')
    verbose = kwargs.get('verbose', True)
    ur = Utterance('gut')

    g = Grammar(**kwargs)
    if target == HTML:
        suff = '.html'
    elif target == LaTeX:
        g.scheme = 'tipa'
        suff = '.tex'
    else:
        suff = ''
    g.level = Syllable
    g.filter = ((StarSyllC,),(StarVoicelessV,))
    g.ranking = ((OnsIdent, 'voi'),
                 (VOP,),
                 (Ident, 'voi'))
    result = g.smartGenEval(ur, verbose=verbose)

    g.writeTableau(filename=(fn + suff), tableau=tableau)
    
    return g

if __name__ == '__main__':
    if len(sys.argv) > 1:
        sys.exit(main(sys.argv[1]))
    else:
        sys.exit(main())
