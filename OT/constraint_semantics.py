'''Defines a grammar for possible arguments to constraints.'''

import representations
from ui_util import batonify
import inspect, itertools

mode = ('IO', 'BR', 'CC')
unit = representations.levels
syllnode = ('ons', 'nuc', 'cod')
feat = representations.features.keys() + representations.features.values()
direction = ('L', 'R')
num = (0, Ellipsis)
feats = (feat, Ellipsis)
featspec = (feat, feats)
fcons = ('Max', 'Dep', None)

ontology = (mode, unit, syllnode, feat, feats, featspec, direction, num, fcons)

def prettyPrintSpec(spec=None):
    '''Returns a representation of the spec spec suitable for printing.'''
    if spec is None:
        return None
    ell = whichEllipsis(spec)
    if ell is None:
        return str(spec)
    if ell == 'kleene':
        return str([spec[0], '...'])
    else:
        return str(['...' if x is Ellipsis else x for x in spec])

def recursiveSpec(spec):
    '''Yields the members of the spec ontology that appear inside spec.'''
    for subspec in spec:
        if subspec in ontology:
            yield subspec

def whichEllipsis(spec=None):
    '''Ellipsis in a spec can mean one of two things.  It can mean the
    same as a Kleene plus for other specs in the ontology (as in the
    featspec spec), or it can be used to specify a (possibly
    open-ended) sequence of numbers (as in the num spec).  This
    function returns a tuple (lower bound, upper bound) if the spec
    passed to it uses Ellipsis to specify a sequence of numbers,
    'kleene' if the spec is a Kleene-star type, or None if there is no
    Ellipsis in the spec.'''
    if spec is None:
        return None
    if Ellipsis in spec:
        if (len(spec) == 2 and
            spec[0] in ontology):
            return 'kleene'
        else:
            (lbound, ubound) = (None, None)
            if len(spec) == 3:
                assert type(spec[0]) in (int, long, float), spec
                assert spec[1] is Ellipsis, spec
                assert type(spec[2]) in (int, long, float), spec
                lbound = spec[0]
                ubound = spec[2]
            else:
                assert len(spec) == 2
                if spec[0] is Ellipsis:
                    assert type(spec[1]) in (int, long, float), spec
                    ubound = spec[1]
                else:
                    assert type(spec[0]) in (int, long, float), spec
                    lbound = spec[0]
            return (lbound, ubound)
    else:
        return None

def meetsSpec(parm, spec=None):
    '''Tests whether the parameter parm matches the parameter spec
    spec.'''
    if spec is None:
        return True

    for subspec in recursiveSpec(spec):
        if meetsSpec(parm, subspec):
            return True

    ell = whichEllipsis(spec)
    if ell is None:
        return (parm in spec)
    elif ell == 'kleene':
        for subparm in parm:
            if not meetsSpec(subparm, spec[0]):
                return False
        return True
    else:
        (lbound, ubound) = ell
        if lbound is None:
            return (parm <= ubound)
        elif ubound is None:
            return (lbound <= parm)
        else:
            return (lbound <= parm <= ubound)

def specForParmName(nm):
    '''Determines whether an argument named nm should be constrained
    by a spec in the ontology.  If so, returns it.  Otherwise, returns
    None.'''
    space = globals()
    if space.get(nm, None) in ontology:
        return space[nm]
    trailing = nm.split('_')[-1]
    if space.get(trailing, None) in ontology:
        return space[trailing]
    return None

def schemaForConstraint(cfunc):
    '''Returns a list of the parameter names expected by the
    constraint function cfunc.'''
    argspec = inspect.getargspec(cfunc)

    schema = argspec.args[2:]
                    
    if argspec.varargs:
        schema.append(argspec.varargs)
    return schema

@batonify
def validateConstraint(constr, baton=None, verbose=True):
    '''Tests whether constr is a valid instance of the constraint
    schema defined by the function constr[0], based on the names of
    the parameters constr[0] takes.'''
    baton.message('Validating constraint %s' % str(constr))
    argspec = inspect.getargspec(constr[0])

    schema = argspec.args[2:]
    parms = list(constr[1:len(schema) + 1])

    if argspec.varargs:
        schema.append(argspec.varargs)
        parms.append(constr[len(parms) + 1:])

    print schema
    print parms

    for pname, parm in zip(schema, parms):
        spec = specForParmName(pname)
        if not meetsSpec(parm, spec):
            raise InvalidConstraint(constr, parm, spec)
        else:
            baton.message('Parameter %s in range %s' % 
                          (str(parm), prettyPrintSpec(spec)))
        baton.twirl()
    return True
    
class InvalidConstraint(Exception):
    def __init__(self, constr, parm, spec):
        self.constr = constr
        self.parm = str(parm)
        self.spec = spec

    def __str__(self):
        return ('Invalid constraint %s: parameter %s should be one of %s' % 
                (self.constr, self.parm, prettyPrintSpec(self.spec)))
